
#include <stdlib.h>
#include <robotics/slpp.h>
#include <robotics/table_tennis/env_state.h>
#include "table_tennis_common.h"
#include "SL.h"

#define NDOF 7

static char init_called_before = 0;
static void* robot_task = 0;

/**
 * @brief Puts the 3d blob information in the TTBallObs format
 */
static void init_3d_ball_obs(struct TTBallObs* obs, int blob_id, double time) {
  static const int indices[3] = {_X_,_Y_,_Z_};
  int i;
  obs->time = time;
  for (i=0; i<3; i++) {
    obs->pos[i] = blobs[blob_id].blob.x[indices[i]];
  }
  obs->sensor_id = blob_id;
  obs->dims = 3;
}

/**
 * @brief Puts the 3D ball estimates of SL in the environment buffer of SL++
 */
static void fill_3d_ball_data(void* env, double time) {
  static const int ix_blobs_3d[] = {1,3};
  static const unsigned int n_blobs_3d = 2;
  unsigned int i;
  struct TTBallObs obs3d;
  for (i=0; i<n_blobs_3d; i++) {
    init_3d_ball_obs(&obs3d, ix_blobs_3d[i], time);
    tt_envobs_add_sl_ball(env, &obs3d);
  }
}

/**
 * @brief Puts the ball data into the environment state object.
 */
static void fill_ball_data(void* env, double time) {
  fill_3d_ball_data(env, time);
  /* TODO: Fill also 2d raw ball information */
}

/**
 * @brief Pushes the current joint measurement in the task state buffer
 */
static void push_joint_measure(void* task, double time) {
  unsigned int i;
  double q[NDOF], qd[NDOF];
  for (i=0; i<NDOF; i++) {
    q[i] = joint_state[i+1].th;
    qd[i] = joint_state[i+1].thd;
  }
  rob_slpp_add_joint_obs(task, q, qd, time);
}

/**
 * @brief Get the controller command (In this case desired joint configuration) and set it
 * to the internal SL controller
 */
static void set_invdyn_motor_commands(void* task, double time) {
  unsigned int i;
  double q[NDOF], qd[NDOF], qdd[NDOF];
  struct RobSLPPInvDynSignal des_joint;
  des_joint.q = q; des_joint.qd = qd; des_joint.qdd = qdd;
  rob_slpp_control(task, &des_joint, time);
  for (i=0; i<NDOF; i++) {
    joint_des_state[i+1].th = q[i];
    joint_des_state[i+1].thd = qd[i];
    joint_des_state[i+1].thdd = qdd[i];
  }
}

/**
 * @brief Freezes the robot and program execution if the joint configuration is considered unsafe
 */
static void freeze_if_unsafe() {
  if (!check_joint_limits(joint_state, 0.01)) {
    printf("Joint limits are exceeding soft limits! Freezing...\n");
    freeze();
  }
  if (!check_des_joint_limits(joint_des_state, 0.01)) {
    printf("Joint des limits are exceeding soft limits! Freezing...\n");
    freeze();
  }
}

/**
 * Do nothing. Useful for debugging only.
 */
static void nop() {
  unsigned int i;
  for (i=0; i<NDOF; i++) {
    joint_des_state[i+1].th = joint_state[i+1].th;
    joint_des_state[i+1].thd = 0.0;
    joint_des_state[i+1].thdd = 0.0;
  }
}

/**
 * @brief This code is called to initialize the SL++ task
 */
static int init_slpp_task() {
  static const struct RobSLPPTaskParams params = {NDOF};
  if (!init_called_before) {
    init_called_before = 1;
    robot_task = rob_slpp_idtt_menu_create_task(&params);
  } else {
    rob_slpp_restart_task(robot_task);
  }
  return 1; /* true */
}

/**
 * @brief Control loop function of the SL++ task
 */
static int run_slpp_task() {
  double curr_time = rob_slpp_get_task_time(robot_task);
  void* env = rob_slpp_get_obs_env(robot_task);
  fill_ball_data(env, curr_time);
  push_joint_measure(robot_task, curr_time);
  set_invdyn_motor_commands(robot_task, curr_time);
  freeze_if_unsafe();
  /* Finally, run the "SL Controller" */
  SL_InvDyn(NULL, joint_des_state, endeff, &base_state, &base_orient);
  return 1; /* true */
}

static int change_slpp_task() {
  static const struct RobSLPPTaskParams params = {NDOF};
  if (robot_task != 0) {
    rob_slpp_delete_task( robot_task );
    robot_task = rob_slpp_idtt_menu_create_task(&params);
  }
  return TRUE;
}

/**
 * @brief Adds the SL++ IDTT task to the usual SL menu
 */
void add_slpp_task() {
  addTask("SL++ IDTT Task", init_slpp_task, run_slpp_task, change_slpp_task);
}
