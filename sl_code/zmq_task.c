
#include "table_tennis_common.h"
#include <zmq.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <glib.h>

#define BALL_BUFF_SIZE (1<<16)

/* Ball Buffer code */

typedef struct {
  double pos[3];
  double time;
  int cam_id;
  int no_use; //To avoid filling problems in 32/64 bit machines
} BallObs;

typedef struct {
  BallObs buff[BALL_BUFF_SIZE];
  BallObs* last_obs[4];
  int size;
} BallState;

static void init_ball_state(BallState *st) {
  int i;
  st->size = 0;
  for (i=0; i<4; i++) {
    st->last_obs[i]=NULL;
  }
}

static int equal_ball_positions(const BallObs* obs1, const BallObs* obs2) {
  static double eps = 1e-6;
  int i;
  for (i=0; i<3; i++) {
    if (fabs(obs1->pos[i] - obs2->pos[i]) > eps) return 0;
  }
  return 1;
}

static void init_ball_obs(BallObs* obs, int blob_id, double time) {
  static int indices[3] = {_X_,_Y_,_Z_};
  int i;
  obs->time = time;
  for (i=0; i<3; i++) {
    obs->pos[i] = blobs[blob_id].blob.x[indices[i]];
  }
  obs->cam_id = blob_id;
}

/* Trajectory Buffer Code */

typedef struct {
  double q[N_DOFS];
  double qdot[N_DOFS];
  double qdotdot[N_DOFS];
  double time;
} TrajStep;

typedef struct {
  TrajStep* data;
  int max_size;
  int low_pos, high_pos;
  pthread_mutex_t my_lock;
} TrajBuffer;

static TrajBuffer* traj_buff_new(int buffer_size) {
  TrajBuffer *ans = (TrajBuffer*)malloc(sizeof(TrajBuffer));
  ans->low_pos = ans->high_pos = 0;
  ans->max_size = buffer_size;
  ans->data = (TrajStep*)malloc(buffer_size * sizeof(TrajStep));
  pthread_mutex_init(&(ans->my_lock), NULL);
  return ans;
}

static void traj_buff_delete(TrajBuffer* buff) {
  pthread_mutex_destroy(&(buff->my_lock));
  free(buff->data);
  free(buff);
}

static TrajStep traj_buffer_at(TrajBuffer* buff, int index) {
  TrajStep ans;
  pthread_mutex_lock(&(buff->my_lock));
  ans = buff->data[(buff->low_pos + index) % buff->max_size];
  pthread_mutex_unlock(&(buff->my_lock));
  return ans;
}

static unsigned int traj_buffer_size(TrajBuffer* buff) {
  unsigned int ans;
  pthread_mutex_lock(&(buff->my_lock));
  ans = buff->high_pos - buff->low_pos;
  pthread_mutex_unlock(&(buff->my_lock));
  return ans;
}

static void traj_buffer_pop_front(TrajStep* ans, TrajBuffer* buff, int nelem) {
  int i;
  if (nelem > traj_buffer_size(buff)) {
    printf("Error: The number of elements requested from the trajectory buffer exceeds the amount of elements\n");
    return;
  }
  pthread_mutex_lock(&(buff->my_lock));
  for (i=0; i<nelem; i++) {
    ans[i] = buff->data[(buff->low_pos + i) % buff->max_size];
  }
  buff->low_pos += nelem;
  pthread_mutex_unlock(&(buff->my_lock));
}

static void traj_buffer_pop_back(TrajStep* ans, TrajBuffer* buff, int nelem) {
  int i;
  if (nelem > traj_buffer_size(buff)) {
    printf("Error: The number of elements requested from the trajectory buffer exceeds the amount of elements\n");
    return;
  }
  pthread_mutex_lock(&(buff->my_lock));
  for (i=0; i<nelem; i++) {
    ans[i] = buff->data[(buff->high_pos - i - 1) % buff->max_size];
  }
  buff->high_pos -= nelem;
  pthread_mutex_unlock(&(buff->my_lock));
}

static void traj_buffer_push_back(TrajBuffer* buff, const TrajStep* data, int nelem) {
  int i;
  if (nelem > (buff->max_size - traj_buffer_size(buff))) {
    printf("Error: The number of elements pushed into trajectory buffer exceeds the available capacity\n");
    return;
  }
  pthread_mutex_lock(&(buff->my_lock));
  for (i=0; i<nelem; i++) {
    buff->data[(buff->high_pos + i) % buff->max_size] = data[i];
  }
  buff->high_pos += nelem;
  pthread_mutex_unlock(&(buff->my_lock));
}

static void traj_buffer_clean(TrajBuffer* buff) {
  pthread_mutex_lock(&(buff->my_lock));
  buff->low_pos = buff->high_pos = 0;
  pthread_mutex_unlock(&(buff->my_lock));
}

/* Trajectory server code */

#define DES_TRAJ_BUFF_SIZE 1800000
#define OBS_TRAJ_BUFF_SIZE 200000
#define SOCK_BUFF_SIZE (1 << 20)
#define TCP_PORT_NUM 7646

//request action codes
#define TRAJ_ACTION_END 0
#define TRAJ_ACTION_CLEAN 1
#define TRAJ_ACTION_PUSH_BACK 2
#define TRAJ_ACTION_GET_OBSERVED_ARM 3
#define TRAJ_ACTION_GET_BALL_OBS 4
#define TRAJ_ACTION_CLEAR_BALL_OBS 5
#define TRAJ_ACTION_ADD_SIM_BALL 6
#define TRAJ_ACTION_CLEAR_SIM_BALL 7
#define TRAJ_ACTION_GET_TIME 8

//response codes
#define TRAJ_RESP_END 0
#define TRAJ_RESP_GET_OBSERVED_ARM 3
#define TRAJ_RESP_GET_BALL_OBS 4
#define TRAJ_RESP_GET_TIME 8

/* Defining global variables */

static TrajBuffer* desired = NULL;
static TrajBuffer* observed = NULL;
static pthread_t traj_server_thread;
static pthread_mutex_t server_robot_lock;
static char init_called_before = 0;
static BallState ball_st;
static GQueue* ball_sim_st;
static double start_time;
static double last_task_time;

/* End global variables */

static int traj_server_push_back_action(const char* sock_buff, TrajBuffer* traj_buff) {
  const unsigned int *nelem = (const unsigned int*)sock_buff;
  const TrajStep* elems = (const TrajStep*)(sock_buff + sizeof(unsigned int));
  traj_buffer_push_back(traj_buff, elems, *nelem);
  return sizeof(unsigned int) + (*nelem)*sizeof(TrajStep);
}

static int traj_server_add_sim_ball(const char* sock_buff, GQueue* queue) {
  const unsigned int *nelem = (const unsigned int*)sock_buff;
  const BallObs* elems = (const BallObs*)(sock_buff + sizeof(unsigned int));
  unsigned int i;
  BallObs* curr;
  //double robot_time = task_servo_time - start_time;
  //printf("Received simulated ball trajectory of lenght %d, each packet of %lu bytes\n", *nelem, sizeof(BallObs));
  for (i=0; i<*nelem; i++) {
    curr = (BallObs*)malloc(sizeof(BallObs));
    *curr = elems[i];
    //curr->time += robot_time;
    g_queue_push_tail(queue, (gpointer*)curr);
  }
  //printf("New simulated buffer size: %d\n", g_queue_get_length(queue));
  return sizeof(unsigned int) + (*nelem)*sizeof(BallObs);
}

static void traj_server_clear_sim_ball(GQueue* queue) {
  while (!g_queue_is_empty(queue)) {
    free( g_queue_pop_head(queue) );
  }
}

static int traj_server_get_arm_state(const char* req_buff, char* res_buff, int* res_bytes, TrajBuffer* obs_buff) {
  const unsigned int *p_nelem = (const unsigned int*)req_buff;
  unsigned int obs_buff_size = traj_buffer_size(obs_buff);
  unsigned int nelem = ((*p_nelem > obs_buff_size) ? obs_buff_size : *p_nelem);
  res_buff[(*res_bytes)++] = TRAJ_RESP_GET_OBSERVED_ARM;
  //printf("requested %u arm positions\n", nelem);
  *((unsigned int*)(res_buff + *res_bytes)) = nelem;
  *res_bytes += sizeof(unsigned int);
  traj_buffer_pop_back((TrajStep*)(res_buff + *res_bytes), obs_buff, nelem);
  *res_bytes += sizeof(TrajStep)*nelem;
  traj_buffer_clean(obs_buff);
  return sizeof(unsigned int);
}

static void traj_server_get_ball_obs(char* res_buff, int* res_bytes) {
  unsigned int nelem = ball_st.size;
  res_buff[(*res_bytes)++] = TRAJ_RESP_GET_BALL_OBS;
  *((unsigned int*)(res_buff + *res_bytes)) = nelem;
  *res_bytes += sizeof(unsigned int);
  memcpy(res_buff + *res_bytes, ball_st.buff, nelem*sizeof(BallObs));
  *res_bytes += nelem*sizeof(BallObs);
  init_ball_state(&ball_st);
}

static void traj_server_get_time(char* res_buff, int* res_bytes) {
  res_buff[(*res_bytes)++] = TRAJ_RESP_GET_TIME;
  *((double*)(res_buff + *res_bytes)) = last_task_time;
  *res_bytes += sizeof(double);
}

static void* traj_server_main(void* arg) {
  void *context = zmq_ctx_new();
  void* responder = zmq_socket(context, ZMQ_REP);
  char* req_buffer = malloc(SOCK_BUFF_SIZE);
  char* res_buffer = malloc(SOCK_BUFF_SIZE);
  char uri[100];
  int num_bytes, res_bytes, i;
  char finished;
  sprintf(uri, "tcp://*:%d", TCP_PORT_NUM);
  assert( zmq_bind(responder, uri) == 0 ); /* Bind to TCP port */
  printf("Starting server in the port %d\n", TCP_PORT_NUM);

  while (1) {
    finished = 0;
    num_bytes = zmq_recv(responder, req_buffer, SOCK_BUFF_SIZE, 0);
    res_bytes = 0;
    //printf("request received\n");
    pthread_mutex_lock(&server_robot_lock);
    for (i=0; i<num_bytes && !finished; i++) {
      //printf("action on i=%d\n", i);
      switch (req_buffer[i]) {
        case TRAJ_ACTION_END:
          finished = 1;
          break;
        case TRAJ_ACTION_CLEAN:
          traj_buffer_clean(desired);
          break;
        case TRAJ_ACTION_PUSH_BACK:
          i += traj_server_push_back_action(req_buffer + (i + 1), desired);
          break;
        case TRAJ_ACTION_GET_OBSERVED_ARM:
          i += traj_server_get_arm_state(req_buffer + (i + 1), res_buffer, &res_bytes, observed);
          break;
        case TRAJ_ACTION_GET_BALL_OBS:
          traj_server_get_ball_obs(res_buffer, &res_bytes);
          break;
        case TRAJ_ACTION_CLEAR_BALL_OBS:
          init_ball_state(&ball_st);
          break;
        case TRAJ_ACTION_ADD_SIM_BALL:
          i += traj_server_add_sim_ball(req_buffer + (i + 1), ball_sim_st);
          break;
        case TRAJ_ACTION_CLEAR_SIM_BALL:
          traj_server_clear_sim_ball(ball_sim_st);
          break;
        case TRAJ_ACTION_GET_TIME:
          traj_server_get_time(res_buffer, &res_bytes);
          break;
        default:
          printf("Socket received action %d is not recognized\n", req_buffer[i]);
          finished = 1;
          break;
      }
      //printf("request fully processed\n");
      pthread_mutex_unlock(&server_robot_lock);
    }
    res_buffer[res_bytes++] = TRAJ_RESP_END;
    zmq_send(responder, res_buffer, res_bytes, 0);
    //printf("response sent\n");
  }

  free(req_buffer);
  free(res_buffer);
  return NULL;
}

/* SL and ZMQ code */

static int init_zmq_task() {
  if (!init_called_before) {
    init_called_before = 1;
    desired = traj_buff_new(DES_TRAJ_BUFF_SIZE);
    observed = traj_buff_new(OBS_TRAJ_BUFF_SIZE);
    if (pthread_create(&traj_server_thread, NULL, traj_server_main, NULL) != 0) {
      printf("Can't start server thread\n");
      return FALSE;
    }
    pthread_mutex_init(&server_robot_lock, NULL);
    ball_sim_st = g_queue_new();
  }
  init_ball_state(&ball_st);
  traj_server_clear_sim_ball(ball_sim_st);
  start_time = task_servo_time;
  return TRUE;
}

static void append_ball_info(double time) {
  static int relevant_blobs[2] = {1,3};
  int blob_id, i;
  BallObs obs;
  BallObs *sim;
  for (i=0; i<2; i++) {
    blob_id = relevant_blobs[i];
    if (blobs[blob_id].status == 1) {
      init_ball_obs(&obs, blob_id, time);
      if (ball_st.last_obs[blob_id] == NULL || !equal_ball_positions(&obs, 
            ball_st.last_obs[blob_id])) {
        if (ball_st.size < BALL_BUFF_SIZE) {
          ball_st.buff[ball_st.size++] = obs;
          ball_st.last_obs[blob_id] = &ball_st.buff[ball_st.size-1];
        }
      }
    }
    /* If ball_sim_st is not empty, we assume we are in ball simulation mode */
    if (!g_queue_is_empty(ball_sim_st)) {
      sim = (BallObs*)g_queue_peek_head(ball_sim_st);
      if (time > sim->time) {
        //printf("%lf %lf %lf, time=%lf\n", sim->pos[0], sim->pos[1], sim->pos[2], sim->time);
        sim->time = time;
        ball_st.buff[ball_st.size++] = *sim;
        ball_st.last_obs[blob_id] = &ball_st.buff[ball_st.size-1];
        free( g_queue_pop_head(ball_sim_st) );
      }
    }
    /* end of code for adding simulated balls */
  }
}

static int run_zmq_task() {
  TrajStep curr;
  int i;
  double task_time;
  pthread_mutex_lock(&server_robot_lock);
  task_time = task_servo_time - start_time;
  last_task_time = task_time;
  if ((traj_buffer_size(desired) == 0) || (traj_buffer_at(desired,0).time > task_time)) {
    for (i=1; i<=N_DOFS; i++) {
      joint_des_state[i].th = joint_state[i].th + joint_state[i].thd/task_servo_rate;
      joint_des_state[i].thd = 0.0;
      joint_des_state[i].thdd = 0.0;
      joint_des_state[i].uff = 0.0;
    }
  } else {
    traj_buffer_pop_front(&curr, desired, 1);
    for (i=1; i<=N_DOFS; i++) {
      joint_des_state[i].th = curr.q[i-1];
      joint_des_state[i].thd = curr.qdot[i-1];
      joint_des_state[i].thdd = curr.qdotdot[i-1];
    }
  }

  if (traj_buffer_size(observed) == OBS_TRAJ_BUFF_SIZE) observed->low_pos++;
  for (i=1; i<=N_DOFS; i++) {
    curr.q[i-1] = joint_state[i].th;
    curr.qdot[i-1] = joint_state[i].thd;
    curr.qdotdot[i-1] = joint_state[i].thdd;
  }
  curr.time = task_time;
  traj_buffer_push_back(observed, &curr, 1);
  append_ball_info(task_time);

  pthread_mutex_unlock(&server_robot_lock);
  if (!check_joint_limits(joint_state, 0.01)) {
    printf("Joint limits are exceeding soft limits! Freezing...\n");
    freeze();
  }
  if (!check_des_joint_limits(joint_des_state, 0.01)) {
    printf("Joint des limits are exceeding soft limits! Freezing...\n");
    freeze();
  }
  SL_InvDyn(NULL, joint_des_state, endeff, &base_state, &base_orient);
  return TRUE;
}

static int change_zmq_task() {
  return TRUE;
}

void add_zmq_task() {
  addTask("ZMQ Task", init_zmq_task, run_zmq_task, change_zmq_task);
}
