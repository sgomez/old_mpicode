
#include <stdlib.h>
#include <robotics/slpp/sl.h>
#include <robotics/table_tennis/env_state.h>
#include "table_tennis_common.h"
#include "SL.h"

#define NDOF 7

static char init_called_before = 0;
static void* robot_task = 0;
static void* sl_obj = 0;
static void* inv_dyn_state = 0;
static void* inv_dyn_control_signal = 0;

/* These IDs come from the JSON configuration file */
static const int nsensors = 2;
static const int sl_ids[] = {3,1};
static const int slpp_ids_3d[] = {0,1};
static const int slpp_ids_2d[] = {10,11,12,13};

static void push_new_vision_blobs(double time) {
  int i,j,k;
  double x[3];
  for (i=0; i<nsensors; i++) {
    if (blobs[sl_ids[i]].status == 1) {
      for (j=0; j<3; j++) {
        x[j] = blobs[sl_ids[i]].blob.x[j+1];
      }
      if (rob_slpp_is_obs_new(sl_obj, slpp_ids_3d[i], x)) {
        rob_slpp_add_sensor_obs(sl_obj, slpp_ids_3d[i], time, x);
      }
    }
    for (k=0; k<2; k++) {
      if (raw_blobs2D[sl_ids[i]][k+1].status == 1) {
        for (j=0; j<2; j++) {
          x[j] = raw_blobs2D[sl_ids[i]][k+1].x[j+1];
        }
        if (rob_slpp_is_obs_new(sl_obj, slpp_ids_2d[2*i + k], x)) {
          rob_slpp_add_sensor_obs(sl_obj, slpp_ids_2d[2*i + k], time, x);
        }
      }
    }
  }
}

/**
 * @brief Pushes the current joint measurement in the task state buffer
 */
static void push_joint_measure(void* sl_obj, double time) {
  unsigned int i;
  double q[NDOF], qd[NDOF];
  for (i=0; i<NDOF; i++) {
    q[i] = joint_state[i+1].th;
    qd[i] = joint_state[i+1].thd;
  }
  rob_slpp_add_joint_obs(sl_obj, q, qd, time);
}

/**
 * @brief Get the controller command (In this case desired joint configuration) and set it
 * to the internal SL controller
 */
static void set_invdyn_motor_commands(double time) {
  unsigned int i;
  double q[NDOF], qd[NDOF], qdd[NDOF];
  //First set up the current state
  for (i=0; i<NDOF; i++) {
    q[i] = joint_state[i+1].th;
    qd[i] = joint_state[i+1].thd;
  }
  rob_slpp_set_inv_dyn_state(inv_dyn_state, q, qd, time);
  //Now add the control signal
  rob_slpp_control(sl_obj, inv_dyn_control_signal, inv_dyn_state, time);
  rob_slpp_get_inv_dyn_signal(inv_dyn_control_signal, q, qd, qdd);
  for (i=0; i<NDOF; i++) {
    joint_des_state[i+1].th = q[i];
    joint_des_state[i+1].thd = qd[i];
    joint_des_state[i+1].thdd = qdd[i];
  }
}

/**
 * @brief Freezes the robot and program execution if the joint configuration is considered unsafe
 */
static void freeze_if_unsafe() {
  if (!check_joint_limits(joint_state, 0.01)) {
    printf("Joint limits are exceeding soft limits! Freezing...\n");
    freeze();
  }
  if (!check_des_joint_limits(joint_des_state, 0.01)) {
    printf("Joint des limits are exceeding soft limits! Freezing...\n");
    freeze();
  }
}

/**
 * @brief This code is called to initialize the SL++ task
 */
static int init_slpp_task() {
  if (!init_called_before) {
    init_called_before = 1;
    robot_task = rob_slpp_create_net_task("config/net_task.json");
    sl_obj = rob_slpp_sl_object_net_task(robot_task);
    inv_dyn_state = rob_slpp_create_inv_dyn_state(NDOF);
    inv_dyn_control_signal = rob_slpp_create_inv_dyn_signal(NDOF);
  } else {
    //rob_slpp_restart_task(robot_task);
  }
  return 1; /* true */
}

/**
 * @brief Display the table tennis stuff in OpenGL
 */
static void display_ttennis() {
  double pos[8+4*nsensors];
  int i,j;
  setDefaultEndeffector();
  endeff[RIGHT_HAND].x[_Z_] = 0.3;
  pos[0] = 8 + 4*nsensors;
  simulate_racket(&sim_racket_state, &sim_racket_orient, 
      cart_state[RIGHT_HAND], cart_orient[RIGHT_HAND]);
  pos[1] = sim_racket_state.x[_X_];
  pos[2] = sim_racket_state.x[_Y_];
  pos[3] = sim_racket_state.x[_Z_];
  pos[4] = sim_racket_orient.q[_Q0_];
  pos[5] = sim_racket_orient.q[_Q1_];
  pos[6] = sim_racket_orient.q[_Q2_];
  pos[7] = sim_racket_orient.q[_Q3_];

  for (i=0; i<nsensors; i++) {
    pos[8+4*i] = blobs[sl_ids[i]].status;
    for (j=1; j<=3; j++) {
      pos[8 + 4*i + j] = blobs[sl_ids[i]].blob.x[j];
    }
  }

  sendUserGraphics("display_tt_2", pos, (8 + 4*nsensors)*sizeof(double));
}

/**
 * @brief Control loop function of the SL++ task
 */
static int run_slpp_task() {
  double curr_time = rob_slpp_get_task_time(sl_obj);
  push_joint_measure(sl_obj, curr_time);
  set_invdyn_motor_commands(curr_time);
  push_new_vision_blobs( curr_time );
  freeze_if_unsafe();
  /* Finally, run the "SL Controller" */
  SL_InvDyn(NULL, joint_des_state, endeff, &base_state, &base_orient);
  display_ttennis();
  return 1; /* true */
}

static int change_slpp_task() {
  /*static const struct RobSLPPTaskParams params = {NDOF};
  if (robot_task != 0) {
    rob_slpp_delete_task( robot_task );
    robot_task = rob_slpp_idtt_menu_create_task(&params);
  }*/
  return TRUE;
}

/**
 * @brief Adds the SL++ IDTT task to the usual SL menu
 */
void add_slpp_net_task() {
  addTask("SL++ Net Task", init_slpp_task, run_slpp_task, change_slpp_task);
}
