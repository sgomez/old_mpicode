
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  GQueue* q = g_queue_new();
  int *data;
  int i;
  for (i=0; i<10; i++) {
    data = malloc(sizeof(int));
    *data = 2*i;
    g_queue_push_tail(q, data);
  }
  while (!g_queue_is_empty(q)) {
    data = g_queue_pop_head(q);
    printf("%d\n", *data);
    free(data);
  }
  g_queue_free(q);
}
