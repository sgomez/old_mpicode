#include <zmq.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>

/* Trajectory Buffer Code */

typedef struct {
  double q[N_DOFS];
  double qdot[N_DOFS];
} TrajStep;

/* Double Ended queue code */

typedef struct {
  void* data;
  size_t elem_size; /* Size of each element in bytes */
  size_t max_size; /* Number of elements in the buffer */
  size_t low_pos, high_pos;
  pthread_mutex_t my_lock;
} Dequeue;

static Dequeue* dequeue_new(size_t buffer_size, size_t elem_size) {
  Dequeue *ans = (Dequeue*)malloc(sizeof(Dequeue));
  ans->low_pos = ans->high_pos = 0;
  ans->max_size = buffer_size;
  ans->elem_size = elem_size;
  ans->data = malloc(buffer_size * elem_size);
  pthread_mutex_init(&(ans->my_lock), NULL);
  return ans;
}

static void dequeue_delete(Dequeue* buff) {
  pthread_mutex_destroy(&(buff->my_lock));
  free(buff->data);
  free(buff);
}

static void* dequeue_at(Dequeue* buff, int index) {
  void* ans;
  pthread_mutex_lock(&(buff->my_lock));
  ans = buff->data + buff->elem_size*((buff->low_pos + index) % buff->max_size);
  pthread_mutex_unlock(&(buff->my_lock));
  return ans;
}

static unsigned int dequeue_size(Dequeue* buff) {
  unsigned int ans;
  pthread_mutex_lock(&(buff->my_lock));
  ans = buff->high_pos - buff->low_pos;
  pthread_mutex_unlock(&(buff->my_lock));
  return ans;
}

static void _dequeue_copy_to(void* ans, size_t from_internal_ix, size_t to_internal_ix, Dequeue* buff) {
  size_t ix1 = from_interal_ix, ix2 = to_internal_ix, n1;
  if (ix2 > ix1) {
    memcpy(ans, buff->data + ix1*buff->elem_size, nelem*buff->elem_size);
  } else {
    n1 = (buff->max_size-ix1)*buff->elem_size;
    memcpy(ans, buff->data + ix1*buff->elem_size, n1);
    memcpy(ans + n1, buff->data, ix2*buff->elem_size);
  }
}

static void dequeue_copy_from(Dequeue* buff, size_t from_ix, size_t to_ix, const void* array) {
}

static void dequeue_pop_front(void* ans, Dequeue* buff, int nelem) {
  size_t ix1, ix2;
  if (nelem > dequeue_size(buff)) {
    printf("Error: The number of elements requested from the trajectory buffer exceeds the amount of elements\n");
    return;
  }
  pthread_mutex_lock(&(buff->my_lock));
  ix1 = buff->low_pos;
  ix2 = (buff->low_pos + nelem) % buff->max_size;
  _dequeue_copy_to(ans, ix1, ix2, buff);
  buff->low_pos += nelem;
  pthread_mutex_unlock(&(buff->my_lock));
}

static void dequeue_pop_back(void* ans, Dequeue* buff, int nelem) {
  size_t ix1, ix2;
  if (nelem > dequeue_size(buff)) {
    printf("Error: The number of elements requested from the trajectory buffer exceeds the amount of elements\n");
    return;
  }
  pthread_mutex_lock(&(buff->my_lock));
  ix1 = (buff->high_pos - nelem + buff->max_size) % buff->max_size;
  ix2 = buff->high_pos;
  _dequeue_internal_copy(ans, ix1, ix2, buff);
  buff->high_pos -= nelem;
  pthread_mutex_unlock(&(buff->my_lock));
}

/* TODO: Finish this */
static void dequeue_push_back(Dequeue* buff, const void* data, int nelem) {
  size_t ix1,ix2,n1;
  if (nelem > (buff->max_size - dequeue_size(buff))) {
    printf("Error: The number of elements pushed into trajectory buffer exceeds the available capacity\n");
    return;
  }
  pthread_mutex_lock(&(buff->my_lock));
  for (i=0; i<nelem; i++) {
    buff->data[(buff->high_pos + i) % buff->max_size] = data[i];
  }
  buff->high_pos += nelem;
  pthread_mutex_unlock(&(buff->my_lock));
}

static void traj_buffer_clean(TrajBuffer* buff) {
  pthread_mutex_lock(&(buff->my_lock));
  buff->low_pos = buff->high_pos = 0;
  pthread_mutex_unlock(&(buff->my_lock));
}


