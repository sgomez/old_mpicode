
#include <zmq.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>

#define N_DOFS 7

/* Trajectory Buffer Code */

typedef struct {
  double q[N_DOFS];
  double qdot[N_DOFS];
} TrajStep;

typedef struct {
  TrajStep* data;
  int max_size;
  int low_pos, high_pos;
  pthread_mutex_t my_lock;
} TrajBuffer;

TrajBuffer* traj_buff_new(int buffer_size) {
  TrajBuffer *ans = (TrajBuffer*)malloc(sizeof(TrajBuffer));
  ans->low_pos = ans->high_pos = 0;
  ans->max_size = buffer_size;
  ans->data = (TrajStep*)malloc(buffer_size * sizeof(TrajStep));
  pthread_mutex_init(&(ans->my_lock), NULL);
  return ans;
}

void traj_buff_delete(TrajBuffer* buff) {
  pthread_mutex_destroy(&(buff->my_lock));
  free(buff->data);
  free(buff);
}

TrajStep traj_buffer_at(TrajBuffer* buff, int index) {
  TrajStep ans;
  pthread_mutex_lock(&(buff->my_lock));
  ans = buff->data[(buff->low_pos + index) % buff->max_size];
  pthread_mutex_unlock(&(buff->my_lock));
  return ans;
}

unsigned int traj_buffer_size(TrajBuffer* buff) {
  unsigned int ans;
  pthread_mutex_lock(&(buff->my_lock));
  ans = buff->high_pos - buff->low_pos;
  pthread_mutex_unlock(&(buff->my_lock));
  return ans;
}

void traj_buffer_pop(TrajStep* ans, TrajBuffer* buff, int nelem) {
  int i;
  if (nelem > traj_buffer_size(buff)) {
    printf("Error: The number of elements requested from the trajectory buffer exceeds the amount of elements\n");
    return;
  }
  pthread_mutex_lock(&(buff->my_lock));
  for (i=0; i<nelem; i++) {
    ans[i] = buff->data[(buff->low_pos + i) % buff->max_size];
  }
  buff->low_pos += nelem;
  pthread_mutex_unlock(&(buff->my_lock));
}

void traj_buffer_push(TrajBuffer* buff, const TrajStep* data, int nelem) {
  int i;
  if (nelem > (buff->max_size - traj_buffer_size(buff))) {
    printf("Error: The number of elements pushed into trajectory buffer exceeds the available capacity\n");
    return;
  }
  pthread_mutex_lock(&(buff->my_lock));
  for (i=0; i<nelem; i++) {
    buff->data[(buff->high_pos + i) % buff->max_size] = data[i];
  }
  buff->high_pos += nelem;
  pthread_mutex_unlock(&(buff->my_lock));
}

void traj_buffer_clean(TrajBuffer* buff) {
  pthread_mutex_lock(&(buff->my_lock));
  buff->low_pos = buff->high_pos = 0;
  pthread_mutex_lock(&(buff->my_lock));
}

#define TRAJ_BUFF_SIZE 1800000
#define SOCK_BUFF_SIZE (1 << 20)
#define TRAJ_ACTION_END 0
#define TRAJ_ACTION_CLEAN 1
#define TRAJ_ACTION_PUSH 2

static TrajBuffer* buff;
static pthread_t traj_server_thread;

int traj_server_push(const char* sock_buff, TrajBuffer* traj_buff) {
  const unsigned int *nelem = (const unsigned int*)sock_buff;
  const TrajStep* elems = (const TrajStep*)(sock_buff + sizeof(unsigned int));
  traj_buffer_push(traj_buff, elems, *nelem);
  return sizeof(unsigned int) + (*nelem)*sizeof(TrajStep);
}

void* traj_server_main(void* arg) {
  void *context = zmq_ctx_new();
  void* responder = zmq_socket(context, ZMQ_REP);
  char* sock_buffer = malloc(SOCK_BUFF_SIZE);
  int rc = zmq_bind(responder, "tcp://*:5555");
  int num_bytes, i;
  char finished;
  assert(rc == 0);

  while (1) {
    finished = 0;
    num_bytes = zmq_recv(responder, sock_buffer, SOCK_BUFF_SIZE, 0);
    for (i=0; i<num_bytes && !finished; i++) {
      switch (sock_buffer[i]) {
        case TRAJ_ACTION_END:
          finished = 1;
          break;
        case TRAJ_ACTION_CLEAN:
          traj_buffer_clean(buff);
          break;
        case TRAJ_ACTION_PUSH:
          i += traj_server_push(sock_buffer + (i + 1), buff);
          break;
        default:
          printf("Socket received action %d is not recognized\n", sock_buffer[i]);
          finished = 1;
          break;
      }
    }
    zmq_send(responder,"OK", 2, 0);
  }

  free(sock_buffer);
  return NULL;
}

int main() {
  int buff_size, pop_nelem, i, j;
  buff = traj_buff_new(TRAJ_BUFF_SIZE);
  if (pthread_create(&traj_server_thread, NULL, traj_server_main, NULL) != 0) {
    printf("Can't start server thread\n");
    return 1;
  }
  while (scanf("%d", &pop_nelem) != EOF) {
    buff_size = traj_buffer_size(buff);
    printf("Current number of elements %d\n", buff_size);
    if (buff_size < pop_nelem) {
      printf("Can't pop %d elements out of %d\n", pop_nelem, buff_size);
      continue;
    }
    TrajStep elements[pop_nelem];
    traj_buffer_pop(elements, buff, pop_nelem);
    for (i=0; i<pop_nelem; i++) {
      printf("T%d: ", i);
      for (j=0; j<N_DOFS; j++) {
        printf("j%d(%.3lf,%.3lf) ", j, elements[i].q[j], elements[i].qdot[j]);
      }
      printf("\n");
    }
  }
  printf("Locking in main code\n");
  pthread_join(traj_server_thread, NULL);
  traj_buff_delete(buff);
}
