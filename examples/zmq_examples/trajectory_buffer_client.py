
import zmq
import struct
import ctypes
import binascii

tcp_port = 5555

def pack_trajectory(qs,qdots):
  N = len(qs)
  #The first character in the format is for endianness (< little, > big, @ default)
  trajHead = struct.Struct('< b I')
  trajStep = struct.Struct('< 7d 7d')
  buff = ctypes.create_string_buffer(trajHead.size + N*trajStep.size)
  trajHead.pack_into(buff, 0, 2, N)
  for i in xrange(N):
    data = qs[i] + qdots[i]
    trajStep.pack_into(buff,trajHead.size + i*trajStep.size, *data)
  return buff.raw

def my_main():
  context = zmq.Context()
  socket = context.socket(zmq.REQ)
  uri = "tcp://localhost:{0}".format( tcp_port )
  socket.connect(uri)
  q = [[1.7967, -0.2344, -0.0941, 1.7391, -1.5738, 0.0445, 0.2699]]
  qdot = [[-0.0001, 0.4136, -0.1390, 0.6383, 0.1373, 0.2201, -0.1305]]
  data = pack_trajectory(q,qdot)
  print binascii.hexlify(data)
  socket.send(data)
  response = socket.recv()
  print str(response)

my_main()
