
#include <stdio.h>
#include <zmq.h>
#include <assert.h>

int main() {
  void *context = zmq_ctx_new();
  void* responder = zmq_socket(context, ZMQ_REP);
  int rc = zmq_bind(responder, "tcp://*:5555");
  int num_bytes;
  assert(rc == 0);

  while (1) {
    char buffer[100];
    num_bytes = zmq_recv(responder, buffer, 100, 0);
    buffer[num_bytes] = '\0';
    printf("%s\n", buffer);
    zmq_send(responder,"Hello client!!!", 15, 0);
  }
}
