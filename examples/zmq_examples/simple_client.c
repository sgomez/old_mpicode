
#include <stdio.h>
#include <zmq.h>
#include <assert.h>

int main() {
  void *context = zmq_ctx_new();
  void* requester = zmq_socket(context, ZMQ_REQ);
  int rc = zmq_connect(requester, "tcp://localhost:5555");
  int num_bytes;
  assert(rc == 0);

  char buffer[100];
  zmq_send(requester, "Hello server!!!", 15, 0);
  num_bytes = zmq_recv(requester, buffer, 100, 0);
  buffer[num_bytes] = '\0';
  printf("%s\n", buffer);

}
