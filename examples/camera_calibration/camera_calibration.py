
import numpy as np


def get_2D_points(pts_3d, cam_matrix):
  n = np.shape(pts_3d)[0]
  hom_3d = np.concatenate((pts_3d.T,np.ones((1,n))), axis=0)
  hom_2d = np.dot(cam_matrix,hom_3d)
  pts_2d = np.zeros((n,2))
  for i in xrange(n):
    for j in xrange(2):
      pts_2d[i][j] = hom_2d[j][i] / hom_2d[2][i]
  return pts_2d

def get_cam_calibration(pts3d, pts2d):
  n = np.shape(pts3d)[0]
  A = np.zeros((2*n, 11))
  b = np.zeros(2*n)
  for i in xrange(n):
    for j in xrange(2):
      b[2*i + j] = pts2d[i][j]
      for k in xrange(3):
        A[2*i + j][4*j + k] = pts3d[i][k]
        A[2*i + j][8 + k] = -(pts3d[i][k]*pts2d[i][j])
      A[2*i + j][4*j + 3] = 1
  values = np.append(np.dot(np.linalg.pinv(A),b), 1)
  return np.reshape(values, (3,4))

cam1 = np.array([[289.37, 7.68, 3.697, 275.26],
  [1.4, -103.45, -256.4, -160.04],
  [0.05, 0.23, -0.102, 1]])
#p3d = np.array([[0.032,-1.63,-0.9]])
p3d = np.random.normal(0,1,(20,3))
print "3D points: ", p3d
p2d = get_2D_points(p3d, cam1)
print "2D points: ", p2d
p2dn = p2d + np.random.normal(0,5,(20,2))
print "2D noisy points: ", p2dn
est_mat = get_cam_calibration(p3d, p2dn)
print "Estimated matrix: ", est_mat
