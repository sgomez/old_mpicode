
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.linalg
import kalman

class Yanlong_A_Mat:
  def __init__(self, airDrag, bounceVec, deltaT, bounceZ):
    self.deltaT = deltaT
    self.bounceZ = bounceZ
    tableWidth = 1.526
    tableLenght = 2.74
    self.Xrange = [-0.5*tableWidth,0.5*tableWidth]
    self.Yrange = [-3.5,-3.5+tableLenght]
    self.__params = np.array([airDrag] + bounceVec)

  def flyMat(self, state, params=None):
    params = params if params!=None else self.params
    deltaT = self.deltaT
    V = state['prev_z'][[1,3,5]]
    airDrag = params[0]*np.linalg.norm(V)
    Adim = []
    for d in xrange(3):
      Adim.append( np.array([[1, deltaT - 0.5*airDrag*deltaT**2],[0,1-airDrag*deltaT]]) )
    Afly = scipy.linalg.block_diag(Adim[0],Adim[1],Adim[2])
    return Afly

  def bounceMat(self, state, params=None):
    params = params if params!=None else self.params
    deltaT = self.deltaT
    V = state['prev_z'][[1,3,5]]
    airDrag = params[0]*np.linalg.norm(V)
    bounceFac = params[1:4]
    Abounce = np.array([[1,deltaT - 0.5*airDrag*deltaT**2,0,0,0,0],[0,bounceFac[0],0,0,0,0], \
        [0,0,1,deltaT - 0.5*airDrag*deltaT**2,0,0], [0,0,0,bounceFac[1],0,0], \
        [0,0,0,0,-bounceFac[2],-bounceFac[2]*deltaT],[0,0,0,0,0,-bounceFac[2]]])
    return Abounce

  def isBounce(self, state):
    Afly = self.flyMat(state)
    Bfly = state['B'].flyMat(state)
    next_state = np.dot(Afly, state['prev_z']) + np.dot(Bfly, state['action'])
    next_z = next_state[4]
    if next_z <= self.bounceZ:
      next_x = next_state[0]
      next_y = next_state[2]
      return next_x>=self.Xrange[0] and next_x<=self.Xrange[1] and next_y>=self.Yrange[0] and next_y<=self.Yrange[1]
    return False

  def mat(self, state, params=None):
    if self.isBounce(state):
      return self.bounceMat(state, params)
    return self.flyMat(state,params)
  
  def n_params(self):
    return len(self.__params)

  def shape(self):
    return 6,6

  @property
  def params(self):
    return self.__params

  @params.setter
  def params(self, params):
    self.__params = params

  def deriv(self, state, param_id, params=None):
    params = params if params!=None else self.params
    deltaT = self.deltaT
    V = state['prev_z'][[1,3,5]]
    beta = params[0]
    bounceFac = params[1:4]
    ans = np.zeros((6,6))
    if self.isBounce(state):
      if param_id==0: #beta[0]
        ans[0,1] = -0.5*deltaT**2*np.lingalg.norm(V)
        ans[2,3] = -0.5*deltaT**2*np.linalg.norm(V)
      elif param_id==1: #bounceFac[0]
        ans[1,1] = 1
      elif param_id==2: #bounceFac[1]
        ans[3,3] = 1
      elif param_id==3: #bounceFac[2]
        ans[4,4] = -1
        ans[4,5] = -deltaT
        ans[5,5] = -1
      else:
        raise NotImplementedError
    else:
      if param_id >= 0 and param_id < 3:
        ans[2*param_id,2*param_id + 1] = -0.5*deltaT**2*np.linalg.norm(V)
        ans[2*param_id+1,2*param_id+1] = -deltaT*np.linalg.norm(V)
    return ans


class Ball_A_Mat:

  def __init__(self, airDrag, bounceVec, deltaT, bounceZ):
    self.deltaT = deltaT
    self.bounceZ = bounceZ
    tableWidth = 1.526
    tableLenght = 2.74
    self.Xrange = [-0.5*tableWidth,0.5*tableWidth]
    self.Yrange = [-3.5,-3.5+tableLenght]
    self.__params = np.array(airDrag + bounceVec)

  def flyMat(self, state, params=None, **opt_par):
    params = params if params!=None else self.params
    deltaT = self.deltaT if not 'deltaT' in opt_par else opt_par['deltaT']
    beta = params[0:3]
    Adim = []
    for d in xrange(3):
      Adim.append( np.array([[1, deltaT - 0.5*beta[d]*deltaT**2],[0,1-beta[d]*deltaT]]) )
    Afly = scipy.linalg.block_diag(Adim[0],Adim[1],Adim[2])
    return Afly

  def bounceMat(self, state, params=None, **opt_par):
    params = params if params!=None else self.params
    deltaT = self.deltaT if not 'deltaT' in opt_par else opt_par['deltaT']
    beta = params[0:3]
    bounceFac = params[3:6]
    Abounce = np.array([[1,deltaT - 0.5*beta[0]*deltaT**2,0,0,0,0],[0,bounceFac[0],0,0,0,0], \
        [0,0,1,deltaT - 0.5*beta[1]*deltaT**2,0,0], [0,0,0,bounceFac[1],0,0], \
        [0,0,0,0,1,0], [0,0,0,0,0,-bounceFac[2]]])
        #[0,0,0,0,-bounceFac[2],-bounceFac[2]*deltaT],[0,0,0,0,0,-bounceFac[2]]])
    return Abounce

  def isBounce(self, state):
    Afly = self.flyMat(state)
    Bfly = state['B'].flyMat(state)
    next_state = np.dot(Afly, state['prev_z']) + np.dot(Bfly, state['action'])
    next_z = next_state[4]
    if next_z <= self.bounceZ:
      next_x = next_state[0]
      next_y = next_state[2]
      return next_x>=self.Xrange[0] and next_x<=self.Xrange[1] and next_y>=self.Yrange[0] and next_y<=self.Yrange[1]
    return False

  def mat(self, state, params=None, **opt_par):
    if self.isBounce(state):
      return self.bounceMat(state, params, **opt_par)
    return self.flyMat(state,params,**opt_par)
  
  def n_params(self):
    return len(self.__params)

  def shape(self):
    return 6,6

  @property
  def params(self):
    return self.__params

  @params.setter
  def params(self, params):
    self.__params = params

  def deriv(self, state, param_id, params=None, **opt_par):
    params = params if params!=None else self.params
    deltaT = self.deltaT if not 'deltaT' in opt_par else opt_par['deltaT']
    beta = params[0:3]
    bounceFac = params[3:6]
    ans = np.zeros((6,6))
    if self.isBounce(state):
      if param_id==0: #beta[0]
        ans[0,1] = -0.5*deltaT**2
      elif param_id==1: #beta[1]
        ans[2,3] = -0.5*deltaT**2
      elif param_id==2: #beta[2]
        pass #derivative is zero
      elif param_id==3: #bounceFac[0]
        ans[1,1] = 1
      elif param_id==4: #bounceFac[1]
        ans[3,3] = 1
      elif param_id==5: #bounceFac[2]
        #ans[4,4] = -1
        #ans[4,5] = -deltaT
        ans[5,5] = -1
      else:
        raise NotImplementedError
    else:
      if param_id >= 0 and param_id < 3:
        ans[2*param_id,2*param_id + 1] = -0.5*deltaT**2
        ans[2*param_id+1,2*param_id+1] = -deltaT
    return ans

class Ball_B_Mat:
  def __init__(self, deltaT, bounceZ, bounceFac):
    self.deltaT = deltaT
    self.gravety = -9.8
    self.bounceZ = bounceZ
    self.bounceFac = bounceFac
    self.params = 0 #don't optimize anything here for the moment

  def flyMat(self, state, params=None, **opt_par):
    deltaT = self.deltaT if not 'deltaT' in opt_par else opt_par['deltaT']
    Bfly = np.array([[0],[0],[0],[0],[0.5*deltaT**2*self.gravety],[deltaT*self.gravety]])
    return Bfly

  def bounceMat(self, state, params=None, **opt_par):
    bounceZ = self.bounceZ
    bfac = self.bounceFac #state['A'].params[5]
    #Bbounce = np.array([[0],[0],[0],[0],[bounceZ*(1+bfac)],[0]])
    Bbounce = np.array([[0],[0],[0],[0],[0],[0]])
    return Bbounce

  def mat(self, state, params=None, **opt_par):
    if (state['A'].isBounce(state)):
      return self.bounceMat(state, params, **opt_par)
    return self.flyMat(state, params, **opt_par)
  
  def n_params(self):
    return len(self.__params)

  def shape(self):
    return 6,1

  @property
  def params(self):
    return self.__params

  @params.setter
  def params(self, params):
    self.__params = params

  def deriv(self, state, param_id, params, **opt_par):
    raise NotImplementedError


def save_ball_model(file_name, deltaT, kf, A, **params):
  f = file(file_name, "wb")
  np.savez(f, mu0=kf.mu0, P0=kf.P0, Sigma=kf.Sigma, Gamma=kf.Gamma, \
      Aparams=A.params, deltaT=deltaT)
  f.close()

def load_ball_model(file_name, **params):
  params.setdefault('a_mat', 'linear')
  f = file(file_name, 'rb')
  data = np.load(f)
  deltaT = data['deltaT']
  a_params = data['Aparams']
  bounceZ = -(1.71 - (0.76 + 0.02 + 0.02))
  if params['a_mat'] == 'linear':
    beta = a_params[0:3].tolist()
    bounceFac = a_params[3:6].tolist()
    A = Ball_A_Mat(beta,bounceFac,deltaT,bounceZ)
  elif params['a_mat'] == 'yanlong':
    airDrag = a_params[0]
    bounceFac = a_params[1:4].tolist()
    A = Yanlong_A_Mat(airDrag, bounceFac, deltaT, bounceZ)
  else: raise NotImplementedError
  B = Ball_B_Mat(deltaT, bounceZ, bounceFac[2])
  C = kalman.ConstMat(np.array([[1,0,0,0,0,0],[0,0,1,0,0,0],[0,0,0,0,1,0]]))
  kf = kalman.LinearKF(A, C, B=B)
  kf.mu0 = data['mu0']
  kf.P0 = data['P0']
  kf.Sigma = data['Sigma']
  kf.Gamma = data['Gamma']
  return kf, deltaT


def load_ball_data(file_path, real_set):
  #dropSet = set([17,20,22,23,24,29,56]) #some datasamples are no good
  #real_set = [i for i in range(15,66) if not i in dropSet]
  data = []
  for i in real_set:
      mat = np.loadtxt(file_path + "unifyData" + str(i) + ".txt")
      vision = mat[:,0:11]
      cam1 = []
      cam2 = []
      time1 = []
      time2 = []
      for t in xrange(np.shape(vision)[0]):
        time = vision[t][-1]
        if vision[t][1]==1:
          pos = vision[t][2:5]
          if len(cam1)==0 or not np.allclose(cam1[-1],pos):
            cam1.append(pos)
            time1.append(time)
        if vision[t][6]==1:
          pos = vision[t][7:10]
          if len(cam2)==0 or not np.allclose(cam2[-1],pos):
            cam2.append(pos)
            time2.append(time)
      data.append({'file_num': i, 'cam1': np.array(cam1), 'time1': np.array(time1), \
          'cam2': np.array(cam2), 'time2': np.array(time2)})
  return data

def segment_ball_trajectories(data, split_pts):
  split_data = []
  segm_data = {}
  for i in xrange(np.shape(split_pts)[0]):
    segm_data[split_pts[i][0]] = split_pts[i][1:]
  for instance in data:
    segm_inst = segm_data[instance['file_num']]
    pre_bounce_pos = []
    pre_bounce_time = []
    post_bounce_pos = []
    post_bounce_time = []
    cam1_ix = 0
    cam2_ix = 0
    while cam1_ix < np.shape(instance['cam1'])[0] and cam2_ix < np.shape(instance['cam2'])[0]:
      if instance['time1'][cam1_ix] == instance['time2'][cam2_ix]:
        ctime = instance['time1'][cam1_ix]
        cpos = (instance['cam1'][cam1_ix,:] + instance['cam2'][cam2_ix,:]) / 2.0
        cam1_ix += 1
        cam2_ix += 1
      elif instance['time1'][cam1_ix] < instance['time2'][cam2_ix]:
        ctime = instance['time1'][cam1_ix]
        cpos = instance['cam1'][cam1_ix,:]
        cam1_ix += 1
      else:
        ctime = instance['time2'][cam2_ix]
        cpos = instance['cam2'][cam2_ix,:]
        cam2_ix += 1
      if ctime > segm_inst[0] and ctime < segm_inst[1]:
        pre_bounce_pos.append(cpos)
        pre_bounce_time.append(ctime)
      elif ctime >= segm_inst[1] and ctime < segm_inst[2]:
        post_bounce_pos.append(cpos)
        post_bounce_time.append(ctime)
    while cam1_ix < np.shape(instance['cam1'])[0]:
      ctime = instance['time1'][cam1_ix]
      cpos = instance['cam1'][cam1_ix,:]
      cam1_ix += 1
      if ctime > segm_inst[0] and ctime < segm_inst[1]:
        pre_bounce_pos.append(cpos)
        pre_bounce_time.append(ctime)
      elif ctime >= segm_inst[1] and ctime < segm_inst[2]:
        post_bounce_pos.append(cpos)
        post_bounce_time.append(ctime)
    while cam2_ix < np.shape(instance['cam2'])[0]:
      ctime = instance['time2'][cam2_ix]
      cpos = instance['cam2'][cam2_ix,:]
      cam2_ix += 1
      if ctime > segm_inst[0] and ctime < segm_inst[1]:
        pre_bounce_pos.append(cpos)
        pre_bounce_time.append(ctime)
      elif ctime >= segm_inst[1] and ctime < segm_inst[2]:
        post_bounce_pos.append(cpos)
        post_bounce_time.append(ctime)
    split_data.append({"pre_bounce_time": np.array(pre_bounce_time),\
        "pre_bounce_pos": np.array(pre_bounce_pos),\
        "post_bounce_time": np.array(post_bounce_time),\
        "post_bounce_pos": np.array(post_bounce_pos), "file_num": instance['file_num']})
  return split_data

def estimate_deltaT(times):
  dsum = 0.0
  ssum = 0.0
  nval = 0
  for time in times:
    for i in xrange(len(time)-1):
      val = time[i+1] - time[i]
      dsum += val
      ssum += val**2
      nval += 1
  deltaT = dsum / nval
  print "deltaT=",deltaT," dev=",np.sqrt((ssum/nval) - deltaT**2)
  return deltaT

def plot_ball_trajectory(instance):
  plt.figure(1)
  axis = ['X','Y','Z']
  for dim in xrange(3):
    plt.subplot(3,1,dim+1)
    plt.plot(instance['time1'],instance['cam1'][:,dim],'rx')
    plt.plot(instance['time2'],instance['cam2'][:,dim],'bx')
    plt.ylabel(axis[dim])
  plt.xlabel('Time')
  plt.show()

def plot_segm_ball_trajectory(instance):
  plt.figure(1)
  axis = ['X','Y','Z']
  for dim in xrange(3):
    plt.subplot(3,1,dim+1)
    plt.plot(instance['pre_bounce_time'],instance['pre_bounce_pos'][:,dim],'rx')
    plt.plot(instance['post_bounce_time'],instance['post_bounce_pos'][:,dim],'bx')
    plt.ylabel(axis[dim])
  plt.xlabel('Time')
  plt.show()



