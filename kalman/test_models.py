
import numpy as np
import matplotlib.pyplot as plt

def sim_physic_sys(acc_eq, x0, v0, deltaT, T):
  t = 0.0
  x = x0
  v = v0
  ans = []
  while t<T:
    a = acc_eq(x,v)
    ans.append([t,x,v])
    x = x + v*deltaT + 0.5*deltaT**2*a
    v = v + deltaT*a
    t += deltaT
  ans.append([t,x,v])
  return np.array(ans)

def test_model():
  g = -9.8
  lin_model = lambda x, v: g - 0.26*v
  #model2 = lambda x, v: g - 0.7*v
  yanlong_model = lambda x, v: g - 0.1414*v**2
  x0 = 0.58
  v0 = 0.0
  T = 0.35

  s1 = sim_physic_sys(lin_model, x0, v0, 1.0/60.0, T)
  s2 = sim_physic_sys(lin_model, x0, v0, 1e-3, T) #1.0/500.0, 0.4)
  s3 = sim_physic_sys(yanlong_model, x0, v0, 1e-3, T)

  plt.plot(s1[:,0], s1[:,1], 'r')
  plt.plot(s2[:,0], s2[:,1], 'b')
  plt.plot(s3[:,0], s3[:,1], 'g')
  plt.show()

test_model()
