
import numpy as np
import kalman as kalman
import matplotlib.pyplot as plt
import ball_model
import random
import os
import sys
sys.path.append('../proMP')
import proMP
import barret
import time
import ball_hit_utils as hit_utils
import ball_data

def gen_ball_data(N,T=60):
  deltaT = 0.016039288
  bounceZ = -(1.71 - (0.76 + 0.02)) 
  A = ball_model.Ball_A_Mat([0.3,0.3,0.3],[0.84263225, 0.8481574, 0.83570056],deltaT,bounceZ)
  B = ball_model.Ball_B_Mat(deltaT,bounceZ,0.83)
  C = kalman.ConstMat(np.array([[1,0,0,0,0,0],[0,0,1,0,0,0],[0,0,0,0,1,0]]))
  mu0 = np.array([0.62383662, -1.85340228, -3.82161499, 4.32921207, -0.61743292, 1.48630307])
  P0 = np.array([[ 0.03552352, -0.02081283, -0.08411745,  0.04575043, -0.00679479, 0.20797866],\
       [-0.02081283,  0.01324169,  0.04927737, -0.02594942,  0.00380298, -0.12261297],\
       [-0.08411745,  0.04927737,  0.19918896, -0.10833978,  0.01605169, -0.4925759 ],\
       [ 0.04575043, -0.02594942, -0.10833978,  0.06260651, -0.00961129, 0.26540026],\
       [-0.00679479,  0.00380298,  0.01605169, -0.00961129,  0.00196223, -0.03808301],\
       [ 0.20797866, -0.12261297, -0.4925759 ,  0.26540026, -0.03808301, 1.22247041]])
  kf = kalman.LinearKF(A,C,B=B)
  kf.init_params(init_state_mean=mu0,init_state_cov=P0,init_Sigma=1e-5*np.eye(3),init_Gamma=1e-6*np.eye(6))
  X = []
  U = []
  for i in xrange(N):
    Tn = random.randint(60,80)
    Un = np.ones((Tn,1))
    Xn = kf.generate_sample(Tn,actions=Un)
    U.append(Un)
    X.append(Xn)
  return X,U,A,B,C

def get_data_EM(segm_data, key_value):
  X = []
  U = []
  for instance in segm_data:
    Xn = instance[key_value]
    Un = np.ones((np.shape(Xn)[0],1))
    X.append(Xn)
    U.append(Un)
  return X, U

def exec_after_E_step(em_obj, X, U, means, covs):
  t=np.linspace(0,5,50)
  filter_means = np.array(means[0])[:,0]
  filter_dev = np.sqrt(np.array(covs[0])[:,0,0])
  noise_dev = np.sqrt(em_obj.Sigma[0,0])
  plt.plot(t,X[0],'g')
  plt.plot(t,filter_means,'r')
  plt.fill_between(t, filter_means - noise_dev, filter_means + noise_dev, facecolor='gray')
  plt.fill_between(t, filter_means - filter_dev, filter_means + filter_dev, facecolor='blue')
  plt.show()

def plot_ball_E_step(em_obj, X, U, means, covs, inst_id, time):
  inst_id = 0
  positions = X[inst_id]
  Tn,D = np.shape(positions)
  filter_means = np.zeros((Tn,D))
  filter_dev = np.zeros((Tn,D))
  noise_dev = np.zeros((Tn,D))
  axis = "XYZ"
  for t in xrange(Tn):
    for d in xrange(D):
      filter_means[t][d] = means[inst_id][t][2*d]
      filter_dev[t][d] = np.sqrt(covs[inst_id][t][2*d,2*d])
      noise_dev[t][d] = np.sqrt(em_obj.Sigma[d,d])
  plt.figure(1)
  for d in xrange(D):
    plt.subplot(D,1,d)
    plt.plot(time,positions[:,d],'g.')
    plt.plot(time,filter_means[:,d],'r')
    plt.fill_between(time, filter_means[:,d] - noise_dev[:,d], filter_means[:,d] + noise_dev[:,d], facecolor='gray')
    plt.fill_between(time, filter_means[:,d] - filter_dev[:,d], filter_means[:,d] + filter_dev[:,d], facecolor='blue')
    plt.ylabel(axis[d])
  plt.xlabel('Time')
  plt.show()

def get_full_EM_data(segm_data):
  X1,U1 = get_data_EM(segm_data, 'pre_bounce_pos')
  X2,U2 = get_data_EM(segm_data, 'post_bounce_pos')
  N = len(X1)
  X = []
  U = []
  for n in xrange(N):
    xn = X1[n].tolist() + X2[n].tolist()
    un = U1[n].tolist() + U2[n].tolist()
    X.append(np.array(xn))
    U.append(np.array(un))
  return X,U


def ball_example_E_step_callback(emobj, X, U, means, covs, plot_inst, time_inst, do_plot=True):
  if (do_plot):
    plot_ball_E_step(emobj,X,U,means,covs,plot_inst,time_inst)
  emobj.B.bounceFac = emobj.A.params[5]

def ball_example(do_data_plot=False, do_EM_plot=False, plot_inst=0, max_iter=10):
  data = ball_model.load_ball_data("./ball_data/",xrange(2,11))
  segm_data = ball_model.segment_ball_trajectories(data, np.loadtxt('./ball_data/vision_split.txt'))
  beta = [0.2,0.2,0.2]
  bounceFac = [0.8,0.8,0.8]
  gravety = -9.8
  ballHeightCorrection = 0.02
  bounceZ = -(1.71 - (0.76 + 0.02 + ballHeightCorrection))
  deltaT = ball_model.estimate_deltaT(map(lambda x: x['pre_bounce_time'], segm_data)) / 1000.0 #from milliseconds to seconds
  mu0 = np.zeros(6)
  P0 = np.eye(6)
  Sigma = 0.1*np.eye(3)
  Gamma = 1e-4*np.eye(6)
  if os.path.isfile('./full_ball_em_saved.npz'):
    f = file('./full_ball_em_saved.npz','rb')
    data = np.load(f)
    beta = data['Aparams'][0:3].tolist()
    bounceFac = data['Aparams'][3:6].tolist()
    deltaT = data['deltaT']
    mu0 = data['mu0']
    P0 = data['P0']
    Sigma = data['Sigma']
    Gamma = data['Gamma']
  
  X,U = get_full_EM_data(segm_data)
  A = ball_model.Ball_A_Mat(beta, bounceFac, deltaT, bounceZ)
  B = ball_model.Ball_B_Mat(deltaT, bounceZ, bounceFac[2])
  C = kalman.ConstMat(np.array([[1,0,0,0,0,0],[0,0,1,0,0,0],[0,0,0,0,1,0]]))
  if do_data_plot:
    for n in xrange(N):
      Tn = np.shape(X[n])[0]
      time = np.array(range(Tn))*0.016
      plt.figure(1)
      axis = ['X','Y','Z']
      for dim in xrange(3):
        plt.subplot(3,1,dim+1)
        plt.plot(time,X[n][:,dim],'rx')
        plt.ylabel(axis[dim])
      plt.xlabel('Time')
      plt.show()
  get_time = lambda a: np.array(a['pre_bounce_time'].tolist() + a['post_bounce_time'].tolist())
  em_callback = lambda emobj, X, U, means, covs: ball_example_E_step_callback(emobj, X, U, \
      means, covs, plot_inst, get_time(segm_data[plot_inst]), do_EM_plot)
  kf = kalman.LinearKF(A, C, B=B)
  kf.EM_train(X, U, max_iter=max_iter, init_Gamma=Gamma, init_Sigma=Sigma, P0=P0, mu0=mu0,\
      debug_LB=False, em_print_params=False, print_lowerbound=True,\
      optimize_A=True, check_grad=False, verbose=False, \
      Abounds=[(0,10),(0,10),(0,10),(0,1),(0,1),(0,1)], \
      callback_after_E_step = em_callback)

  print "mu0=", kf.mu0
  print "P0=", kf.P0
  print "Sigma=", kf.Sigma
  print "Gamma=", kf.Gamma
  A_theta = A.params
  print "air_drag=", A_theta[0:3]
  print "bouncing=", A_theta[3:6]
  ball_model.save_ball_model('full_ball_em.npz', deltaT, kf, A)


def promp_opt_hit_manifold(ball_means, ball_covs, ball_times, promp_robot, overlap, kinematics, inv_kin, **params):
  opt_T = 0.5 #Do proMP strike in this time (seconds)
  proMP_fun = lambda t: hit_utils.proMP_function(promp_robot, t, **params)

  t1 = time.time()
  start_times, start_values = overlap.start_time_convolution(opt_T,ball_means,ball_covs,ball_times,proMP_fun)
  t0 = start_times[np.argmax(start_values)]
  t2 = time.time()
  print "Time of optimal overlap: ", t2 - t1
  rmeans, rcovs, rorient, ball_offset = overlap.compute_robot_trajectory(t0, opt_T, ball_times, proMP_fun) #get robot end effector dist
  overlaps = overlap.trajectory_overlap(ball_means, ball_covs, rmeans, rcovs, ball_offset)
  #return t0, opt_T, start_times, start_values, promp_robot, proMP_function

  #Now the conditioning part
  cond_ixs = hit_utils.top_k_hitting_points(overlaps, 1)
  cond_promp = promp_robot
  #print "Number of cond. pts: ", len(cond_ixs)
  t1 = time.time()
  to_save = {'ball_means': ball_means, 'ball_covs': ball_covs, 'ball_times': ball_times, 'overlaps': overlaps, \
      'start_times': start_times, 'start_values': start_values, 't0': t0, 'prior_rmeans': rmeans, \
      'prior_rcovs': rcovs, 'prior_rorient': rorient, 'ball_offset': ball_offset, 'cond_ixs': cond_ixs}

  for rob_ix in cond_ixs:
    cond_ix = rob_ix + ball_offset
    hit_time = ball_times[cond_ix]
    rob_time = (hit_time - t0) / opt_T
    mu_th, Sig_th = proMP_fun(rob_time)
    #print "prior: t=", hit_time, "mu=", mu_th, "S=", Sig_th
    #print "lhood: mu_x=", ball_means[cond_ix], " S_x=", ball_covs[cond_ix]
    mu_th2, Sig_th2 = inv_kin.inv_kin(mu_th, Sig_th, ball_means[cond_ix], ball_covs[cond_ix])
    #print "posterior: mu=", mu_th2, "S=", Sig_th2
    mu_x1, jac_x1, ori_x1 = kinematics.position_and_jac(mu_th)
    mu_x2, jac_x2, ori_x2 = kinematics.position_and_jac(mu_th2)
    jac_x1 = jac_x1[0:3,:]
    jac_x2 = jac_x2[0:3,:]
    #print "fwd-kin prior: mu=", mu_x1, " S=", np.dot(jac_x1, np.dot(Sig_th, jac_x1.T))
    #print "fwd-kin posterior: mu=", mu_x2, " S=", np.dot(jac_x2,np.dot(Sig_th2, jac_x2.T))
    to_save.update({'mu_th': mu_th2, 'Sigma_th': Sig_th2, 'rob_time': rob_time})
    cond_promp = cond_promp.condition(rob_time, pos_t=mu_th2, Sigma_pos=Sig_th2) #Condition on the posterior given by inv_kin
    proMP_fun = lambda t: hit_utils.proMP_function(cond_promp, t, **params)
  t2 = time.time()
  print "Conditioning time: ", t2 - t1
  fout = file('test_kalman_step.npz', 'wb')
  np.savez(fout, **to_save)
  fout.close()
  return t0, opt_T, start_times, start_values, overlaps, cond_promp, proMP_fun

def ball_predict_example(instances,**params):
  params.setdefault('promp_type', 'full')
  params.setdefault('kernel_type', 'sqexp')
  params.setdefault('model_fname', '../proMP/saved_models/' + params['promp_type'] + '_' + params['kernel_type'] + '_promp.npz')
  #params.setdefault('kfilter_fname', 'saved_data/full_ball_em.npz')
  params.setdefault('kfilter_fname', 'saved_data/trained_kf.npz')
  params.setdefault('a_mat', 'linear')
  params.setdefault('zero_init_vel', True)
  params.setdefault('nobs', 5)
  params.setdefault('plot_std_mult', 2.0)
  data = ball_model.load_ball_data("./ball_data/",instances)
  vision_split = np.loadtxt('./ball_data/vision_split.txt')
  segm_data = ball_model.segment_ball_trajectories(data, vision_split)
  X,U = get_full_EM_data(segm_data)
  #kf, deltaT = ball_model.load_ball_model('full_ball_em.npz')
  kf, deltaT = ball_model.load_ball_model(params['kfilter_fname'], **params)
  kf.A.params = [0.6,0.6,0.1,0.9,0.9,0.8]
  kinematics = barret.BarretKinematics()
  inv_kin = proMP.ProbInvKinematics(kinematics)
  overlap = proMP.Robot_Ball_Overlap(kinematics)
  promp_stream = proMP.load_stream(params['model_fname'])
  #kernel_builder = proMP.proMP_Gauss_Basis.build_from_stream
  kernel_builder = proMP.KernelBuilder.get_stream_builder(params['kernel_type'])
  if params['promp_type'] == 'full':
    promp_robot = proMP.proMP_Dependent_Robot_Angles.build_from_stream(promp_stream, kernel_builder, proMP.proMP.build_from_stream)
  else:
    promp_robot = proMP.proMP_Indep_Robot_Angles.build_from_stream(promp_stream, kernel_builder, proMP.proMP.build_from_stream)
  zero_init_vel_promp = promp_robot.condition(0, vel_t=np.zeros(7), Sigma_vel=0.01*np.eye(7))
  if params['zero_init_vel']:
    robot_promp = zero_init_vel_promp
  promp_fun = lambda t: hit_utils.proMP_function(promp_robot, t, **params)
  N = len(X)
  axis = "XYZ"
  for n in xrange(N):
    rob_time = segm_data[n]['pre_bounce_time'].tolist() + segm_data[n]['post_bounce_time'].tolist()
    assert len(rob_time) == len(X[n])
    Tn = len(rob_time)
    Tp = int(round(1.2 / deltaT))
    rob_time = np.array(rob_time) / 1000.0
    rob_time -= rob_time[0]
    ptime = deltaT*np.array(range(Tp))
    for t in xrange(0,Tn,params['nobs']):
      filter_means = np.zeros((Tp,3))
      filter_dev = np.zeros((Tp,3))
      filter_covs = []
      Xn = X[n][0:t,:]
      ensdt_inst = ball_data.ensure_deltaT({'time': rob_time[0:t], 'X': Xn, 'observed': [True for i in xrange(len(Xn))], 'obs_set': range(len(Xn))}, deltaT=deltaT, tol=0.002)
      hidden = map(lambda x: not x, ensdt_inst['observed'])

      Un = np.ones((Tp,1))

      t1 = time.time()
      means,covs,Pvals,As,Bs,Cs,Ds = kf.forward_recursion(ensdt_inst['X'],Un,hidden=hidden,deltaTs=kf.get_delta_Ts(ensdt_inst['time']))
      for tp in xrange(Tp):
        filter_cov = np.zeros((3,3))
        for d in xrange(3):
          filter_means[tp][d] = means[tp][2*d]
          for d2 in xrange(3):
            filter_cov[d,d2] = covs[tp][2*d,2*d2]
          filter_dev[tp][d] = np.sqrt(filter_cov[d,d])
        filter_covs.append(filter_cov)

      t0, opt_T, start_times, start_values, overlaps1, cond_promp, new_promp_fun = promp_opt_hit_manifold(filter_means,\
          filter_covs, ptime, promp_robot, overlap, kinematics, inv_kin, **params)
      rmeans_old, rcovs_old, rorient_old, ball_offset_old = overlap.compute_robot_trajectory(t0, opt_T, ptime, promp_fun)
      rmeans, rcovs, rorient, ball_offset = overlap.compute_robot_trajectory(t0, opt_T, ptime, new_promp_fun) #get robot end effector dist
      overlaps2 = overlap.trajectory_overlap(filter_means, filter_covs, rmeans, rcovs, ball_offset)
      t2 = time.time()
      print "Computation time: ", t2 - t1

      pred_dist_time, pred_dist_val = hit_utils.rob_ball_dist(ptime, filter_means, rmeans, t0)
      real_dist_time, real_dist_val = hit_utils.rob_ball_dist(rob_time - rob_time[0], X[n], rmeans, t0)

      print "Minimum distance - Robot - Racket: Real Ball = ", min(real_dist_val), " Predicted Ball = ", min(pred_dist_val)

      rtime = ptime[ball_offset:ball_offset+len(rmeans)]
      rob_dev = np.zeros((len(rtime),3))
      rob_dev_old = np.zeros((len(rtime),3))
      for tr in xrange(len(rtime)):
        for d in xrange(3):
          rob_dev[tr,d] = np.sqrt(rcovs[tr][d,d])
          rob_dev_old[tr,d] = np.sqrt(rcovs_old[tr][d,d])
      rmeans = np.array(rmeans)
      rmeans_old = np.array(rmeans_old)
      #print "Robot deviations: ", rob_dev
      ax1 = plt.subplot(6,1,1)
      plt.plot(start_times, start_values)
      plt.xlabel('Start times')
      plt.ylabel('Objective function')
      std_mul = params['plot_std_mult']
      for d in xrange(3):
        plt.subplot(6,1,d+2,sharex=ax1)
        plt.plot(rob_time[0:t],Xn[:,d],'gx')
        plt.plot(rob_time[t:Tn],X[n][t:Tn,d],'rx')
        plt.plot(ptime,filter_means[:,d],'b')
        plt.fill_between(ptime[:], filter_means[:,d]-std_mul*filter_dev[:,d], filter_means[:,d]+std_mul*filter_dev[:,d], facecolor='blue', alpha=0.5)
        plt.plot(rtime, rmeans[:,d], 'g')
        plt.fill_between(rtime, rmeans[:,d]-std_mul*rob_dev[:,d], rmeans[:,d]+std_mul*rob_dev[:,d], facecolor='green', alpha=0.5)
        plt.plot(rtime, rmeans_old[:,d], 'r')
        plt.fill_between(rtime,rmeans_old[:,d]-std_mul*rob_dev_old[:,d],rmeans_old[:,d]+std_mul*rob_dev_old[:,d],facecolor='red',alpha=0.5)
        plt.ylabel(axis[d])
      plt.subplot(6,1,5,sharex=ax1)
      plt.plot(rtime, overlaps1, 'b')
      plt.ylabel('Prior overlap')
      plt.subplot(6,1,6,sharex=ax1)
      plt.plot(pred_dist_time, pred_dist_val, 'b')
      plt.plot(real_dist_time, real_dist_val, 'r')
      plt.ylabel('Ball - racket distance')
      plt.xlabel('Time')
      plt.show()

def test1(do_plot=True):
  X,U,A,B,C = gen_ball_data(20)
  times = []
  deltaT = 0.016
  if do_plot:
    for i in xrange(5):
      Tn = np.shape(X[i])[0]
      time = np.array(range(Tn))*0.016
      plt.figure(1)
      axis = ['X','Y','Z']
      for dim in xrange(3):
        plt.subplot(3,1,dim+1)
        plt.plot(time,X[i][:,dim],'rx')
        plt.ylabel(axis[dim])
      plt.xlabel('Time')
      plt.show()
  kf = kalman.LinearKF(A,C,B=B)
  plot_inst = 0
  time_inst = np.array(range(np.shape(X[plot_inst])[0])) * deltaT
  A.params = np.array([0.01,0.01,0.01,1,1,0.8])
  kf.EM_train(X, U, max_iter=20, init_Gamma=1e-2*np.eye(6), init_Sigma=0.1*np.eye(3),\
      P0=np.eye(6), debug_LB=True, em_print_params=False, print_lowerbound=True,\
      optimize_A=True, check_grad=False, verbose=False,\
      A_bounds=[(0,10),(0,10),(0,10),(0,1),(0,1),(0,1)], \
      callback_after_E_step = (lambda emobj,X,U,means,covs: plot_ball_E_step(emobj,X,U,means,covs,plot_inst,time_inst)))
  print "mu0=", kf.mu0
  print "P0=", kf.P0
  print "Sigma=", kf.Sigma
  print "Gamma=", kf.Gamma
  print "Aparams=", A.params


#ball_example(False,False,0,5)
ball_predict_example(range(2,11))
