
import numpy as np
#import kalman as kalman
import matplotlib.pyplot as plt
#import ball_model
#import random
import os
import sys
sys.path.append('../proMP')
#import proMP
#import barret
#import time
import ball_hit_utils as hit_utils
import ball_data
import re

def load_raw_ball_traj(**params):
  params.setdefault('folder', 'raw_traj/')
  params.setdefault('last_timestamp', '00000000_000000')
  ball_data = []
  ts_regex = re.compile('\d{8}_\d{6}')
  new_timestamp = params['last_timestamp']
  for fname in os.listdir(params['folder']):
    if not fname.endswith('.npz'): continue
    curr_time = ts_regex.search(fname).group()
    if curr_time > params['last_timestamp']:
      f = file(params['folder'] + fname, 'rb')
      data = np.load(f)
      ball_data.append({'time_stamp': curr_time, 'obs': data['ball']})
      if curr_time > new_timestamp: new_timestamp = curr_time
  return ball_data, new_timestamp

def append_raw_ball_traj(**params):
  params.setdefault('file_name', 'saved_data/raw_ball_traj.npz')
  data = {}
  if os.path.isfile(params['file_name']):
    f = file(params['file_name'], 'rb')
    old_data = np.load(f)
    time_stamp = old_data['time_stamp']
    new_data, new_timestamp = load_raw_ball_traj(last_timestamp=time_stamp, **params)
    if len(new_data) == 0:
      print "Everything up to date"
      return
    print "Appending ", len(new_data), " new ball observation to ", params['file_name'], \
        ". Now you have ", len(new_data) + len(old_data['ball']), " observations"
    data['time_stamp'] = new_timestamp
    new_data = list(old_data['ball']) + new_data
    data['ball'] = new_data
    f.close()
  else:
    new_data, new_timestamp = load_raw_ball_traj(**params)
    print "Saving ", len(new_data), " new ball observation to a new file ", params['file_name']
    data['time_stamp'] = new_timestamp
    data['ball'] = new_data
  f = file(params['file_name'], 'wb')
  np.savez(f, **data)

def process_raw_data(**params):
  params.setdefault('raw_fname', 'saved_data/raw_ball_traj.npz')
  params.setdefault('split_merge_func', ball_data.split_merge_raw_data)
  params.setdefault('mark_outliers', ball_data.ransac_mark_outliers)
  params.setdefault('processed_fname', 'saved_data/processed_ball.npz')
  f_in = file(params['raw_fname'], 'rb')
  raw_data = np.load(f_in)
  print "Processing ", params['raw_fname'], " with ", len(raw_data['ball']), " instances..."
  merged_ball_data = params['split_merge_func'](raw_data['ball'])
  filtered_balls = map(lambda inst: params['mark_outliers'](inst, **params), \
      merged_ball_data)
  to_save = {'time_stamp': raw_data['time_stamp'], 'ball': filtered_balls}
  f_in.close()
  f_out = file(params['processed_fname'], 'wb')
  np.savez(f_out, **to_save)
  print "Done"

#Given a processed file, produce a file with a similar format making sure the deltaT values
#are in a certain range of a desired value
def ensure_deltaT(**params):
  params.setdefault('original_file', 'saved_data/processed_ball.npz')
  params.setdefault('result_file', 'saved_data/similar_deltaT.npz')
  params.setdefault('deltaT', 0.0165)
  params.setdefault('tol', 0.002)
  f_in = file(params['original_file'], 'rb')
  f_out = file(params['result_file'], 'wb')
  in_data = np.load(f_in)
  print "Processing ", params['original_file'], " with ", len(in_data['ball']), " instances..."
  ans = []
  for inst in in_data['ball']:
    ans.append(ball_data.ensure_deltaT(inst, **params))
  to_save = {'time_stamp': in_data['time_stamp'], 'ball': ans}
  np.savez(f_out, **to_save)
  print "Done"

def plot_raw_ball_data(instance, **params):
  axis = "XYZ"
  plt.figure(1)
  red_time, red_pos = hit_utils.extract_incoming_ball_traj(instance['obs'], 1e100, cam_id=3)
  blue_time, blue_pos = hit_utils.extract_incoming_ball_traj(instance['obs'], 1e100, cam_id=1)
  for d in xrange(3):
    plt.subplot(3,1,d+1)
    plt.plot(red_time,red_pos[:,d],'rx')
    plt.plot(blue_time, blue_pos[:,d], 'bx')
    plt.ylabel(axis[d])
  plt.xlabel('Time')
  plt.show()

def plot_processed_ball_data(instance, **params):
  axis = "XYZ"
  plt.figure(1)
  X = instance['X']
  time = np.array(instance['time'])
  obs = instance['observed']
  blues = instance['obs_set']
  reds = np.nonzero(map(lambda x : not x, obs))[0]
  for d in xrange(3):
    plt.subplot(3,1,d+1)
    plt.plot(time[blues],X[blues,d],'bx')
    if len(reds) > 0:
      plt.plot(time[reds], X[reds,d], 'rx')
    plt.ylabel(axis[d])
  plt.xlabel('Time')
  plt.show()


def plot_ball(**params):
  params.setdefault('fname', 'saved_data/raw_ball_traj.npz')
  params.setdefault('plotter_func', plot_raw_ball_data)
  f = file(params['fname'], 'rb')
  data = np.load(f)
  balls = data['ball']
  params.setdefault('instances', range(len(balls)))
  #dts = []
  for i in params['instances']:
    #dts += map(lambda ix: balls[i]['time'][ix+1] - balls[i]['time'][ix], range(len(balls[i]['time'])-1))
    params['plotter_func'](balls[i], **params)
  #plt.hist(dts,30)
  #plt.show()

#ensure_deltaT()
#append_raw_ball_traj()
#plot_ball(fname='saved_data/processed_ball.npz', plotter_func=plot_processed_ball_data)
#plot_ball(fname='saved_data/raw_ball_traj.npz', plotter_func=plot_raw_ball_data)
#process_raw_data()

plot_ball(fname='saved_data/similar_deltaT.npz', plotter_func=plot_processed_ball_data)

