
import numpy as np
import kalman as kalman
import matplotlib.pyplot as plt
import ball_model
import random
import os
import sys
sys.path.append('../proMP')
import proMP
import barret
import time
import ball_hit_utils as hit_utils

def load_data(**params):
  params.setdefault('data_fname', 'saved_data/similar_deltaT.npz')
  f = file(params['data_fname'], 'rb')
  data = np.load(f)
  X = []
  U = []
  Hidden = []
  Times = []
  for inst in data['ball']:
    #if not reduce(lambda x,y: x and y, inst['observed']): continue
    X.append(inst['X'])
    Hidden.append( map(lambda x: not x, inst['observed']) )
    U.append( np.ones((len(X[-1]),1)) )
    Times.append(inst['time'])
  return X, U, Hidden, Times

def E_step_callback(emobj, X, U, means, covs):
#  if (do_plot):
#    plot_ball_E_step(emobj,X,U,means,covs,plot_inst,time_inst)
#  emobj.B.bounceFac = emobj.A.params[5]
  pass

def get_cov_prior(std_diag, v):
  return {'v': v, 'invS0': np.diag(map(lambda x: x**2, std_diag))*(v+len(std_diag)+1)}

def train_kalman(**params):
  params.setdefault('model_fname', 'saved_data/trained_kf.npz')
  params.setdefault('max_iter', 10)
  params.setdefault('optimize_A', False)
  params.setdefault('print_lowerbound', True)
  params.setdefault('debug_LB', True)
  params.setdefault('check_grad', False)
  params.setdefault('a_mat', 'linear')
  params.setdefault('prior_P0', get_cov_prior([0.5,1,1.5,1,1,1], 50))
  params.setdefault('prior_Sigma', get_cov_prior([0.01,0.01,0.01], 20))
  params.setdefault('prior_Gamma', get_cov_prior([1e-6 for i in xrange(6)], 7))
  X, U, Hidden, Times = load_data(**params)
  if os.path.isfile(params['model_fname']):
    f = file(params['model_fname'],'rb')
    data = np.load(f)
    beta = data['Aparams'][0:3].tolist()
    bounceFac = data['Aparams'][3:6].tolist()
    #airDrag = data['Aparams'][0]
    #bounceFac = data['Aparams'][1:4]
    deltaT = data['deltaT']
    params.setdefault('mu0', data['mu0'])
    params.setdefault('P0', data['P0'])
    params.setdefault('init_Sigma', data['Sigma'])
    params.setdefault('init_Gamma', data['Gamma'])
  else:
    params.setdefault('mu0', np.zeros(6))
    params.setdefault('P0', np.eye(6))
    params.setdefault('init_Sigma', 1e-3*np.eye(3))
    params.setdefault('init_Gamma', 1e-6*np.eye(6))
    beta = [0.6,0.6,0.1] #air drag
    bounceFac = [0.9,0.9,0.8] #bounce factors
    #airDrag = 0.14
    deltaT = ball_model.estimate_deltaT(Times)
  ballHeightCorrection = 0.02
  bounceZ = -(1.71 - (0.76 + 0.02 + ballHeightCorrection))
  A = ball_model.Ball_A_Mat(beta, bounceFac, deltaT, bounceZ)
  #A = ball_model.Yanlong_A_Mat(airDrag, bounceFac, deltaT, bounceZ)
  B = ball_model.Ball_B_Mat(deltaT, bounceZ, bounceFac[2])
  C = kalman.ConstMat(np.array([[1,0,0,0,0,0],[0,0,1,0,0,0],[0,0,0,0,1,0]]))
  kf = kalman.LinearKF(A, C, B=B)
  kf.EM_train(X, U, Abounds=[(0,10),(0,10),(0,10),(0,1),(0,1),(0,1)], \
      hidden = Hidden, callback_after_E_step = E_step_callback, Time=Times, **params)
  print "mu0=", kf.mu0
  print "P0=", kf.P0
  print "Sigma=", kf.Sigma
  print "Gamma=", kf.Gamma
  A_theta = A.params
  print "air_drag=", A_theta[0:3]
  print "bouncing=", A_theta[3:6]
  ball_model.save_ball_model(params['model_fname'], deltaT, kf, A, **params)

train_kalman()
