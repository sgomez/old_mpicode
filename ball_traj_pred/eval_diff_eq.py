
import robpy.table_tennis.load_data as loader
import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import json
from train_diff_eq import poly_pos_and_vel, bounce_pos

def diff_eq_pred(x0, xd0, past_obs, times, air_drag=np.array([0.15,0.15,0.15]), 
        bounce_fac=np.array([0.9,0.9,0.8]), bounces_left=1):
    ans = []
    pos = x0
    vel = xd0
    prev_t = None
    for t,time in enumerate(times):
        if t<len(past_obs):
            ans.append(past_obs[t])
        else:
            deltaT = time-prev_t
            a = -air_drag*np.linalg.norm(vel)*vel + np.array([0,0,-9.8])
            pos = pos + vel*deltaT + 0.5*(deltaT**2)*a
            vel = vel + deltaT*a
            if pos[2] < 0.0 and bounces_left > 0:
                pos[2] *= -1.0
                vel[2] *= -1.0
                vel = vel*bounce_fac
                bounces_left -= 1
            ans.append(pos)
        prev_t = time
    return np.array(ans)


def test_pred(times, obs, is_bounce, nobs=20, nplot=5, poly_win=6, polyorder=2,
        air_drag=np.array([0.15,0.15,0.15]), 
        bounce_fac=np.array([0.9,0.9,0.8])):
    N,T,D = np.shape(obs)
    predictions = []
    errors = []
    for n,Xn in enumerate(obs):
        curr_nobs = nobs
        bounce_ix = bounce_pos(is_bounce[n])
        if bounce_ix is not None and bounce_ix<curr_nobs: curr_nobs = bounce_ix
        x,xd = poly_pos_and_vel(times[n][0:curr_nobs],Xn[0:curr_nobs],window_len=min(poly_win,curr_nobs),polyorder=polyorder)
        pred = diff_eq_pred(x[-1],xd[-1],Xn[0:curr_nobs],times[n],air_drag=air_drag,bounce_fac=bounce_fac)
        predictions.append(pred)
        diff = Xn - pred
        dist = np.linalg.norm(diff, axis=1)
        errors.append(dist)
    for n in range(nplot):
        time_obs = times[n]
        x_obs = np.array(obs[n])
        pred = predictions[n]
        plt.figure(1)
        for d in xrange(3):
            plt.subplot(3,1,d+1)
            plt.plot(time_obs,x_obs[:,d],'g.')
            plt.plot(time_obs,pred[:,d],'r')
            plt.axvline(time_obs[nobs])
        plt.show()
        diff = obs[n] - pred
        dist = np.linalg.norm(diff, axis=1)
        plt.plot(time_obs, dist)
        plt.axvline(time_obs[nobs])
        plt.show()
        #if n==3: break
    return {'pred': predictions, 'errors': errors, 'nobs': nobs}


def main(args):
    with open(args.data,'r') as f:
        data = np.load(f)
        times = data['times'][args.from_ix:args.to_ix]
        obs = data['obs'][args.from_ix:args.to_ix]
        is_bounce = data['is_bounce'][args.from_ix:args.to_ix]
    if args.save_pred and os.path.isfile(args.save_pred):
        ans = np.load(open(args.save_pred,'r'))
    else:
        ans = test_pred(times, obs, is_bounce, nobs=args.nobs, nplot=args.nplot, poly_win=args.poly_win, polyorder=args.polyorder,
                air_drag=np.array(args.air_drag), bounce_fac=np.array(args.bounce_fac))
        err = np.mean(ans['errors'])
        print("Avg. error: {}".format(err))
        ans['from_ix'] = args.from_ix
        ans['to_ix'] = args.to_ix
        if args.save_pred:
            np.savez(open(args.save_pred,'w'), **ans)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="Path to the consolidated data of ball trajectories")
    parser.add_argument('--nobs', type=int, default=20, help="List of segmentation meta data files")
    parser.add_argument('--from_ix', type=int, default=50, help="Use from this instance on")
    parser.add_argument('--to_ix', type=int, default=55, help="Use until this instance")
    parser.add_argument('--nplot', type=int, default=0, help="Number of instances to plot")
    parser.add_argument('--save_pred', help="File where predictions should be saved")
    parser.add_argument('--poly_win', type=int, default=6, help="Polynom window length")
    parser.add_argument('--polyorder', type=int, default=2, help="Polynom order")
    parser.add_argument('--air_drag', type=float, nargs=3, default=[0.15,0.15,0.15], help="Quadratic air drag coefficient")
    parser.add_argument('--bounce_fac', type=float, nargs=3, default=[0.9,0.9,0.8], help="Bouncing factors")
    args = parser.parse_args()
    main(args)
