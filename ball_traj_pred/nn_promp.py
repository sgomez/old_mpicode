
import numpy as np
import tensorflow as tf
import robpy.neural_process as rnp
import robpy.utils as utils
import robpy.deep_promp as deep_promp
import matplotlib.pyplot as plt

import argparse
import os
import json

def train_ball_promp(sess, fw_ball_traj, duration, min_z=-1e100, min_duration=0.8):
    feature_dims = 6
    encoder_type = 0
    if encoder_type==0:
        encoder = lambda x, is_training: tf.reshape(
                rnp.feature_mlp(x, is_training, hidden_units=[4,6,10,feature_dims], hidden_activation=tf.nn.tanh, batch_norm=False), 
                shape=(tf.shape(x)[0],tf.shape(x)[1],1,feature_dims)
                )
    elif encoder_type==1:
        init_centers = np.linspace(-0.1,1.1,feature_dims-1)
        init_scales = 6.5*np.ones(feature_dims-1) #10*np.random.rand(feature_dims-1)
        encoder = lambda x, is_training: tf.reshape(
                deep_promp.rbf(x, np.reshape(init_centers,(-1,1)), init_scales), 
                shape=(tf.shape(x)[0],tf.shape(x)[1],1,feature_dims)
                )
    prior_mu_w = None #{'m0': tf.zeros((feature_dims,1), dtype=tf.float64), 'k0': 1.0}
    #prior_Sigma_w = {'v0': feature_dims+1.0, 'mean_cov_mle': lambda v0, Sw_mle: tf.multiply(Sw_mle, tf.eye(feature_dims, dtype=tf.float64))}
    prior_Sigma_w = None #{'v0': feature_dims+1.0, 'mean_cov_mle': lambda v0, Sw_mle: 10*tf.eye(feature_dims, dtype=tf.float64)}
    dpmp = deep_promp.DeepProMP(in_dims=1, out_dims=1, feature_dims=feature_dims, phi=encoder, prior_mu_w=prior_mu_w, prior_Sigma_w=prior_Sigma_w)

    ball_pos = np.array(fw_ball_traj[1])
    N,T,D = ball_pos.shape
    x = np.array(fw_ball_traj[0]).reshape((N,T,1))
    y = ball_pos[:,:,2].reshape((N,T,1)) #take only z at the moment


    history = dpmp.fit(sess, x, y, lr=1e-5, init_phi_lr=1e-3, opt_basis_steps=10, em_batch_size=100, basis_batch_size=20, 
            iterations=20, init_phi_iter=10)#, noise_x=1e-3, noise_y=1e-2)
    if 'init_phi_loss' in history:
        phi_loss = history['init_phi_loss']
        plt.plot(phi_loss)
        plt.title('Loss of the basis function initialization procedure')
        plt.show()
    ob_loss = np.mean(history['opt_basis_losses'], axis=0)
    plt.plot(ob_loss)
    plt.show()
    plt.plot(history['loss'])
    plt.show()
    
    print "Mean Weights:\n", sess.run(dpmp.mu_w)
    print "Stdev Weights:\n", np.sqrt(np.diag( sess.run(dpmp.Sigma_w) ))
    print "Noise stdev:\n", np.sqrt(np.diag( sess.run(dpmp.Sigma_y) ))
    return dpmp

def test_pred(sess, fw_ball_traj, model, duration, nobs=20, min_z=-1e100, min_duration=0.8, n_plot=2):
    ball_pos = np.array(fw_ball_traj[1])
    N,T,D = ball_pos.shape
    x = np.array(fw_ball_traj[0]).reshape((N,T,1))
    y = ball_pos[:,:,2].reshape((N,T,1)) #take only z at the moment

    predictions = []
    errors = []
    e_step = model.comp_w_dist(sess, x[:,0:nobs], y[:,0:nobs])
    y_means, y_covs = model.output_dist(sess, x, w_dist=e_step)

    plt.title('Example of after training')
    for i in range(5):
        plt.plot(x[i,:,0],y[i,:,0],'rx')
        plt.plot(x[i,:,0],y_means[i,:,0,0])
        plt.show()
    return None

def main(args):
    with open(args.cons,'r') as f:
        data = np.load(f)
        fw_ball_traj = (data['times'][0:args.n_training], data['obs'][0:args.n_training], 
            data['is_bounce'][0:args.n_training])
        fw_ball_traj_test = (data['times'][args.test_from_ix:], data['obs'][args.test_from_ix:], 
            data['is_bounce'][args.test_from_ix:])
    duration = args.duration
    min_z = args.min_z
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        if not os.path.isfile(args.model):
            print("Starting the training procedure")
            model = train_ball_promp(sess, fw_ball_traj, duration=duration, min_z=min_z, min_duration=args.min_duration)
            #stream = model.to_stream()
            #data = {'promp': stream, 'duration': duration, 'min_z': min_z}
            #with open(args.model, 'w') as fout:
            #    json.dump(data, fout)
        else:
            with open(args.model, 'r') as fin:
                data = json.load(fin)
            model = promp.FullProMP(**data['promp'])
            duration = data['duration']
            min_z = data['min_z']
        ans = test_pred(sess, fw_ball_traj_test, model, duration=duration, nobs=args.test_nobs, min_z=min_z, min_duration=args.min_duration)
        #ans['from_ix'] = args.test_from_ix
        #ans['to_ix'] = args.test_from_ix+len(fw_ball_traj_test[0])
        #if args.save_pred:
        #    np.savez(open(args.save_pred,'w'), **ans)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('cons', help="Path to file with consolidated data")
    parser.add_argument('model', help="Path to the model file")
    parser.add_argument('--n_training', type=int, default=100, help="Number of training data to use")
    parser.add_argument('--test_from_ix', type=int, default=4900, help="Use from this index for testing")
    parser.add_argument('--duration', type=float, default=0.8, help="All ball trajectories will be modeled with this duration")
    parser.add_argument('--min_duration', type=float, default=0.7, help="All trajectories shorter than this are discarded")
    parser.add_argument('--min_z', type=float, default=-1e100, help="All observations lower than the given value would be removed")
    parser.add_argument('--test_nobs', type=int, default=20, help="Number of observation received by the trajectory prediction test")
    parser.add_argument('--save_pred', help="File where predictions and errors should be stored")
    args = parser.parse_args()
    main(args)
