import keras
import numpy as np
import argparse
import matplotlib.pyplot as plt
import os
from sklearn.preprocessing import StandardScaler

def seq_cnn_pred(model, obs, T, add_input=True, add_noise=None, Sigma_y=None, out_norm=None):
    s = obs.shape
    x = np.concatenate((obs,np.zeros((s[0],T,s[2]))), axis = 1)
    for t in xrange(T):
        px = model.predict(x[:,0:s[1]+t,:])
        noise = np.random.multivariate_normal(mean=[0,0,0], cov=Sigma_y, size=s[0])
        pred = px[:,-1,:]
        pred += noise
        if out_norm is not None: pred = out_norm.inverse_transform(pred)
        if add_input: pred += x[:,s[1]+t-1,:]
        #noise = 0.0 if add_noise is None else np.random.normal(loc=0, scale=add_noise, size=(s[0],s[2]))
        x[:,s[1]+t,:] = pred
    return x

def main(args):
    # Load data
    with open(args.data, 'r') as f:
        data = np.load(f)
        times = data['times']
        obs = data['obs']
        is_bounce = data['is_bounce']

    model_fname = "{}.json".format(args.model)
    model_w = "{}.h5".format(args.model)
    model_extra = "{}.npz".format(args.model)

    if os.path.isfile(model_fname) and os.path.isfile(model_w):
        json_file = open(model_fname, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        model = keras.models.model_from_json(loaded_model_json)
        # load weights into new model
        model.load_weights(model_w)

        extra = np.load(open(model_extra,'r'))
        normalizer = StandardScaler()
        out_norm = StandardScaler()
        #print extra['normalizer']
        #normalizer.set_params(**extra['normalizer'])
        Sigma_y = extra['Sigma_y']
        print(Sigma_y)

    N,T,D = np.shape(obs)
    normalizer.fit(np.reshape(obs,[-1,3]))
    all_obs = obs[args.start:args.end,:,:]
    all_obs_flat = np.reshape(all_obs, [-1,3])
    obs_norm_flat = normalizer.transform(all_obs_flat)
    all_obs_norm = np.reshape(obs_norm_flat, (-1,T,D))

    out_norm.fit(np.reshape(all_obs_norm[:,1:,:] - all_obs_norm[:,:-1,:], [-1,3]))

    past_norm = all_obs_norm[:,0:args.k,:]
    future = all_obs[args.start:args.end,args.k:,:]

    multi_pred = []
    print("Doing multiple predictions for all instances between {} and {}".format(args.start, args.end))
    for k in range(args.samples):
        predictions = seq_cnn_pred(model, past_norm, T-args.k, add_input=True, add_noise=None, Sigma_y=Sigma_y) #, out_norm=out_norm)
        multi_pred.append(normalizer.inverse_transform(predictions))
    multi_pred = np.array(multi_pred)
    predictions = np.mean(multi_pred,axis=0)
    print("Done.")
    diff = all_obs - predictions
    dist = np.linalg.norm(diff, axis=2)
    pred_dict = {'from_ix': args.start, 'to_ix': args.end, 'nobs': args.k, 'pred': predictions, 'errors': dist}
    if args.save_pred:
        np.savez(open(args.save_pred, 'w'), **pred_dict)
    avg_dist = np.mean(dist,axis=0)
    std_dist = np.std(dist,axis=0)

    print avg_dist

    plt.plot(avg_dist)
    plt.show()

    if args.plot: 
        for i in args.plot:
            curr_time = times[args.start+i]
            curr_ts = all_obs[i,:,:]
            curr_pred = multi_pred[:,i,:,:]
            #curr_pred = np.array([seq_cnn_pred(model, past[[i],:,:], T-args.k, add_input=True, add_noise=args.noise, Sigma_y=Sigma_y)[0] for j in range(10)])
            fig,ax = plt.subplots(nrows=3)
            for j,curr_ax in enumerate(ax):
                curr_ax.scatter(curr_time,curr_ts[:,j], color=(0.0,0.0,1.0), marker='o')
                curr_ax.plot(curr_time, predictions[i,:,j],color=(1.0,0,0,1.0))
                for k in range(args.samples):
                    curr_ax.plot(curr_time,curr_pred[k,:,j], color=(1.0,0,0,0.3))
            #plt.legend(['real', 'predicted'], loc='upper left')
            plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--model', help="Path to the cnn model")
    parser.add_argument('--data', default='ball_data_one_bounce.npz', help="Ball data")
    parser.add_argument('--k', default=30, type=int, help="Data allowed to see from the past")
    parser.add_argument('--end', default=10, type=int, help="Until which data index should this script use")
    parser.add_argument('--start', type=int, default=0, help="From which data index should this script use")
    parser.add_argument('--plot', nargs='+', type=int, help="Indices of instances to plot")
    parser.add_argument('--noise', type=float, help="Add noise to the predictions")
    parser.add_argument('--save_pred', help="File where the validation predictions are saved")
    parser.add_argument('--samples', type=int, default=10, help="Number of samples to draw for each time series")
    args = parser.parse_args()
    main(args)
