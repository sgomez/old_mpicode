""" Loads a ball model and tests the prediction accuracy
"""


import robpy.kalman as kalman
import robpy.table_tennis.ball_model as ball_model
import robpy.table_tennis.load_data as loader
import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import json

def test_pred(times, obs, is_bounce=None, kf=None, nobs=20, nplot=0):
    N,T,D = np.shape(obs)
    Xavg = obs
    X = Xavg.reshape([N,T,1,D])
    U = np.ones((N,T))
    Hidden = [[False for i in range(T)] for j in range(N)]
    Times = times
    predictions = []
    errors = []
    lhoods = []
    for n,Xn in enumerate(X):
        Tn = len(Xn)
        Un = np.ones((Tn,1))
        means, covs, P_vals, As, Bs, Cs, Ds = kf.forward_recursion(Xn[0:nobs],Un)
        pred = np.array(means)[:,[0,2,4]]
        predictions.append(pred)
        diff = Xavg[n] - pred
        dist = np.linalg.norm(diff, axis=1)
        errors.append(dist)
        lhoods.append(kf.loglikelihood([Xn],[Un]))
    for n in range(nplot):
        time_obs = Times[n]
        x_obs = np.array(Xavg[n])
        pred = predictions[n]
        plt.figure(1)
        for d in xrange(3):
            plt.subplot(3,1,d+1)
            plt.plot(time_obs,x_obs[:,d],'g.')
            plt.plot(time_obs,pred[:,d],'r')
            plt.axvline(time_obs[nobs])
        plt.show()
        diff = Xavg[n] - pred
        dist = np.linalg.norm(diff, axis=1)
        plt.plot(time_obs, dist)
        plt.axvline(time_obs[nobs])
        plt.show()
        #if n==3: break
    return {'pred': predictions, 'errors': errors, 'likelihoods': lhoods, 'nobs': nobs}


def main(args):
    with open(args.data,'r') as f:
        data = np.load(f)
        times = data['times'][args.from_ix:args.to_ix]
        obs = data['obs'][args.from_ix:args.to_ix]
        #is_bounce = data['is_bounce'][args.from_ix, args.to_ix]

    kf, deltaT = ball_model.load_ball_model(args.ball_model)
    if args.save_pred and os.path.isfile(args.save_pred):
        ans = np.load(open(args.save_pred,'r'))
    else:
        ans = test_pred(times, obs, is_bounce=None, kf=kf, nobs=args.nobs, nplot=args.nplot)
        ans['from_ix'] = args.from_ix
        ans['to_ix'] = args.to_ix
        if args.save_pred:
            np.savez(open(args.save_pred,'w'), **ans)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('ball_model', help="Path to the ball model")
    parser.add_argument('data', help="Path to the consolidated data of ball trajectories")
    parser.add_argument('--nobs', type=int, default=20, help="List of segmentation meta data files")
    parser.add_argument('--from_ix', type=int, default=50, help="Use from this instance on")
    parser.add_argument('--to_ix', type=int, default=55, help="Use until this instance")
    parser.add_argument('--nplot', type=int, default=0, help="Number of instances to plot")
    parser.add_argument('--save_pred', help="File where predictions should be saved") 
    args = parser.parse_args()
    main(args)
