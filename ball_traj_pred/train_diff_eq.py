
import argparse
import os
import autograd.numpy as np
from autograd import grad
import matplotlib.pyplot as plt
import json
from scipy import signal

import robpy.table_tennis.load_data as loader

def bounce_pos(is_bounce):
    for i,x in enumerate(is_bounce):
        if x: return i
    return None

def pos_and_vel(time, y, window_len=33, polyorder = 5):
    T,D = y.shape
    pos = np.zeros((T,D))
    vel = np.zeros((T,D))
    for d in range(D):
        pos[:,d] = signal.savgol_filter(y[:,d], window_len, polyorder)
        grad_d = np.gradient(pos[:,d], time)
        vel[:,d] = signal.savgol_filter(grad_d, window_len, polyorder) 
    return (pos, vel)

def poly_pos_and_vel(time, y, window_len=12, polyorder=2):
    T,D = y.shape
    assert(T >= window_len)
    start_ix = 0
    end_ix = window_len
    hw = window_len/2
    pos = np.zeros((T,D))
    vel = np.zeros((T,D))
    for i in range(T):
        if start_ix+hw > i and end_ix<T:
            start_ix += 1
            end_ix += 1
        xwin = time[start_ix:end_ix] - time[i]
        ywin = y[start_ix:end_ix]
        for d in range(D):
            p = np.polyfit(xwin, ywin[:,d], deg=polyorder)
            pos[i,d] = p[-1]
            vel[i,d] = p[-2]
    return pos, vel

def __add_iid_fly(t,x,v,l_in,l_out):
    for i,xi in enumerate(x):
        if i==0: continue
        vi = v[i]
        dt = t[i] - t[i-1]
        l_in.append(np.concatenate(([dt],x[i-1],v[i-1]), axis=0))
        l_out.append(np.concatenate((xi,vi),axis=0))

def create_iid_tset(times, obs, is_bounce, num_training=None, vel=None, window_len=50):
    N = len(times) if num_training is None else num_training
    fly_in = []
    fly_out = []
    bounce_in = []
    bounce_out = []
    for n in range(N):
        bounce_ix = bounce_pos(is_bounce[n])
        if bounce_ix is None: continue
        bounce_ix += 1
        t = times[n]
        x = obs[n]
        t1 = t[0:bounce_ix]
        t2 = t[bounce_ix:]
        if len(t1) < window_len or len(t2) < window_len: continue
        p1,v1 = poly_pos_and_vel(t1, x[0:bounce_ix], window_len=window_len)
        p2,v2 = poly_pos_and_vel(t2, x[bounce_ix:], window_len=window_len)
        __add_iid_fly(t1,p1,v1,fly_in,fly_out)
        __add_iid_fly(t2,p2,v2,fly_in,fly_out)
        bounce_in.append(np.concatenate(([t2[0]-t1[-1]],p1[-1],v1[-1]),axis=0))
        bounce_out.append(np.concatenate((p2[0],v2[0]),axis=0))
    return {'fly': {'in': np.array(fly_in), 'out': np.array(fly_out)}, 
            'bounce': {'in': np.array(bounce_in), 'out': np.array(bounce_out)}}

def fly_physics(params, in_state):
    air_drag = params[0:3]
    gravity = 9.8
    dt = in_state[:,0].reshape((-1,1))
    x = in_state[:,1:4]
    xd = in_state[:,4:]
    xdd = -air_drag*np.linalg.norm(xd)*xd + np.array([0,0,-gravity])

    next_x = x + dt*xd + 0.5*dt**2*xdd
    next_xd = xd + dt*xdd
    return np.concatenate((next_x,next_xd), axis=1)

def bounce_physics(params, in_state):
    bounce_fac = params[0:3]
    dt = in_state[:,0].reshape((-1,1))
    x = in_state[:,1:4]
    xd = in_state[:,4:]

    next_x = np.array([1,1,-1])*(x + dt*xd)
    next_xd = np.array([1,1,-1])*bounce_fac*xd
    return np.concatenate((next_x,next_xd), axis=1)


def train_physics(in_states, out_states, model, init_params, lr=0.01, batch_size=64, epochs=10):
    N = len(in_states)
    params = init_params
    for iteration in range(epochs):
        perm = np.random.permutation(N)
        perm_in_st = in_states[perm]
        perm_out_st = out_states[perm]
        errors = []
        gradients = []
        for batch_id in range(N/batch_size):
            batch_in = perm_in_st[batch_id*batch_size: (batch_id+1)*batch_size]
            batch_out = perm_out_st[batch_id*batch_size: (batch_id+1)*batch_size]
            loss = lambda params: np.mean((model(params,batch_in) - batch_out)**2)
            grad_loss = grad(loss)
            mse = loss(params)
            grad_mse = grad_loss(params)
            gradients.append(grad_mse)
            errors.append(mse)
            params -= grad_mse*lr
        print("Epoch {}: loss {}, params {}, avg grad: {}".format(iteration, np.mean(errors), params, np.mean(gradients, axis=0)))
    return params

def main(args):
    with open(args.data,'r') as f:
        data = np.load(f)
        times = data['times']
        #X = data['X']
        Xd = None #data['Xd']
        noisyX = data['obs'] #data['noisyX']
        #bounce_times = data['bounce_times']
        is_bounce = data['is_bounce']
    print(noisyX.shape)
    dset = create_iid_tset(times, noisyX, is_bounce, num_training=args.ntrain, vel=Xd)
    fly_params = train_physics(dset['fly']['in'], dset['fly']['out'], fly_physics, np.array([0.0,0.15,1.0]), lr=0.001, batch_size=128, epochs=25)
    print('In: {}'.format(dset['bounce']['in'][0:10]))
    print('Out: {}'.format(dset['bounce']['out'][0:10]))
    bounce_params = train_physics(dset['bounce']['in'], dset['bounce']['out'], bounce_physics, np.array([1.0,1.0,1.0]), lr=0.001, batch_size=128, epochs=500)
    print(fly_params)
    print(bounce_params)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="File where training files are stored")
    parser.add_argument('--ntrain', type=int, help="Number of training instances to use")
    args = parser.parse_args()
    main(args)
