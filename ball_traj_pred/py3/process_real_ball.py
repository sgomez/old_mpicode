""" Processes real ball data to have the required format

Processes the given real ball data to have all the same size by extending with
a given physical model, as well as making all the necessary changes to make
sure the real data has the same format as the simulation data.
"""

import argparse
import os
import json
import time

import numpy as np
import matplotlib.pyplot as plt

def create_ball_dataset(Times, Xreal, length=200, deltaT=1.0/180.0):
    N = len(Xreal)
    X = np.zeros((N,length,3))
    Xobs = np.zeros((N,length,1))
    for n in range(N):
        assert(len(Xreal[n]) == len(Times[n]))
        X[n,0] = Xreal[n][0]
        Xobs[n,0] = 1
        Tn = len(Xreal[n])
        t_ix = -1
        for i in range(1,Tn):
            assert(Times[n][i] > Times[n][i-1])
            t = Times[n][i] - Times[n][0]
            t_ix = int(round(t / deltaT))
            if t_ix >= length:
                break
            X[n,t_ix] = Xreal[n][i]
            Xobs[n,t_ix] = 1
    return X, Xobs


def main(args):
    Xreal = []
    Times = []
    for x in args.data:
        with open(x,'rb') as f:
            data = np.load(f, encoding='latin1')
            Xreal.extend(data['X'])
            Times.extend(data['Times'])
    print("Loaded {} ball trajectories. Permuting order...".format(len(Times)))
    p = np.random.permutation(len(Times))
    is_ok = lambda i: len(Times[i]) == len(Xreal[i]) and \
            len(Times[i])>args.min_duration and len(Times[i])<args.max_duration
    pTimes = [Times[i]-Times[i][0] for i in p if is_ok(i)]
    pX = [Xreal[i] for i in p if is_ok(i)]
    print("Using {} ball trajectories with more than {} observations".format(len(pTimes),args.min_duration))
    L = [len(t) for t in pTimes]
    plt.hist(L, bins=30)
    plt.title('Sample length of the time series')
    plt.show()

    deltaT = 1.0/180.0
    length = int(round(args.duration/deltaT))
    X, Xobs = create_ball_dataset(pTimes, pX, deltaT=deltaT, length=length)
    with open(args.out,'wb') as f:
        np.savez(f, X=X, Xobs=Xobs)
    #for i in range(15):
    #    plt.plot(pTimes[i],pX[i][:,2],'bo')
    #    plt.plot(deltaT*np.arange(length), X[i,:,2], 'rx')
    #    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', nargs='+', help="Files with the ball trajectory ball data")
    parser.add_argument('--out', default='out.npz', help="File where the processed data is saved")
    parser.add_argument('--duration', type=float, default=1.2, help="Total number of seconds each trajectory is extended to")
    parser.add_argument('--min_duration', type=int, default=100, help="Minimal length of time series to consider. Too short are usually outliers")
    parser.add_argument('--max_duration', type=int, default=500, help="Maximal length of time series to consider. Too long are usually outliers")
    args = parser.parse_args()
    main(args)
