 
import keras
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import argparse
import os
import json
import time

class TSAug(keras.utils.Sequence):
    """ Augments the data for time series modeling
    """

    def __init__(self, X, Xobs, batch_size=32, shuffle=True):
        self.X = X
        self.Xobs = Xobs
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.on_epoch_end()

    def on_epoch_end(self):
        self.indexes = np.arange(len(self.X))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_ids):
        Y = self.X[list_ids]
        Yobs = self.Xobs[list_ids]
        
        N,T,K = Y.shape        
        ts_lens = np.random.randint(low=0, high=T, size=N)
        is_obs = np.array([np.arange(T) < x for x in ts_lens])
        Xobs = Yobs*is_obs.reshape((self.batch_size,T,1))
        X = Xobs*Y

        return X, Xobs, Y, Yobs

    def __len__(self):
        x = np.floor(len(self.indexes) / self.batch_size)
        return int(x)

    def __getitem__(self, index):
        list_ids = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        X, Xobs, Y, Yobs = self.__data_generation(list_ids)
        return [X,Xobs], [Y]


def dense_encoder(x_size, z_size):
    encoder = keras.models.Sequential()
    encoder.add( keras.layers.Reshape( (4*x_size,) ) )
    encoder.add( keras.layers.Dense(1024, activation='relu') )
    encoder.add( keras.layers.Dense(z_size, activation='relu') )
    return encoder

def dense_decoder(z_size, y_size):
    decoder = keras.models.Sequential()
    decoder.add( keras.layers.Dense(1024, activation='relu') )
    decoder.add( keras.layers.Dense(y_size*3) )
    decoder.add( keras.layers.Reshape( (y_size,3) ) )
    return decoder

def train_ae(Xt, Xobs, Yt, Yobs):
    in_size = 200
    z_size = 64
    batch_size=128
    data_size,T,K = Xt.shape
    n_training = int(0.8*data_size)

    training_generator = TSAug(Xt[0:n_training], Xobs[0:n_training], batch_size=batch_size, shuffle=True)
    validation_generator = TSAug(Xt[n_training:], Xobs[n_training:], batch_size=batch_size, shuffle=True)


    x = keras.layers.Input(shape=(in_size,3))
    x_obs = keras.layers.Input(shape=(in_size,1))
    x_conc = keras.layers.concatenate([x, x_obs])

    encoder = dense_encoder(in_size, z_size)
    encoded = encoder(x_conc)

    decoder = dense_decoder(z_size, in_size)
    decoded = decoder(encoded)
    autoencoder = keras.models.Model(inputs=[x,x_obs], outputs=[decoded])

    autoencoder.compile(optimizer='adam', loss = 'mse')
    reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8, patience=10, verbose=1, min_lr=1e-7)
    callback_list = [reduce_lr]
    #autoencoder.fit([Xt, Xobs], [Yt], validation_split=0.1, epochs=400, batch_size=128, callbacks=callback_list)
    autoencoder.fit_generator(generator=training_generator, validation_data=validation_generator,
            use_multiprocessing=True, workers=4, epochs=500, callbacks=callback_list)

    print("Model trained")

    return autoencoder

def eval_ae(autoencoder, Xtest, Xtest_obs, Ytest):
    test_generator = TSAug(Xtest, Xtest_obs, batch_size=64, shuffle=True)

    [xt,xo], [yt] = test_generator.__getitem__(0)
    time_obs = np.arange(200)/180.0
    t1 = time.time()
    ytest = autoencoder.predict([xt,xo])
    t2 = time.time()
    print("Took {} s to process a batch of {} trajectories. On avg {} ms per trajectory".format(
        t2-t1, len(xt), 1000.0*(t2-t1)/len(xt)))
    for i in range(15):
        obs_ix = np.nonzero(xo[i])[0]
        hid_ix = np.nonzero(1-xo[i])[0]
        plt.figure(1)
        for d in range(3):
            plt.subplot(3,1,d+1)
            plt.plot(time_obs[obs_ix],yt[i][obs_ix,d],'g.')
            plt.plot(time_obs[hid_ix],yt[i][hid_ix,d],'r.')
            plt.plot(time_obs,ytest[i][:,d],'bx')
            if len(obs_ix)>0:
                plt.axvline(time_obs[obs_ix[-1]])
        plt.show()


def main(args):
    with open(args.data,'rb') as f:
        d = np.load(f)
        X = d['X']
        N = len(X)

        ntrain = int(round(N*args.p))
        ntest = N-ntrain
        Xt = d['noisyX'][0:ntrain]
        Yt = X[0:ntrain]
        Xt_obs = np.ones((ntrain,200,1))

        Xtest = d['noisyX'][ntrain:]
        Ytest = X[ntrain:]
        Xtest_obs = np.ones((ntest,200,1))

        if args.model and os.path.isfile(args.model):
            autoencoder = keras.models.load_model(args.model)
        else:
            autoencoder = train_ae(Xt, Xt_obs, Yt, Xt_obs)
            if args.model:
                autoencoder.save(args.model)

        # run examples in test set
        eval_ae(autoencoder, Xtest, Xtest_obs, Ytest)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="File with the normalized training/validation data")
    parser.add_argument('--model', help="Path where the model should be saved")
    parser.add_argument('--p', type=float, default=0.8, help="Percentaje of the data used for training")
    args = parser.parse_args()
    main(args)
