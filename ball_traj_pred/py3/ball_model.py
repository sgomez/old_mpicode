
import keras
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import argparse
import os
import json
import time

class CVAE_TSAug(keras.utils.Sequence):
    """ Augments the data for time series modeling
    """

    def __init__(self, X, Xobs, batch_size=32, ds_mult=16, shuffle=True):
        self.X = X
        self.Xobs = Xobs
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.ds_mult = ds_mult
        self.on_epoch_end()

    def on_epoch_end(self):
        self.indexes = np.repeat(np.arange(len(self.X)), self.ds_mult)
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_ids):
        Y = self.X[list_ids]
        Yobs = self.Xobs[list_ids]
        
        N,T,K = Y.shape        
        ts_lens = np.random.randint(low=0, high=T, size=N)
        is_obs = np.array([np.arange(T) < x for x in ts_lens])
        Xobs = Yobs*is_obs.reshape((self.batch_size,T,1))
        X = Xobs*Y

        return X, Xobs, Y, Yobs

    def __len__(self):
        x = np.floor(len(self.indexes) / self.batch_size)
        return int(x)

    def __getitem__(self, index):
        list_ids = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        X, Xobs, Y, Yobs = self.__data_generation(list_ids)
        return [X,Xobs,Y], [Y]

