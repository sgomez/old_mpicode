""" Loads saved predictions for multiple models and compare those predictions
"""

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import json

def main(args):
    with open(args.data,'rb') as f:
        d = np.load(f)
        for i in range(5):
            plt.plot(d['times'][i],d['X'][i][:,2],'bx')
            plt.show()
        



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="File with the training/validation data")
    args = parser.parse_args()
    main(args)
