""" Scales dataset and saves parameters to file
"""

import argparse
import os
import numpy as np
import json
import sklearn.preprocessing as prep
import pickle

def norm_apply(normalizer, X):
    nX = normalizer.transform(X.reshape(-1,3))
    return nX.reshape(X.shape)

def main(args):
    with open(args.raw_data,'rb') as f:
        data = np.load(f)
        xscaler = prep.StandardScaler()
        xdscaler = prep.StandardScaler()
        X = data['X']
        N = len(X)
        n = int(round(N*args.p))
        Xt = X[0:n]
        
        xscaler.fit(Xt.reshape(-1,3))
        if 'Xd' in data:
            Xdt = data['Xd'][0:n]
            xdscaler.fit(Xdt.reshape(-1,3))
        else:
            xdscaler = None

        ndata = dict(data)
        ndata['X'] = norm_apply(xscaler, data['X'])
        if 'Xd' in data: ndata['Xd'] = norm_apply(xdscaler, data['Xd'])
        if 'noisyX' in data: ndata['noisyX'] = norm_apply(xscaler, data['noisyX'])

        np.savez(open(args.scaled_data,'wb'), **ndata)
        print("Normalized data saved to {}".format(args.scaled_data))
        if args.scaler:
            pickle.dump({'xscaler': xscaler, 'xdscaler': xdscaler}, open(args.scaler,'wb'))
            print("Scaler saved to file {}".format(args.scaler))
     



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('raw_data', help="File with the training/validation data")
    parser.add_argument('scaled_data', help="File where the scaled data should be stored")
    parser.add_argument('--scaler', help="File where the scaler parameters are saved")
    parser.add_argument('--p', type=float, default=0.8, help="Percentaje of the data used for training")
    args = parser.parse_args()
    main(args)
