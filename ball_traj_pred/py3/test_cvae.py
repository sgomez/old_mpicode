 
import keras
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import argparse
import os
import json
import time
import pickle

class TSAug(keras.utils.Sequence):
    """ Augments the data for time series modeling
    """

    def __init__(self, X, Xobs, batch_size=32, ds_mult=16, shuffle=True):
        self.X = X
        self.Xobs = Xobs
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.ds_mult = ds_mult
        self.on_epoch_end()

    def on_epoch_end(self):
        self.indexes = np.repeat(np.arange(len(self.X)), self.ds_mult)
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_ids):
        Y = self.X[list_ids]
        Yobs = self.Xobs[list_ids]
        
        N,T,K = Y.shape        
        ts_lens = np.random.randint(low=0, high=T, size=N)
        is_obs = np.array([np.arange(T) < x for x in ts_lens])
        Xobs = Yobs*is_obs.reshape((self.batch_size,T,1))
        X = Xobs*Y

        return X, Xobs, Y, Yobs

    def __len__(self):
        x = np.floor(len(self.indexes) / self.batch_size)
        return int(x)

    def __getitem__(self, index):
        list_ids = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        X, Xobs, Y, Yobs = self.__data_generation(list_ids)
        Ymask = np.concatenate((Y,Yobs),axis=-1)
        return [X,Xobs,Y], [Ymask]


def dense_encoder(input_size, h_size):
    encoder = keras.models.Sequential()
    encoder.add( keras.layers.Reshape( (input_size,) ) )
    encoder.add( keras.layers.Dense(512, activation='relu') )
    encoder.add( keras.layers.Dense(h_size, activation='relu') )
    return encoder

def dense_decoder(z_size, y_size):
    decoder = keras.models.Sequential()
    decoder.add( keras.layers.Dense(1024, activation='relu') )
    decoder.add( keras.layers.Dense(y_size*3) )
    decoder.add( keras.layers.Reshape( (y_size,3) ) )
    return decoder

def cvae_loss(mu, log_sigma, batch_size):
    def loss(y_mask, y_decoded_mean):
        y = y_mask[:,:,0:-1]
        mask = y_mask[:,:,-1]
        d = y - y_decoded_mean
        d_sq = keras.backend.sum( keras.backend.square(d), axis=-1 )
        d_sq_masked = d_sq * mask
        kl = 0.5 * keras.backend.sum(keras.backend.exp(log_sigma) + 
                keras.backend.square(mu) - 1. - log_sigma)
        rec_loss = keras.backend.sum( d_sq_masked )
        return (rec_loss + kl) / batch_size
    return loss

def train_cvae(Xt, Xobs, Yt, Yobs):
    in_size = Xt.shape[1]
    h_size = 128
    z_size = 16
    batch_size=256
    data_size,T,K = Xt.shape
    n_training = int(0.9*data_size)

    def sampling(args):
        z_mean, z_log_sigma = args
        epsilon = keras.backend.random_normal(shape=(batch_size, z_size))
        return z_mean + tf.exp(z_log_sigma) * epsilon

    training_generator = TSAug(Xt[0:n_training], Xobs[0:n_training], batch_size=batch_size, shuffle=True)
    validation_generator = TSAug(Xt[n_training:], Xobs[n_training:], batch_size=batch_size, shuffle=True)


    x = keras.layers.Input(shape=(in_size,3))
    y = keras.layers.Input(shape=(in_size,3))
    z_in = keras.layers.Input(shape=(z_size,))
    x_obs = keras.layers.Input(shape=(in_size,1))
    x_conc = keras.layers.concatenate([x, x_obs])
    cencoder_input = keras.layers.concatenate([x_conc, y])

    h_f = dense_encoder(7*in_size, h_size)
    h_encode = h_f(cencoder_input)

    mu_z_f = keras.layers.Dense(z_size)
    mu_z = mu_z_f(h_encode)

    log_sig_z_f = keras.layers.Dense(z_size)
    log_sig_z = log_sig_z_f(h_encode)

    z_f = keras.layers.Lambda(sampling, output_shape=(z_size,))
    z = z_f([mu_z, log_sig_z])
    h_decode = dense_encoder(4*in_size, h_size)(x_conc)
    z_cond_training = keras.layers.concatenate([z,h_decode])
    z_cond_eval = keras.layers.concatenate([z_in, h_decode])

    encoder = keras.models.Model(inputs=[x,x_obs,y], outputs=[mu_z, log_sig_z])
    decoder_f = dense_decoder(z_size+h_size, in_size)
    decoded_training = decoder_f(z_cond_training)
    decoded_eval = decoder_f(z_cond_eval)
    decoder = keras.models.Model(inputs=[x, x_obs, z_in], outputs=[decoded_eval])
    autoencoder = keras.models.Model(inputs=[x,x_obs,y], outputs=[decoded_training])

    my_vae_loss = cvae_loss(mu_z,log_sig_z,batch_size)
    autoencoder.compile(optimizer='adam', loss = my_vae_loss)
    reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8, patience=10, verbose=1, min_lr=1e-7)
    callback_list = [reduce_lr]
    #autoencoder.fit([Xt, Xobs], [Yt], validation_split=0.1, epochs=400, batch_size=128, callbacks=callback_list)
    autoencoder.fit_generator(generator=training_generator, validation_data=validation_generator,
            use_multiprocessing=True, workers=4, epochs=400, callbacks=callback_list)

    extra = {'z_size': z_size, 'h_size': h_size, 'in_size': in_size, 'model': 'cvae'}

    print("Model trained")

    return encoder, decoder, extra

def eval_cvae(decoder, Xtest, Xtest_obs, Ytest, extra, norm=None):
    samples = 30
    batch_size = 64
    in_size = extra['in_size']
    z_size=extra['z_size']
    test_generator = TSAug(Xtest, Xtest_obs, batch_size=batch_size, shuffle=True)

    [xt,xo,_], [yt] = test_generator.__getitem__(0)
    time_obs = np.arange(in_size)/180.0

    t1 = time.time()
    # mu_z and sig_z have both dim [batch, T, K] out -> [samples, batch, T, K]
    z = np.random.normal(loc=0.0, scale=1.0, size=(samples,batch_size,z_size)) #np.array([np.random.normal(loc=mu_z, scale=sig_z) for i in range(samples)])
    t2 = time.time()
    print("Took {} s to draw {} samples from {} trajectories. On avg {} ms per trajectory".format(
        t2-t1, samples, len(xt), 1000.0*(t2-t1)/len(xt)))
    t1 = time.time()
    ytest_samples = np.array([decoder.predict([xt,xo,z[i]]) for i in range(samples)])
    t2 = time.time()
    print("Took {} s to decode {} samples from {} trajectories. On avg {} ms per trajectory".format(
        t2-t1, samples, len(xt), 1000.0*(t2-t1)/len(xt)))
    if norm is not None:
        #yt = [norm.inverse_transform(np.concatenate((x,np.ones((len(x),1))), axis=1)) for x in yt]
        yt = [norm.inverse_transform(x[:,0:3]) for x in yt]
        ytest_samples = norm.inverse_transform(ytest_samples.reshape((-1,3))).reshape(ytest_samples.shape)
    for i in range(15):
        obs_ix = np.nonzero(xo[i])[0]
        hid_ix = np.nonzero(1-xo[i])[0]
        plt.figure(1)
        for d in range(3):
            plt.subplot(3,1,d+1)
            plt.plot(time_obs[obs_ix],yt[i][obs_ix,d],'g.')
            plt.plot(time_obs[hid_ix],yt[i][hid_ix,d],'r.')
            y_test_mean = np.mean(ytest_samples[:,i,:,d],axis=0)
            y_test_std = np.std(ytest_samples[:,i,:,d],axis=0)
            for j in range(2):
                plt.plot(time_obs,ytest_samples[j][i][:,d],'m')
            plt.plot(time_obs, y_test_mean, 'b')
            plt.fill_between(time_obs, y_test_mean - 2*y_test_std, y_test_mean + 2*y_test_std,
                    color='b', alpha=0.5)
            if len(obs_ix)>0:
                plt.axvline(time_obs[obs_ix[-1]])
        plt.show()


def main(args):
    with open(args.data,'rb') as f:
        d = np.load(f)
        X = d['X']
        N = len(X)

        ntrain = int(round(N*args.p))
        ntest = N-ntrain
        Xt = X[0:ntrain]
        Yt = X[0:ntrain]
        Xtest = X[ntrain:]
        Ytest = X[ntrain:]
        if 'Xobs' in d:
            Xt_obs = d['Xobs'][0:ntrain]
            Xtest_obs = d['Xobs'][ntrain:]
        else:
            T = len(X[0])
            Xt_obs = np.ones((ntrain,T,1))
            Xtest_obs = np.ones((ntest,T,1))

        if args.model and os.path.exists(args.model):
            keras.losses.my_vae_loss = keras.losses.mean_squared_error
            extra = json.load( open(os.path.join(args.model,'extra.json'), 'r') )
            decoder = keras.models.load_model( os.path.join(args.model,'decoder.h5') )
        else:
            encoder, decoder, extra = train_cvae(Xt, Xt_obs, Yt, Xt_obs)
            if args.model:
                os.makedirs(args.model)
                encoder.save(os.path.join(args.model, 'encoder.h5'))          
                decoder.save(os.path.join(args.model, 'decoder.h5'))
                json.dump(extra, open(os.path.join(args.model, 'extra.json'),'w'))

        if args.s:
            my_s = pickle.load(open(args.s, 'rb'))
            norm = my_s['xscaler']
        else:
            norm = None
        # run examples in test set
        eval_cvae(decoder, Xtest, Xtest_obs, Ytest, extra, norm)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="File with the normalized training/validation data")
    parser.add_argument('--model', help="Path where the model should be saved")
    parser.add_argument('--p', type=float, default=0.8, help="Percentaje of the data used for training")
    parser.add_argument('--s', help="File where the scaling model was saved")
    args = parser.parse_args()
    main(args)
