import numpy as np
import tensorflow as tf
import robpy.neural_process as rnp
import robpy.utils as utils
import robpy.deep_promp as deep_promp
import matplotlib.pyplot as plt

import argparse
import os
import json

def create_dataset(times, X, bounce_times):
    """ Create input features and corresponding outputs

    features: current_time, bounce_time
    """
    N,T,D = X.shape
    my_bounce_times = np.array([x[0] if len(x)>0 else -1.0 for x in bounce_times])
    x_times = np.reshape(times, (N,T,1))
    x_bounce_times = np.tile(np.reshape(my_bounce_times, (N,1,1)), (1,T,1))
    x = np.concatenate((
        x_times,
        x_bounce_times,
        x_times - x_bounce_times
        ), axis=2)
    y = X
    return x, y

def train_ball_promp(sess, x, ball_pos):
    N,T,in_dims = x.shape
    feature_dims = 25
    encoder_type = 0
    if encoder_type==0:
        encoder = lambda x, is_training: tf.reshape(
                rnp.feature_mlp(x, is_training, hidden_units=[8,16,32,64,feature_dims], hidden_activation=tf.nn.relu, batch_norm=False), 
                shape=(tf.shape(x)[0],tf.shape(x)[1],1,feature_dims)
                )
    elif encoder_type==1:
        init_centers = np.linspace(-0.1,1.1,feature_dims-1)
        init_scales = 6.5*np.ones(feature_dims-1) #10*np.random.rand(feature_dims-1)
        encoder = lambda x, is_training: tf.reshape(
                deep_promp.rbf(x, np.reshape(init_centers,(-1,1)), init_scales), 
                shape=(tf.shape(x)[0],tf.shape(x)[1],1,feature_dims)
                )
    prior_mu_w = None #{'m0': tf.zeros((feature_dims,1), dtype=tf.float64), 'k0': 1.0}
    #prior_Sigma_w = {'v0': feature_dims+1.0, 'mean_cov_mle': lambda v0, Sw_mle: tf.multiply(Sw_mle, tf.eye(feature_dims, dtype=tf.float64))}
    prior_Sigma_w = None #{'v0': feature_dims+1.0, 'mean_cov_mle': lambda v0, Sw_mle: 10*tf.eye(feature_dims, dtype=tf.float64)}
    dpmp = deep_promp.DeepProMP(in_dims=in_dims, out_dims=1, feature_dims=feature_dims, phi=encoder, prior_mu_w=prior_mu_w, prior_Sigma_w=prior_Sigma_w)

    y = ball_pos[:,:,2].reshape((N,T,1)) #take only z at the moment

    history = dpmp.fit(sess, x, y, lr=1e-4, init_phi_lr=1e-3, opt_basis_steps=20, em_batch_size=100, basis_batch_size=25, 
            iterations=50, init_phi_iter=500)#, noise_x=1e-3, noise_y=1e-2)
    if 'init_phi_loss' in history:
        phi_loss = history['init_phi_loss']
        plt.plot(phi_loss)
        plt.title('Loss of the basis function initialization procedure')
        plt.show()
    ob_loss = np.mean(history['opt_basis_losses'], axis=0)
    plt.plot(ob_loss)
    plt.show()
    plt.plot(history['loss'])
    plt.show()
    
    print "Mean Weights:\n", sess.run(dpmp.mu_w)
    print "Stdev Weights:\n", np.sqrt(np.diag( sess.run(dpmp.Sigma_w) ))
    print "Noise stdev:\n", np.sqrt(np.diag( sess.run(dpmp.Sigma_y) ))
    return dpmp

def test_pred(sess, x, ball_pos, model, nobs, n_plot):
    N,T,D = ball_pos.shape
    y = ball_pos[:,:,2].reshape((N,T,1)) #take only z at the moment

    predictions = []
    errors = []
    e_step = model.comp_w_dist(sess, x[:,0:nobs], y[:,0:nobs])
    y_means, y_covs = model.output_dist(sess, x, w_dist=e_step)

    plt.title('Example of after training')
    for i in range(5):
        plt.plot(x[i,:,0],y[i,:,0],'rx')
        plt.plot(x[i,:,0],y_means[i,:,0,0])
        plt.show()
    return None

def main(args):
    with open(args.data,'r') as f:
        data = np.load(f)
        times = data['times']
        X = data['X']
        Xd = data['Xd']
        noisyX = data['noisyX']
        bounce_times = data['bounce_times']
    x, y = create_dataset(times, X, bounce_times)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        dpmp = train_ball_promp(sess, x, y)
        test_pred(sess, x, y, dpmp, nobs=200, n_plot=5)
        test_pred(sess, x, y, dpmp, nobs=100, n_plot=5)
        test_pred(sess, x, y, dpmp, nobs=50, n_plot=5)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="Path to file with data")
    parser.add_argument('--n_training', type=int, help="Number of instances used for training")
    args = parser.parse_args()
    main(args)
