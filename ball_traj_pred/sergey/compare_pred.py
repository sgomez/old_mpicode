""" Loads saved predictions for multiple models and compare those predictions
"""


import robpy.kalman as kalman
import robpy.table_tennis.ball_model as ball_model
import robpy.table_tennis.load_data as loader
import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import json

def main(args):
    all_pred = {}
    winners = []
    for fname in args.pred:
        pred = np.load(open(fname,'r'))
        all_pred[fname] = pred
        nobs = pred['nobs']
        pred_err = np.array(pred['errors'])[:,nobs:]
        avg_pred_err = np.mean(pred_err, axis=0)
        plt.plot(avg_pred_err, label=fname)
        print("{} with {} obs avg error: {}".format(fname, nobs, np.mean(avg_pred_err)))
        winners.append( (np.mean(avg_pred_err), fname) )
        if 'likelihoods' in pred:
            avg_v_lh = np.mean(pred['likelihoods'])
            print("{} avg val lhood: {}".format(fname, avg_v_lh))
    plt.legend()
    plt.show()
    winners.sort()
    print(winners)
    if args.data:
        data = np.load(open(args.data,'r'))
        fig = plt.figure()
        ax = [fig.add_subplot(3,1,d+1) for d in range(3)]
        for fname in all_pred:
            pred = all_pred[fname]
            T = min(len(pred['pred']), len(data['obs'][pred['from_ix']]))
            diff = abs(pred['pred'][:,0:T] - data['obs'][pred['from_ix']:pred['to_ix']][:,0:T])
            avg_diff = np.mean(diff, axis=0)
            for d in range(3): ax[d].plot(avg_diff[:,d], label=fname)
        plt.legend()
        plt.show()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('pred', nargs='+', help="Files to be loaded for comparison")
    parser.add_argument('--data', help="File with the original data")
    args = parser.parse_args()
    main(args)
