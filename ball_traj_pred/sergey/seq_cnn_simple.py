
import keras
import tensorflow as tf
import numpy as np
import argparse
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
import os

def gaussian_loss(y_true, y_pred):
    """ Computes a negative log likelihood for a Gaussian prediction

    The predictions are assumed to have twice as many dimensions as the
    input representing the mean and standard deviation vectors concatenated.
    """
    #sess = keras.backend.get_session()
    s = keras.backend.shape(y_true) #.eval(session=sess)
    #print N,K,D
    mean = y_pred[:,:,0:s[2]]
    chol_prec_flat = y_pred[:,:,s[2]:]
    chol_prec = keras.backend.reshape(chol_prec_flat, (s[0],s[1],s[2],s[2]))
    chol_prec_t = tf.transpose(chol_prec,perm=[0,1,3,2]) 
    prec = tf.matmul(chol_prec, chol_prec_t) + 1e-8*tf.eye(s[2],batch_shape=(s[0],s[1]))
    diff = tf.reshape(y_true - mean, (s[0],s[1],s[2],1))
    v = tf.matmul(chol_prec, diff)
    #v = diff
    mah_dist = tf.reduce_sum(keras.backend.square(v))
    log_prec = tf.reduce_sum(tf.linalg.logdet(prec))
    L = tf.cast(s[0]*s[1], tf.float32)
    ans = (mah_dist - log_prec) / L
    return ans


def seq_cnn(input_dims, output_dims):
    model = keras.models.Sequential()
    model.add(keras.layers.Conv1D(filters=4, kernel_size=2, 
        padding='causal', dilation_rate=1, 
        activation='relu', input_dim=input_dims))
    model.add(keras.layers.GaussianNoise(0.01))
    #model.add(keras.layers.normalization.BatchNormalization())
    model.add(keras.layers.Conv1D(filters=6, kernel_size=2, 
        padding='causal', dilation_rate=2, 
        activation='relu'))
    #model.add(keras.layers.normalization.BatchNormalization())
    model.add(keras.layers.Conv1D(filters=8, kernel_size=2, 
        padding='causal', dilation_rate=4, 
        activation='relu'))
    #model.add(keras.layers.normalization.BatchNormalization())
    model.add(keras.layers.Conv1D(filters=10, kernel_size=2, 
        padding='causal', dilation_rate=8, 
        activation='relu'))
    #model.add(keras.layers.normalization.BatchNormalization())
    model.add(keras.layers.Conv1D(filters=12, kernel_size=2, 
        padding='causal', dilation_rate=16, 
        activation='relu'))
    #model.add(keras.layers.normalization.BatchNormalization())
    model.add(keras.layers.Conv1D(filters=14, kernel_size=2, 
        padding='causal', dilation_rate=32, 
        activation='relu'))
    #model.add(keras.layers.normalization.BatchNormalization())
    model.add(keras.layers.Conv1D(filters=16, kernel_size=2, 
        padding='causal', dilation_rate=64, 
        activation='relu'))
    #model.add(keras.layers.normalization.BatchNormalization())

    #Output layer
    model.add(keras.layers.Conv1D(filters=output_dims, kernel_size=1))
    return model

def lin_seq_ar(input_dims, output_dims):
    model = keras.models.Sequential()
    #Output layer
    model.add(keras.layers.Conv1D(filters=output_dims, kernel_size=8, padding='causal', input_dim=input_dims))
    return model

def gaussian_seq_cnn(input_dims):
    model = seq_cnn(input_dims, input_dims + input_dims*input_dims)

def seq_cnn_pred(model, obs, T, add_input=True, gaussian_dim=None, out_norm=None):
    s = obs.shape
    x = np.concatenate((obs,np.zeros((s[0],T,s[2]))), axis = 1)
    for t in xrange(T):
        px = model.predict(x[:,0:s[1]+t,:])
        if out_norm is not None: px = out_norm.inverse_transform(px)
        px = px[:,-1,:]
        if gaussian_dim is not None:
            d = gaussian_dim
            mu = px[:,0:d]
            v = np.reshape(px[:,d:],(-1,d,d))
            v_t = np.transpose(v,(0,2,1))
            prec = np.matmul(v,v_t) + np.reshape(1e-8*np.eye(d),[1,d,d])  
            Sigma = np.linalg.inv(prec)
            px = [np.random.multivariate_normal(mean=mu[i], cov=Sigma[i]) for i in range(len(mu))]
        if add_input: px += x[:,s[1]+t-1,:]
        x[:,s[1]+t,:] = px
    return x

def main(args):
    # Load data
    with open(args.data, 'r') as f:
        data = np.load(f)
        times = data['times']
        obs = data['obs']
        is_bounce = data['is_bounce']

    N,T,D = np.shape(obs)

    # Train a data normalization object
    obs_flat = np.reshape(obs, [-1,3])
    normalizer = StandardScaler().fit(obs_flat)
    obs_norm_flat = normalizer.transform(obs_flat)
    obs_norm = np.reshape(obs_norm_flat, (N,T,D))

    inputs = obs_norm[:,0:-1,:]
    one_step_pred = obs_norm[:,1:,:]
    outputs = one_step_pred - inputs
    outputs_flat = np.reshape(outputs, [-1,3])
    out_norm = StandardScaler().fit(outputs_flat)
    outputs_norm = out_norm.transform(outputs_flat).reshape(outputs.shape)

    model_fname = "{}.json".format(args.model)
    model_w = "{}.h5".format(args.model)
    model_extra = "{}.npz".format(args.model)
    tset_size = 4000

    if os.path.isfile(model_fname) and os.path.isfile(model_w):
        json_file = open(model_fname, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        model = keras.models.model_from_json(loaded_model_json)
        # load weights into new model
        model.load_weights(model_w)

        extra = np.load(open(model_extra,'r'))
        normalizer.set_params(**extra['normalizer'])
    else:
        # Do not penalize first K obs as there is very little evidence
        train_mask = np.ones((N,T-1))
        if args.k:
            train_mask[:,0:args.k] = 0

        #model = seq_cnn(D,D+D*D)
        model = seq_cnn(D,D)
        #model = lin_seq_ar(D,D)
        # serialize model to JSON
        model_json = model.to_json()
        with open(model_fname, "w") as json_file:
            json_file.write(model_json)
        model.compile(loss="mse", optimizer="adam", sample_weight_mode="temporal")
        #model.compile(loss=gaussian_loss, optimizer="adam", sample_weight_mode="temporal")
        filepath="checkpoints/weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
        checkpoint = keras.callbacks.ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
        #reduce learning learning rate on plateau
        reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8, patience=15, verbose=1, min_lr=1e-7)
        #callback list
        callbacks_list = [reduce_lr]#,checkpoint]
        # fit the model
        history = model.fit(inputs[0:tset_size,:,:], outputs[0:tset_size,:,:], validation_split=0.1, epochs=args.epochs, 
                batch_size=16, callbacks=callbacks_list, sample_weight=train_mask[0:tset_size,:])
        plt.plot(np.log10(history.history['loss']))
        plt.plot(np.log10(history.history['val_loss']))
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()

        # serialize weights to HDF5
        model.save_weights(model_w)
        print("Saved model to disk")

        # Statistics of the output distribution on training data
        train_pred = model.predict(inputs[0:tset_size])
        k = 0 if not args.k else args.k
        train_diff = outputs[0:tset_size,k:,:] - train_pred[:,k:,:]
        train_diff = np.reshape(train_diff, [-1,3]).T
        diff_mean = np.mean(train_diff, axis=1)
        diff_cov = np.cov(train_diff)
        diff_cov = np.diag(np.diag(diff_cov))

        out_norm_par = {'scale': out_norm.scale_, 'mean': out_norm.mean_}
        extra = {'normalizer': normalizer.get_params(), 'Sigma_y': diff_cov, 'out_norm': out_norm_par}
        np.savez(open(model_extra,'w'), **extra)

        print("Training diff mean={} \n cov={}".format(diff_mean, diff_cov))

    print("Predicting for the test set")
    k = 40 if not args.k else args.k
    test_in = inputs[tset_size:,0:k,:]
    test_out = one_step_pred[tset_size:,:,:]
    #test_pred = seq_cnn_pred(model, obs=test_in, T=T-k, add_input=True, gaussian_dim=3)
    test_pred = seq_cnn_pred(model, obs=test_in, T=T-k, add_input=True, out_norm=out_norm)
    test_pred = normalizer.inverse_transform(test_pred)
    for i in range(10):
        curr_time = times[tset_size+i]
        curr_ts = obs[tset_size+i,:,:]
        curr_pred = test_pred[i,:,:]
        fig,ax = plt.subplots(nrows=3)
        for i,curr_ax in enumerate(ax):
            curr_ax.scatter(curr_time,curr_ts[:,i])
            curr_ax.plot(curr_time[k:],curr_pred[k:,i])
        plt.legend(['real', 'predicted'], loc='upper left')
        plt.show()




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--k', type=int, help="On training avoids penalizing first k predictions. On test is how many obs to use")
    parser.add_argument('--model', help="Path to the cnn model")
    parser.add_argument('--data', default='ball_data_one_bounce.npz', help="Ball data")
    parser.add_argument('--epochs', default=30, type=int, help="Number of training epochs")
    args = parser.parse_args()
    main(args)
