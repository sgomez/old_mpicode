
import robpy.full_promp as promp
import robpy.table_tennis.load_data as loader
import robpy.utils as utils
import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import json

def extend_ball_traj(times, positions, duration, use_nobs=36):
    basis = {
            'conf': [{"type": "poly", "nparams": 0, "conf": {"order": 2}}],
            'params': []
            }
    basis_fun = lambda t,params: promp.comb_basis(t, params, conf=basis["conf"])
    bf_lambdas = promp.get_bfun_lambdas(basis['params'], basis_fun, True, True)
    poly_promp = promp.FullProMP(basis=basis, num_joints=3,
            model={'mu_w': np.zeros(9), 'Sigma_w': np.eye(9)*100, 'Sigma_y':np.eye(3)*1e-5})
    if len(times) < use_nobs: 
        raise Exception("Given a trajectory of {} obs, but minumum is {} obs".format(len(times), use_nobs))
    #1) Find initial position and velocity
    time_cut = times[-use_nobs]
    poly_time = [x - time_cut for x in times[-use_nobs:]]
    poly_time.append(1.0 + poly_time[0]) #Do not time scale
    poly_obs = positions[-use_nobs:]
    #print poly_time
    #print poly_obs
    e_step = poly_promp.E_step([poly_time], [poly_obs])
    #print e_step
    w = np.random.multivariate_normal(e_step['w_means'][0], e_step['w_covs'][0])
    phi_t = promp.comp_Phi_t(poly_time[-2], 1.0, num_joints=3, **bf_lambdas)
    init_state = np.dot(phi_t, w)
    #print init_state
    x0 = init_state[0:3]
    v0 = init_state[3:6]
    #print("Using w={} to extend the ball trajectory".format(w))
    #print("Initial position={} and velocity={}".format(x0,v0))

    #2) Extend to the desired duration
    deltaT = 1.0/180.0
    airDrag = 0.3
    gravety = np.array([0,0,-9.8])
    curr_time = times[-1]
    curr_pos = x0
    curr_vel = v0
    while (curr_time - times[0]) < duration:
        acc = -airDrag*curr_vel + gravety
        curr_pos = curr_pos + deltaT*curr_vel + 0.5*deltaT**2*acc
        curr_vel = curr_vel + deltaT*acc
        curr_time += deltaT
        times.append(curr_time)
        positions.append(curr_pos)


def build_training_set(fw_ball_traj, duration, min_z=-1e100, augment=False, min_duration=0.8):
    #X, Xavg, U, Hidden, Times = fw_ball_traj
    Times, X, is_bounce = fw_ball_traj
    times = []
    x = []
    for n,t in enumerate(Times):
        for start_ix in range(0,len(t)/2,5):
            d = t[-1] - t[start_ix]
            if d < min_duration: continue
            curr_time = []
            curr_pos = []
            for i in range(start_ix, len(t)):
                c_t = t[i]
                if c_t - t[start_ix] > duration: break
                if X[n][i][2] > min_z:
                    curr_time.append(c_t - t[start_ix])
                    curr_pos.append(X[n][i])
            extend_ball_traj(curr_time, curr_pos, duration)
            curr_time[-1] = duration
            times.append(np.array(curr_time))
            x.append(np.array(curr_pos))
            if not augment: break
    print("Size of the training set: {}".format(len(x)))
    return times, x

def train_ball_promp(fw_ball_traj, duration, min_z=-1e100, min_duration=0.8):
    full_basis = {
            'conf': [
                    {"type": "sqexp", "nparams": 3, "conf": {"dim": 2}},
                    {"type": "sqexp", "nparams": 5, "conf": {"dim": 4}},
                    {"type": "poly", "nparams": 0, "conf": {"order": 1}}
                ],
            'params': [np.log(0.4),0.25,0.75, np.log(0.2), 0.1, 0.4, 0.55, 0.9]
            }
    dim_basis_fun = promp.dim_comb_basis(**full_basis)
    inv_whis_mean = lambda v, Sigma: utils.make_block_diag(Sigma, 3) + 1e-4*np.eye(dim_basis_fun*3)
    args = {
            #'prior_mu_w': {"m0": np.zeros(3*dim_basis_fun), "k0": 1},
            'prior_Sigma_w': {'v':dim_basis_fun*3, 'mean_cov_mle': inv_whis_mean},
            'opt_basis_pars': False,
            'print_lowerbound': True,
            'max_iter': 30
            }
    model = promp.FullProMP(basis=full_basis)
    times, pos = build_training_set(fw_ball_traj, duration, min_z=min_z, augment=False, min_duration=min_duration)
    print("Data-set pre-processed")
    model.train(times=times, q=pos, **args)
    print "Mean Weights:\n", model.mu_w
    print "Stdev Weights:\n", np.sqrt(np.diag(model.Sigma_w))
    print "Noise stdev:\n", np.sqrt(np.diag(model.Sigma_y))
    print "Basis function params: ", model.get_basis_pars()
    return model

def test_pred(fw_ball_traj, model, duration, nobs=20, min_z=-1e100, min_duration=0.8, n_plot=2):
    #X, Xavg, U, Hidden, Times = fw_ball_traj
    Times, X, is_bounce = fw_ball_traj
    times_cut, pos_cut = build_training_set(fw_ball_traj, duration, min_z=min_z, min_duration=min_duration)
    times, pos = build_training_set(fw_ball_traj, duration, min_duration=min_duration)
    N = len(times)
    predictions = []
    errors = []
    lhoods = []
    for n,Xn in enumerate(pos):
        Tn = len(Xn)
        mu_w = model.mu_w
        Sigma_w = model.Sigma_w
        time_n = times_cut[n].tolist()
        time_n.append(duration)
        if nobs > 0:
            e_step = model.E_step([time_n], [pos_cut[n][0:nobs]])
            model.mu_w = e_step['w_means'][0]
            model.Sigma_w = e_step['w_covs'][0]
        #for i in range(nobs):
        #    model.condition(times[n][i],duration,Xn[i],ignore_Sy=False)
        #    #print model.mu_w
        means, covs = model.marginal_w(times[n])
        time_obs = times[n]
        x_obs = np.array(Xn)
        pred = np.array(means)
        variances = np.array([np.diag(x) for x in covs])
        stdevs = np.sqrt(variances)
        diff = x_obs - pred
        dist = np.linalg.norm(diff, axis=1)

        predictions.append(pred)
        errors.append(dist)
        lhoods.append(model.log_likelihood([times[n]],q=[Xn]))

        if n < n_plot:
            plt.figure(1)
            for d in xrange(3):
                plt.subplot(3,1,d+1)
                plt.plot(time_obs,x_obs[:,d],'g.')
                plt.plot(time_obs,pred[:,d],'r')
                plt.fill_between(time_obs, pred[:,d] - 2*stdevs[:,d], pred[:,d] + 2*stdevs[:,d], facecolor='red', alpha=0.5)
                plt.axvline(time_n[nobs])
            plt.show()

            plt.plot(time_obs, dist)
            plt.axvline(time_n[nobs])
            plt.show()
        model.mu_w = mu_w
        model.Sigma_w = Sigma_w
    return {'pred': predictions, 'errors': errors, 'nobs': nobs, 'likelihoods': lhoods}

def main(args):
    with open(args.cons,'r') as f:
        if args.cons.endswith('json'):
            fw_ball_traj = loader.fw_ball_traj( json.load(f) )
        elif args.cons.endswith('npz'):
            data = np.load(f)
            #fw_ball_traj = (data['X'], data['Xavg'], data['U'], data['Hidden'], data['Times'])
            fw_ball_traj = (data['times'][0:args.n_training], data['obs'][0:args.n_training], 
                data['is_bounce'][0:args.n_training])
            fw_ball_traj_test = (data['times'][args.test_from_ix:], data['obs'][args.test_from_ix:], 
                    data['is_bounce'][args.test_from_ix:])
    duration = args.duration
    min_z = args.min_z
    if not os.path.isfile(args.model):
        print("Starting the training procedure")
        model = train_ball_promp(fw_ball_traj, duration=duration, min_z=min_z, min_duration=args.min_duration)
        stream = model.to_stream()
        data = {'promp': stream, 'duration': duration, 'min_z': min_z}
        with open(args.model, 'w') as fout:
            json.dump(data, fout)
    else:
        with open(args.model, 'r') as fin:
            data = json.load(fin)
        model = promp.FullProMP(**data['promp'])
        duration = data['duration']
        min_z = data['min_z']
    ans = test_pred(fw_ball_traj_test, model, duration=duration, nobs=args.test_nobs, min_z=min_z, min_duration=args.min_duration)
    ans['from_ix'] = args.test_from_ix
    ans['to_ix'] = args.test_from_ix+len(fw_ball_traj_test[0])
    if args.save_pred:
        np.savez(open(args.save_pred,'w'), **ans)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('cons', help="Path to file with consolidated data")
    parser.add_argument('model', help="Path to the model file")
    parser.add_argument('--n_training', type=int, default=100, help="Number of training data to use")
    parser.add_argument('--test_from_ix', type=int, default=4900, help="Use from this index for testing")
    parser.add_argument('--duration', type=float, default=0.8, help="All ball trajectories will be modeled with this duration")
    parser.add_argument('--min_duration', type=float, default=0.7, help="All trajectories shorter than this are discarded")
    parser.add_argument('--min_z', type=float, default=-1e100, help="All observations lower than the given value would be removed")
    parser.add_argument('--test_nobs', type=int, default=20, help="Number of observation received by the trajectory prediction test")
    parser.add_argument('--save_pred', help="File where predictions and errors should be stored")
    args = parser.parse_args()
    main(args)
