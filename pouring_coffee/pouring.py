import json
import robpy.slpp as slpp
import robpy.time_series as ts
import robpy.full_promp as promp
import robpy.utils as utils
import robpy.kinematics.forward as fwd_kin
import scipy.linalg

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import argparse
import time

def main(args):
    num_primitives = 3
    movements = []
    goto_time = 2.0
    fwd_barrett = fwd_kin.BarrettKinematics(endeff=[0.0,0.0,0.26,0.0,0.0,0.0])
    inv_barrett = promp.ProbInvKinematics(fwd_barrett)

    for i in range(num_primitives):
        with open('primitives/pouring/promp{}.json'.format(i),'r') as f:
            promp_data = json.load(f)
            robot_promp = promp.FullProMP(**promp_data)
            movements.append({'data': promp_data, 'promp': robot_promp})

    sl = slpp.SLPP(host=args.host, port=args.port)
    if not args.aero:
        sl.clear_sensor_obs()
        print("Obtaining the position of the object...")
        sensor_id = 1
        time.sleep(0.1)
        obs = sl.flush_sensor_obs()
        t, x = slpp.obs_to_numpy(obs)
        if not sensor_id in x:
            jts = sl.get_joint_obs()
            q,_,_ = slpp.joints_to_numpy(jts)
            x,_ = fwd_barrett.end_effector(q[-1])
            print("Object not found, quitting. Spoon at {}".format(x))
            return 1
        obj_pos = np.mean(x[sensor_id], axis=0) + np.array([-0.02,0.06,0.11]) #+ np.array([-0.06, 0.06, 0.08])
    else:
        obj_pos = np.array(args.aero)
    print("Object position: {0}. Adapting the ProMP".format(obj_pos))
    means,covs = movements[0]['promp'].marginal_w([0,1],pos=True)
    mu_q, Sigma_q = inv_barrett.inv_kin(means[1], covs[1], obj_pos, np.eye(3)*1e-6)
    movements[0]['promp'].condition(t=1,T=1,q=mu_q, Sigma_q=Sigma_q)
    means,_ = movements[0]['promp'].marginal_w([0,1],pos=True)
    spoon_pos,_ = fwd_barrett.end_effector(means[1])
    print("q_n={}, mu_x={}".format(means[1], spoon_pos))

    t0 = sl.get_time()+0.5
    q0 = means[0]
    sl.go_to(t0, goto_time, q0.tolist(), np.zeros(7).tolist())
    t0 += goto_time

    durations = [3.0, 2.5, 3.0] #duration of the movements (First is goto)
    waitings = [5.0, 0.5, 1.0] #wait before execution
    for i, mov in enumerate(movements):
        ans = sl.query('mov_prim/add_freeze', {'t0': t0, 'T': waitings[i], 'ndof': 7})
        t0 += waitings[i]
        ans = sl.query('mov_prim/addProMP', {'promp': mov['promp'].to_stream(), 't0':t0, 'T': durations[i]})
        t0 += durations[i]
        #time.sleep(2*T+0.2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--host', default="localhost", help="SL Host")
    parser.add_argument('--port', default="7648", help="SL port")
    parser.add_argument('--aero', nargs=3, type=float, help="Optional position of the aeropress")
    args = parser.parse_args()
    main(args)
