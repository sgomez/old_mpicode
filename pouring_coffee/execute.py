
import json
import robpy.slpp as slpp
import robpy.time_series as ts
import robpy.full_promp as promp
import robpy.utils as utils
import scipy.linalg

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import argparse
import time

num_primitives = 6
movements = []
goto_time = 2.0

for i in range(num_primitives):
    with open('promp{}.json'.format(i),'r') as f:
        promp_data = json.load(f)
        robot_promp = promp.FullProMP(**promp_data)
        movements.append({'data': promp_data, 'promp': robot_promp})

sl = slpp.SLPP(host='wam', port='7648')
t0 = sl.get_time()+0.5
means,_ = movements[0]['promp'].marginal_w([0,1],pos=True)
q0 = means[0]
sl.go_to(t0, goto_time, q0.tolist(), np.zeros(7).tolist())
t0 += goto_time

durations = [3.0, 1.0, 3.0, 3.0, 1.0, 3.0] #duration of the movements (First is goto)
waitings = [5.0, 0.5, 1.0, 5.0, 0.5, 1.0] #wait before execution
for i, mov in enumerate(movements):
    #command = raw_input('e) Exit n) Next. Command: ')
    #if (command == 'e'):
    #    break
    ans = sl.query('mov_prim/add_freeze', {'ndof': 7, 't0': t0, 'T': waitings[i]})
    t0 += waitings[i]
    ans = sl.query('mov_prim/addProMP', {'promp': mov['data'], 't0':t0, 'T': durations[i]})
    t0 += durations[i]
    #time.sleep(2*T+0.2)

