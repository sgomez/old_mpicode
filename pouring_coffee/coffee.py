import json
import robpy.slpp as slpp
import robpy.time_series as ts
import robpy.full_promp as promp
import robpy.utils as utils
import robpy.kinematics.forward as fwd_kin
import scipy.linalg

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import argparse
import time

def main(args):
    num_primitives = 8
    movements = []
    goto_time = 2.0
    fwd_barrett = fwd_kin.BarrettKinematics(endeff=[0.0,0.0,0.26,0.0,0.0,0.0])
    inv_barrett = promp.ProbInvKinematics(fwd_barrett)

    sl = slpp.SLPP(host=args.host, port=args.port)
    if args.grinder and args.aero:
        aeropress_pos = args.aero
        grinder_bottom = args.grinder
    else:
        sl.clear_sensor_obs()
        print("Obtaining the position of the object...")
        sensor_id = 1
        time.sleep(0.1)
        obs = sl.flush_sensor_obs()
        t, x = slpp.obs_to_numpy(obs)
        if not sensor_id in x:
            print("Object not found, quitting")
            return 1
        ball_pos = np.mean(x[sensor_id], axis=0)
        print("Ball position: {}".format(ball_pos))
        if not args.grinder:
            grinder_bottom = ball_pos + np.array([-0.04,0.07,-0.16])
            print("Pass position of the grinder. Example --grinder {}".format(grinder_bottom))
            return 1
        aeropress_pos = ball_pos + np.array([0.0,0.06,0.12])
        grinder_bottom = args.grinder
    grinder_top = grinder_bottom + np.array([0.06,0.0,0.25])

    for i in range(num_primitives):
        with open('primitives/coffee/promp{}.json'.format(i),'r') as f:
            promp_data = json.load(f)
            robot_promp = promp.FullProMP(**promp_data)
            movements.append({'data': promp_data, 'promp': robot_promp})
    
    end_pos_cond = [grinder_top,None,None,grinder_bottom,None,aeropress_pos,None,None]
    q0 = None
    for i in range(num_primitives):
        means,covs = movements[i]['promp'].marginal_w([0,1],pos=True)
        if i==0: q0 = means[0]
        if end_pos_cond[i] is None: continue
        mu_q, Sigma_q = inv_barrett.inv_kin(means[1], covs[1], end_pos_cond[i], np.eye(3)*1e-6)
        prior_mu_q, prior_S_q = movements[i]['promp'].marginal_w([0,1])
        llh = -0.5*promp.quad(prior_mu_q[1]-mu_q,np.linalg.inv(prior_S_q[1]))
        if llh > -10:
            print("Conditioning primitive {} because the position has high likelihood: {}".format(i,llh))
            movements[i]['promp'].condition(t=1,T=1,q=mu_q, Sigma_q=Sigma_q)
        else:
            print("Primitive {} was not conditioned because it has low likelihood: {}".format(i,llh))
            return -1
        #means,_ = movements[i]['promp'].marginal_w([0,1],pos=True)
        #spoon_pos,_ = fwd_barrett.end_effector(means[1])
        #print("q_n={}, mu_x={}".format(means[1], spoon_pos))

    t0 = sl.get_time()+0.5
    sl.go_to(t0, goto_time, q0.tolist(), np.zeros(7).tolist())
    t0 += goto_time

    durations = [3.0, 2.0, 3.0, 3.0, 3.0, 4.0, 2.0, 3.0] #duration of the movements (First is goto)
    waitings = [5.0, 0.5, 1.0, 0.5, 8.0, 0.5, 0.5, 1.0] #wait before execution
    for i, mov in enumerate(movements):
        ans = sl.query('mov_prim/add_freeze', {'t0': t0, 'T': waitings[i], 'ndof': 7})
        t0 += waitings[i]
        ans = sl.query('mov_prim/addProMP', {'promp': mov['promp'].to_stream(), 't0':t0, 'T': durations[i]})
        t0 += durations[i]
        #time.sleep(2*T+0.2)
    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--host', default="localhost", help="SL Host")
    parser.add_argument('--port', default="7648", help="SL port")
    parser.add_argument('--grinder', type=float, nargs=3, help="Grinder bottom position")
    parser.add_argument('--aero', type=float, nargs=3, help="Optional. Position of the aeropress")
    args = parser.parse_args()
    main(args)
