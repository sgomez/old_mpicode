""" Script to train based on protocol buffer segmented data
"""

import json
import robpy.protos.utils as proto_utils
import robpy.protos.table_tennis_pb2 as ttpb
import robpy.time_series as ts
import robpy.full_promp as promp
import robpy.utils as utils
import struct

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import argparse

def joint_space_plot(t, q, qd):
    fig = plt.figure()
    N = len(t)
    joints = range(7)
    for i in joints:
        for n in xrange(N):
            ax = fig.add_subplot(len(joints),2,1+2*i)
            ax.set_ylabel("q_{0}".format(i))
            ax.plot(t[n]-t[n][0], q[n][:,i], color="black")
            ax = fig.add_subplot(len(joints),2,2+2*i)
            ax.set_ylabel("qd_{0}".format(i))
            ax.plot(t[n]-t[n][0], qd[n][:,i], color="black")
    ax.set_xlabel("Time")
    plt.show()

def joint_traj(data, num_primitives=1):
    movements = [{'times': [], 'q': [], 'qd': [], 'z': []} for i in range(num_primitives)]
    for i,elem in enumerate(data):
        tn = []
        qn = []
        qdn = []
        for j in elem.joints:
            tn.append(j.t)
            qn.append(j.q)
            qdn.append(j.qd)
        # Trim the start and end of trajectories
        cuts = ts.multiple_zero_cross_vel(qdn, P=None, low_thresh=0.02, high_thresh=0.8)
        assert(len(cuts) == 1)
        (a,b) = cuts[0]
        tn = tn[a:b]
        qn = qn[a:b]
        qdn = qdn[a:b]
        zn = np.linspace(0,1,len(tn))

        #Then store the resulting primitives
        movements[i%num_primitives]['z'].append(zn)
        movements[i%num_primitives]['times'].append(np.array(tn))
        movements[i%num_primitives]['q'].append(np.array(qn))
        movements[i%num_primitives]['qd'].append(np.array(qdn))
    return movements

def main(args):
    data = proto_utils.load_trials(args.data)
    if args.n:
        N = min(len(data), args.n * args.num_primitives)
        data = data[0:N]
    else:
        N = len(data)
    movements = joint_traj(data, args.num_primitives)

    for i in range(args.num_primitives):
        joint_space_plot(movements[i]['z'], movements[i]['q'], movements[i]['qd'])

    full_basis = {
            'conf': [
                    {"type": "sqexp", "nparams": 4, "conf": {"dim": 3}},
                    {"type": "poly", "nparams": 0, "conf": {"order": 1}}
                ],
            'params': [np.log(0.25),0.25,0.5,0.75]
            }
    dim_basis_fun = promp.dim_comb_basis(**full_basis)
    inv_whis_mean = lambda v, Sigma: (N*utils.make_block_diag(Sigma, 7) + v*args.wstd**2*np.eye(dim_basis_fun*7))/(N+v)
    prior_Sigma_w = {'v':dim_basis_fun*7, 'mean_cov_mle': inv_whis_mean}

    for i, mov in enumerate(movements):
        robot_promp = promp.FullProMP(basis=full_basis)
        robot_promp.train(mov['times'], q=mov['q'], prior_Sigma_w = prior_Sigma_w)
        print "Mean Weights:\n", robot_promp.mu_w
        print "Stdev Weights:\n", np.sqrt(np.diag(robot_promp.Sigma_w))
        print "Noise stdev:\n", np.sqrt(np.diag(robot_promp.Sigma_y))
        print "Basis function params: ", robot_promp.get_basis_pars()
        stream = robot_promp.to_stream()
        with open('promp{}.json'.format(i), 'w') as fout:
            json.dump(stream, fout)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="Path to the protobuf data file")
    parser.add_argument('--num_primitives', type=int, default=1, help="Number of primitives to train assuming they were executed sequentially")
    parser.add_argument('-n', type=int, help="Take a prefix of the data of length n")
    parser.add_argument('--wstd', default=0.5, type=float, help="Prior standard deviation over parameters w")
    args = parser.parse_args()
    main(args)
