
import numpy as np
import datetime

def proMP_function(robot_promp, time_robot, **params):
  if params['promp_type'] == 'full':
    mean, cov = robot_promp.marginal_w(time_robot, pos=True, vel=True, **params)
  elif params['promp_type'] == 'indep':
    means, covs = robot_promp.marginal_w(time_robot)
    mean = np.array(map(lambda x: x[0], means))
    cov = np.diag(map(lambda x: x[0,0], covs))
  else:
    print "Error: Unreconginzed promp type ", params['promp_type']
  assert(len(mean)==7 and np.shape(cov)==(7,7))
  return mean,cov

def top_k_hitting_points(overlaps, k):
  ans = []
  a = b = np.argmax(overlaps)
  ans.append(a)
  for i in xrange(k-1):
    if a==0:
      b += 1
      ans.append(b)
    elif (b+1) == len(overlaps):
      a -= 1
      ans.append(a)
    elif overlaps[a-1] > overlaps[b+1]:
      a -= 1
      ans.append(a)
    else:
      b += 1
      ans.append(b)
  return ans

def rob_ball_dist(ball_time, ball_pos, rob_pos, t0):
  times = []
  dist = []
  j = 0
  for i in xrange(len(ball_time)):
    if ball_time[i] > t0:
      if j == len(rob_pos): break
      times.append(ball_time[i])
      diff = ball_pos[i] - rob_pos[j]
      dist.append(np.sqrt(np.dot(diff,diff)))
      j += 1
  return times, dist

def new_time_id():
  return datetime.datetime.now().strftime('%Y%m%d_%H%M%S')

def reverse_raw_robot_data(data):
  time = data['rob_time'].tolist()
  q = data['q'].tolist()
  qdot = data['qdot'].tolist()
  time.reverse()
  q.reverse()
  qdot.reverse()
  ans = {'time': np.array(time), 'q': np.array(q), 'qdot': np.array(qdot)}
  return ans

def zero_cross_vel_split(t, q, qdot, **params):
  params.setdefault('split_joint', 0) #The chosen joint to use for splitting
  params.setdefault('zero_tol', 0.01) #Percentaje of max velocity considered zero
  #1) Find the maximum velocity joint velocity
  vel = np.array(map(lambda v: v[params['split_joint']], qdot))
  max_ix = np.argmax(np.abs(vel))
  if vel[max_ix] < 0: vel = -vel
  zero_vel = params['zero_tol']*vel[max_ix]
  #2) Move from maximum until zero_vel velocity is found
  a = b = max_ix
  while a>0 and vel[a] > zero_vel: a -= 1
  while b<len(vel) and vel[b] > zero_vel: b += 1
  #3) Split and save
  ans = {'pre_q': q[0:a], 'pre_qdot': qdot[0:a], 'pre_time': t[0:a], \
      'q': q[a:b], 'qdot': qdot[a:b], 'time': t[a:b], \
      'pos_q': q[b:], 'pos_qdot': qdot[b:], 'pos_time': t[b:], \
      'start_time': t[a], 'end_time': t[b]}
  return ans

def extract_incoming_ball_traj(ball_data, end_time, **params):
  params.setdefault('cam_id', 3)
  times = []
  ans = []
  for inst in ball_data:
    if inst['cam_id'] == params['cam_id']:
      if inst['time'] < end_time:
        ans.append(inst['pos'])
        times.append(inst['time'])
  return times, np.array(ans)
