
#include <stdio.h>
#include <math.h>

#define NDOF 7
#define NBASIS_FUN 4

static double w[NDOF][NBASIS_FUN] = {
  {1.08983573, 0.5499812, 0.47783119, 1.09423173},
  {-0.03312099, -0.03745159, 0.0519168, -0.0382587},
  {-0.09216665, 0.0171659, -0.09504106, -0.01412094},
  {1.14937197, 0.519285, 0.56936269, 1.04874536},
  {-1.05349526, -0.50267343, -0.48960188, -1.10902006},
  {0.01537189, 0.02570483, 0.00344471, 0.00684928},
  {0.09471861, 0.05005183, 0.02273813, 0.05546109}
};

static double kernel_centers[NBASIS_FUN] = {0, 0.333, 0.666, 1};
static double sigma_kernel = 0.3;

double compute_angle(double time, int joint_id) {
  int i;
  double ans = 0.0, dif;
  for (i=0; i<NBASIS_FUN; i++) {
    dif = time - kernel_centers[i];
    ans += w[joint_id][i]*exp(-0.5*dif*dif/(sigma_kernel*sigma_kernel));
  }
  return ans;
}

double compute_angle_vel(double time, int joint_id) {
  int i;
  double ans = 0.0, dif, ss;
  for (i=0; i<NBASIS_FUN; i++) {
    ss = sigma_kernel*sigma_kernel;
    dif = time - kernel_centers[i];
    ans += w[joint_id][i]*exp(-0.5*dif*dif/ss)*(-dif/ss);
  }
  return ans;
}

int main() {
  int i, j;
  double time;
  for (i=0; i<100; i++) {
    time = i / 1000.0;
    printf("Time %lf: \n", time);
    for (j=0; j<NDOF; j++) {
      printf("q%d: [%lf, %lf]. ", j, compute_angle(time, j), compute_angle_vel(time, j));
    }
    printf("\n");
  }
  printf("%lf\n", w[5][2]);
}
