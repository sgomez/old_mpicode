
import numpy as np

def poly_surface(poly_list, **params):
  '''Creates a set of surface polygons from a list of transversal polygons
  This method receives a list of transversal polygons that form a 3D object,
  sorted in the way they should be connected. The method returns a set of
  polygons that can be used to render the desired 3D object. All the transversal
  polygons should have the same number of vertices, and the vertices should be 3D.
  '''
  ans = []
  assert(len(poly_list) >= 2)
  n_vertices = len(poly_list[0])
  for i in xrange(1,len(poly_list)):
    for j in xrange(n_vertices):
      c_poly = [poly_list[i-1][j],poly_list[i][j],poly_list[i][j-1],poly_list[i-1][j-1]]
      ans.append(c_poly)
  return ans

def surf_sketch(surface_poly, **params):
  '''Creates a sketch file with the object to render
  This method receives a set of polygons that form the surface of a 3D object,
  and a file name to save Sketch code that defaults to 'out.sk'. It produces
  a file with the given filename that can be used to render the desired 3D
  object.
  '''
  params.setdefault('fillcolor', 'green')
  params.setdefault('opacity', 0.5)
  params.setdefault('cull', 'false')
  params.setdefault('linewidth', '0.01pt')
  params.setdefault('linecolor', params['fillcolor'])
  params.setdefault('filename', 'out.sk')
  params.setdefault('def_name', 'object')
  
  f_out = open(params['filename'], 'w')
  f_out.write('def {0} {{\n'.format(params['def_name']))
  for poly in surface_poly:
    f_out.write('  polygon[fillcolor={0}, opacity={1}, cull={2}, linewidth={3}, linecolor={4}]'.format(\
        params['fillcolor'], params['opacity'], params['cull'], params['linewidth'], params['linecolor']))
    for x in poly:
      f_out.write('({0},{1},{2})'.format(x[0],x[1],x[2]))
    f_out.write('\n')
  f_out.write('}\n')
  f_out.close()

def cov2Dellipse(cov, **params):
  '''Computes a 2D ellipse given a 2*2 covariance matrix
  '''
  params.setdefault('scale', 4.605) #90% confidence intervals
  U, s, V = np.linalg.svd(cov)
  axis = np.sqrt(params['scale']*s)
  return {'axis': axis, 'rot': V}

def ellipse2Dpoly(**params):
  '''Computes polygon from ellipse
  '''
  params.setdefault('n_vertices', 10) #Number of vertices of the polygon
  params.setdefault('axis', np.array([1,1])) #Length in x and y axis
  params.setdefault('rot', np.eye(2)) #Rotation matrix
  params.setdefault('center', np.array([0,0])) #Center of the ellipse

  ans = []
  for i in xrange(params['n_vertices']):
    t = 2*np.pi*i/params['n_vertices']
    x = params['axis']*np.array([np.cos(t), np.sin(t)])
    x = np.dot(params['rot'], x) + params['center']
    ans.append(x)
  return ans

def traj_dist_polys(task_mean, task_cov, **params):
  '''Returns a set of transversal polygons for a trajectory distribution
  '''
  params.setdefault('main_axis', 1)
  main_axis = params['main_axis']
  covs2d = map(lambda x: np.delete(np.delete(x, main_axis, 0), main_axis, 1), task_cov)
  ellipse2d = map(lambda x: cov2Dellipse(x, **params), covs2d)
  polys = []
  for i in xrange(len(ellipse2d)):
    curr_ellipse = ellipse2d[i]
    poly_2d = ellipse2Dpoly(center=np.delete(task_mean[i], main_axis, 0), **curr_ellipse)
    poly_3d = map(lambda x: np.insert(x, main_axis, task_mean[i][main_axis]), poly_2d)
    polys.append(poly_3d)
  return polys


def test():
  trans_poly = traj_dist_polys(map(np.array, [[0,0,0.3],[0,0.1,0.23]]), [0.1*np.eye(3),0.2*np.eye(3)])
  surface = poly_surface(trans_poly)
  surf_sketch(surface, filename='test.sk')

#test()
