
import zmq
import struct
import ctypes
import binascii
import loadData
import time
import numpy as np

context = zmq.Context()

class BarretKinematics:

  def __init__(self, endeff = [0.0, 0.0, 0.14531, 0.0, 0.0, 0.0]):
    self.ZSFE = 0.346
    self.ZHR = 0.505
    self.YEB = 0.045
    self.ZEB = 0.045
    self.YWR = -0.045
    self.ZWR = 0.045
    self.ZWFE = 0.255
    self.endeff = endeff

  def _link_matrices(self,q):
    cq = np.cos(q)
    sq = np.sin(q)

    sa=np.sin(self.endeff[3])
    ca=np.cos(self.endeff[3])

    sb=np.sin(self.endeff[4])
    cb=np.cos(self.endeff[4])

    sg=np.sin(self.endeff[5])
    cg=np.cos(self.endeff[5])

    hi00 = np.array([[1,0,0,0],[0,-1,0,0],[0,0,-1,0],[0,0,0,1]])
    hi01 = np.array([[cq[0],-sq[0],0,0],[sq[0],cq[0],0,0],[0,0,1,self.ZSFE],[0,0,0,1]])
    hi12 = np.array([[0,0,-1,0],[sq[1],cq[1],0,0],[cq[1],-sq[1],0,0],[0,0,0,1]])
    hi23 = np.array([[0,0,1,self.ZHR],[sq[2],cq[2],0,0],[-cq[2],sq[2],0,0],[0,0,0,1]])
    hi34 = np.array([[0,0,-1,0],[sq[3],cq[3],0,self.YEB],[cq[3],-sq[3],0,self.ZEB],[0,0,0,1]])
    hi45 = np.array([[0,0,1,self.ZWR],[sq[4],cq[4],0,self.YWR],[-cq[4],sq[4],0,0],[0,0,0,1]])
    hi56 = np.array([[0,0,-1,0],[sq[5],cq[5],0,0],[cq[5],-sq[5],0,self.ZWFE],[0,0,0,1]])
    hi67 = np.array([[0,0,1,0],[sq[6],cq[6],0,0],[-cq[6],sq[6],0,0],[0,0,0,1]])
    hi78 = np.array([[cb*cg, -(cb*sg), sb, self.endeff[0]], [cg*sa*sb + ca*sg, ca*cg - sa*sb*sg, -(cb*sa), self.endeff[1]], [-(ca*cg*sb) + sa*sg, cg*sa + ca*sb*sg, ca*cb, self.endeff[2]], [0,0,0,1]])
    return [hi00,hi01,hi12,hi23,hi34,hi45,hi56,hi67,hi78]

  def forward_kinematics(self,q):
    H = self._link_matrices(q)
    A = H[0]
    ans = []
    for i in xrange(1,len(H)):
      A = np.dot(A,H[i])
      ans.append(A)
    return ans

  def __rotMatToEul(self,rotMat):
    eul = np.zeros(3)
    eul[0] =  np.arctan2(-rotMat[2,1],rotMat[2,2])
    eul[1] =   np.arctan2(rotMat[2,0],np.sqrt(rotMat[2,1]**2+rotMat[2,2]**2))
    eul[2] =   np.arctan2(-rotMat[1,0],rotMat[0,0])
    return eul

  def __end_eff(self, q, As):
    end_eff = As[-1]
    pos = end_eff[0:3,3]
    orientation = self.__rotMatToEul(end_eff[0:3,0:3].transpose())
    return pos, orientation

  def end_effector(self,q):
    As = self.forward_kinematics(q)
    return self.__end_eff(q, As)

  def __num_jac(self,q,eps):
    ans = np.zeros((3,len(q)))
    fx,ori = self.end_effector(q)
    for i in xrange(len(q)):
      q[i] += eps
      fxh,ori = self.end_effector(q)
      ans[:,i] = (fxh - fx) / eps
      q[i] -= eps
    return ans

  def __analytic_jac(self, q, As):
    ans = np.zeros((6,len(q)))
    pe = As[-1][0:3,3]
    for i in xrange(len(q)):
      zprev = As[i][0:3,2]
      pprev = As[i][0:3,3]
      ans[0:3,i] = np.cross(zprev, pe - pprev)
      ans[3:6,i] = zprev
    return ans

  def jacobian(self, q):
    As = self.forward_kinematics(q)
    #j1 = self.__num_jac(q,1e-6)
    j2 = self.__analytic_jac(q, As)
    #print "diff = ", np.linalg.norm(j1 - j2[0:3,:])
    return j2

  def position_and_jac(self, q):
    As = self.forward_kinematics(q)
    jac = self.__analytic_jac(q, As)
    pos, ori = self.__end_eff(q, As)
    return pos, jac, ori

  def end_eff_trajectory(self, Q):
    pos = []
    orientation = []
    for t in xrange(len(Q)):
      post, ort = self.end_effector(Q[t])
      pos.append(post)
      orientation.append(ort)
    return np.array(pos), np.array(orientation)

class Robot:
  #context: zmq context
  #req: zmq request socket
  #req_buff: buffer of data to send to the robot

  def _decode_response(self, response):
    offset = 0
    op_code_st = struct.Struct('< b')
    ans = {}
    while offset < len(response):
      op_code = op_code_st.unpack_from(response, offset)[0]
      offset += 1
      if (op_code == 0):
        break #means end of response
      elif (op_code == 3):
        #observed arm positions
        nelem_st = struct.Struct('< I')
        n = nelem_st.unpack_from(response, offset)[0]
        offset += nelem_st.size
        traj_step_st = struct.Struct('< 22d')
        qs = []
        qdots = []
        qdotdots = []
        times = []
        for i in xrange(n):
          tmp = traj_step_st.unpack_from(response, offset)
          qs.append(tmp[0:7])
          qdots.append(tmp[7:14])
          qdotdots.append(tmp[14:21])
          times.append(tmp[21])
          offset += traj_step_st.size
        ans['q'] = qs
        ans['qdot'] = qdots
        ans['qdotdot'] = qdotdots
        ans['arm_times'] = times
      elif (op_code == 4):
        #Observed ball positions
        nelem_st = struct.Struct('< I')
        n = nelem_st.unpack_from(response, offset)[0]
        offset += nelem_st.size
        ball_obs_st = struct.Struct('< 4d 2i') #last int for padding
        ball_pos = []
        for i in xrange(n):
          tmp = ball_obs_st.unpack_from(response, offset)
          inst = {'pos': tmp[0:3], 'time': tmp[3], 'cam_id': tmp[4]}
          ball_pos.append(inst)
          offset += ball_obs_st.size
        if (n>0): ans['ball_pos'] = ball_pos
      elif (op_code == 8):
        time_st = struct.Struct('< d')
        ans['rob_time'] = time_st.unpack_from(response, offset)[0]
        offset += time_st.size
    return ans

  def __init__(self, host, tcp_port):
    uri = "tcp://{0}:{1}".format(host, tcp_port)
    self.req = context.socket(zmq.REQ)
    self.req.connect(uri)
    self.req_buff = ''

  def append_trajectory(self, qs, qdots, **params):
    params.setdefault('start_time', -1.0)
    N = len(qs)
    #The first character in the format is for endianness (< little, > big, @ default)
    trajHead = struct.Struct('< b I')
    trajStep = struct.Struct('< 22d')
    buff = ctypes.create_string_buffer(trajHead.size + N*trajStep.size)
    trajHead.pack_into(buff, 0, 2, N) #2 is for push_back
    for i in xrange(N):
      data = qs[i] + qdots[i]
      if 'qdotdot' in params: 
        data += params['qdotdot'][i]
      else:
        data += [0.0 for j in xrange(7)]
      data.append(params['start_time'])
      trajStep.pack_into(buff,trajHead.size + i*trajStep.size, *data)
    #print 'The buffer: ', map(hex,map(ord,buff.raw))
    self.req_buff += buff.raw
  
  def append_sim_ball(self, times, balls, **params):
    params.setdefault('cam_id', 3)
    head = struct.Struct('< b I')
    ball_obs = struct.Struct('< 4d 2i') #last int for padding
    N = len(balls)
    buff = ctypes.create_string_buffer(head.size + N*ball_obs.size)
    head.pack_into(buff, 0, 6, N) #6 is for appending ball simulations
    #print 'elem_size:', ball_obs.size
    for i in xrange(N):
      data = balls[i].tolist() + [times[i], params['cam_id'], 0] #last int for padding
      ball_obs.pack_into(buff, head.size + i*ball_obs.size, *data)
    self.req_buff += buff.raw

  def append_clear_sim_ball(self):
    self.req_buff += struct.pack('< b', 7) #7 is for clearing the ball simulated buffer

  def append_get_ball_obs(self):
    self.req_buff += struct.pack('< b', 4) #4 is for get ball observations
  
  def append_clear_ball_obs(self):
    self.req_buff += struct.pack('< b', 5) #5 is for clearing ball observations
  
  def append_get_time(self):
    self.req_buff += struct.pack('< b', 8) #8 for getting task servo time

  def clear_program(self):
    self.req_buff = struct.pack('< b', 1) #1 is for clear

  def send_program(self):
    self.req_buff += struct.pack('< b', 0)
    self.req.send(self.req_buff)
    self.req_buff = ''
    response = self.req.recv()
    return self._decode_response(response)

  def get_arm_traj(self, last_n=5000):
    data = struct.pack('< b I b', 3, last_n, 0)
    self.req.send(data)
    response = self.req.recv()
    ans = self._decode_response(response)
    return ans

  def current_state(self, n=1):
    ans = self.get_arm_traj(n)
    return ans['q'], ans['qdot']


class RobotClock:

  def sync(self, robot):
    t1_pc = time.time()
    robot.append_get_time()
    tmp = robot.send_program()
    rob_time = tmp['rob_time']
    t2_pc = time.time()
    print "Time ping: ", t2_pc - t1_pc
    pc_time = (t2_pc + t1_pc) / 2.0
    #pc_time = t2_pc
    self.offset = rob_time - pc_time
 
  def __init__(self, robot):
    self.sync(robot)

  def time(self):
    return time.time() + self.offset

def poly_deriv(poly):
  ans = []
  for i in xrange(1,len(poly)):
    ans.append(poly[i]*i)
  return ans

def compute_poly(order, conditions, **params):
  params.setdefault('prior_prec', 0)
  Phi = []
  y = []
  derivs = [[1] * (order+1)]
  for condition in conditions:
    t = condition['t']
    while len(derivs) <= condition['der_order']:
      derivs.append(poly_deriv(derivs[-1]))
    phi = []
    for i in xrange(order+1):
      pexp = i - condition['der_order']
      if pexp < 0:
        phi.append(0.0)
      else:
        phi.append(derivs[condition['der_order']][pexp]*(t**pexp))
    y.append(condition['output'])
    Phi.append(phi)
  #print 'Phi=',Phi
  #print 'y=',y
  Phi=np.array(Phi)
  y=np.array(y)
  Phi_pinv = np.dot( np.linalg.inv(np.dot(Phi.T,Phi) + params['prior_prec']**2*np.eye(order+1)), Phi.T )
  return np.dot(Phi_pinv,y)

def poly_eval(poly, t):
  ans = 0.0
  for i in xrange(len(poly)):
    ans += poly[i]*t**i
  return ans

def traj_poly_gen(time_steps, poly_coefs, **params):
  T = time_steps / float(params['rob_freq'])
  poly_derivs = map(lambda poly_coef: poly_deriv(poly_coef), poly_coefs)
  poly_acc = map(lambda poly_coef: poly_deriv(poly_coef), poly_derivs)
  #print 'Result coef:', poly_coefs
  qs = []
  qdots = []
  qdotdots = []
  for t in np.linspace(0,T,time_steps):
    qs.append(map(lambda poly_coef: poly_eval(poly_coef, t), poly_coefs))
    qdots.append(map(lambda poly_coef: poly_eval(poly_coef, t), poly_derivs))
    qdotdots.append(map(lambda poly_coef: poly_eval(poly_coef, t), poly_acc))
  return qs, qdots, qdotdots

def stop_traj(pos, vel, time_steps, **params):
  params.setdefault('poly_order', 3)
  params.setdefault('rob_freq', 500)
  T = time_steps / float(params['rob_freq'])
  poly_coefs = []
  for d in xrange(7):
    poly_cond = [{'t': 0, 'der_order': 0, 'output': pos[d]}, \
        {'t': 0, 'der_order': 1, 'output': vel[d]}, \
        {'t': T, 'der_order': 1, 'output': 0.0},\
        {'t': T, 'der_order': 2, 'output': 0.0}]
    if 'init_acc' in params:
      poly_cond.append({'t': 0, 'der_order': 2, 'output': params['init_acc'][d]})
    poly_coefs.append( compute_poly(params['poly_order'], poly_cond, **params) )
  return traj_poly_gen(time_steps, poly_coefs, **params)

def plan_poly_traj(from_q, to_q, time_steps, **params):
  params.setdefault('prior_prec', 0.001) 
  params.setdefault('poly_order', 5)
  params.setdefault('rob_freq', 500)
  params.setdefault('init_vel', np.zeros(7))
  params.setdefault('init_acc', np.zeros(7))
  params.setdefault('des_vel', np.zeros(7))
  params.setdefault('des_acc', np.zeros(7))
  T = time_steps / float(params['rob_freq'])
  poly_coefs = []
  for d in xrange(7):
    poly_cond = [{'t': 0, 'der_order': 0, 'output': from_q[d]}, \
        {'t': T, 'der_order': 0, 'output': to_q[d]}, \
        {'t': 0, 'der_order': 1, 'output': params['init_vel'][d]}, \
        {'t': T, 'der_order': 1, 'output': params['des_vel'][d]}, \
        {'t': 0, 'der_order': 2, 'output': params['init_acc'][d]}, \
        {'t': T, 'der_order': 2, 'output': params['des_acc'][d]}]
    poly_coefs.append( compute_poly(params['poly_order'], poly_cond, **params) )
  qs, qdots, qdotdots = traj_poly_gen(time_steps, poly_coefs, **params)
  return qs, qdots, qdotdots

def go_to_state(robot, qdes, time_steps, **params):
  q_init, qd_init = robot.current_state()
  qs, qdots, qdotdots = plan_poly_traj(q_init[0], qdes, time_steps, init_vel=qd_init[0], **params)
  robot.clear_program()
  robot.append_trajectory(qs,qdots,qdotdot=qdotdots)
  robot.send_program()

def test1(robot):
  qdes = [1.7967, -0.2344, -0.0941, 1.7391, -1.5738, 0.0445, 0.2699]
  qdotdes = [-0.0001, 0.4136, -0.1390, 0.6383, 0.1373, 0.2201, -0.1305]
  go_to_state(robot,qdes,qdotdes,1000)

def send_okan_data(robot):
  data = loadData.loadOkanData()
  robot.clear_program()
  for instance in data:
    print "Sending instance ", instance['file_num']
    #print "Going to initial state..."
    go_to_state(robot, instance['q'][0], instance['qdot'][0], 500)
    robot.append_trajectory(instance['q'].tolist(), instance['qdot'].tolist())
    robot.send_program()
    time.sleep(10)


#robot = Robot("10.42.31.249","7646")
#robot = Robot("localhost","7646")
#test1(robot)
#send_okan_data(robot)
