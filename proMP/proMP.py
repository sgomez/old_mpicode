
import numpy as np
from scipy.linalg import block_diag
import scipy.optimize as opt
from barret import poly_deriv

#One kind of kernel for proMP. All kernel classes should support __call__ and
#time_deriv for evaluation of the kernel and its time derivatives respectively
class proMP_Poly_Basis:
  #Attributes:
  #degree: The degree of the polynomial

  def __init__(self,**params):
    params.setdefault('degree', 3)
    self.degree = params['degree']
  
  def __call__(self, time):
    basis_f = map(lambda ix: time**ix, range(self.degree+1))
    return np.array(basis_f)

  def time_deriv(self, time, order=1):
    phi = np.zeros(self.degree+1)
    coefs = np.ones(self.degree+1)
    basis = time**np.array(range(self.degree+1))
    for i in xrange(order):
      coefs = poly_deriv(coefs)
    phi[order:self.degree+1] = coefs*basis[0:self.degree+1-order]
    return phi
    #return coefs*basis[0:self.degree+1]
    #deriv = map(lambda ix: ix*(time**(ix-1)) if ix>0 else 0.0, range(self.degree+1))
    #return np.array(deriv)

  def dim(self):
    return self.degree + 1

  def to_stream(self):
    ans = {'degree': self.degree}
    return ans

  @classmethod
  def build_from_stream(cls, stream):
    obj = cls(**stream)
    return obj

#One kind of kernel for proMP. All kernel classes should support __call__ and
#time_deriv for evaluation of the kernel and its time derivatives respectively
class proMP_Gauss_Basis:
  #Attributes:
  #centers: The centers (means) of each gaussian kernel
  #sigma_kernel: A single spread (deviation) for all kernels

  def __init__(self,**params):
    params.setdefault('num_basis', 5)
    params.setdefault('centers',np.linspace(0,1,params['num_basis']))
    params.setdefault('sigma_kernel',0.25)
    self.centers = params['centers']
    self.sigma_kernel = params['sigma_kernel']
  
  def __call__(self, time):
    sigma_sq = self.sigma_kernel**2
    ans = np.exp(-0.5*(time - self.centers)**2 / sigma_sq)
    return ans

  def time_deriv(self, time, order=1):
    sigma_sq = self.sigma_kernel**2
    val = np.exp(-0.5*(time - self.centers)**2 / sigma_sq)
    if order==1:
      return val*((self.centers-time)/(sigma_sq))
    elif order==2:
      return val*(((time-self.centers)/sigma_sq)**2 - 1/sigma_sq)
    else: raise NotImplementedError("Derivatives only implemented up to second order")

  def dim(self):
    return len(self.centers)

  def to_stream(self):
    ans = {'centers': self.centers, 'sigma_kernel': self.sigma_kernel}
    return ans

  @classmethod
  def build_from_stream(cls, stream):
    obj = cls(**stream)
    return obj

class KernelBuilder:
  kernel_stream_builder = {'sqexp': proMP_Gauss_Basis.build_from_stream, \
    'poly': proMP_Poly_Basis.build_from_stream}

  kernel_builder = {'sqexp': proMP_Gauss_Basis, \
    'poly': proMP_Poly_Basis}

  @classmethod
  def get_builder(cls, key):
    return cls.kernel_builder[key]

  @classmethod
  def get_stream_builder(cls, key):
    return cls.kernel_stream_builder[key]


#represent a probabilistic movement primitive, each movement y_n is generated as
#y_n(t) = phi(t)*w + eps, where phi(t) are the basis functions and w are some 
#parameters generated from a Gaussian prior with mean_w mean and Sigma_w covariace
#and eps is Gaussian noise with zero mean and Sigma_y covariance
class proMP:
  #Attributes:
  #mean_w: Prior mean for w values
  #Sigma_w: Prior variance for w values
  #Sigma_y: Likelihood variance
  
  def __init__(self, **params):
    if 'Sigma_y' in params: self.Sigma_y = params['Sigma_y']
    if 'mean_w' in params: self.mean_w = params['mean_w']
    if 'Sigma_w' in params: self.Sigma_w = params['Sigma_w']

  #If passed optional parameter Sigma_q then do conditioning on distribution
  def condition(self, phi_t, mu_q, **params):
    params.setdefault('ignore_Sigma_y', True)
    d,k = np.shape(phi_t)
    if params['ignore_Sigma_y']:
      tmp1 = np.dot(self.Sigma_w, phi_t.T)
      tmp2 = np.dot(phi_t, np.dot(self.Sigma_w, phi_t.T))
      tmp2 = np.linalg.inv(tmp2)
      tmp3 = np.dot(tmp1,tmp2)
      mu_w = self.mean_w + np.dot(tmp3, (mu_q - np.dot(phi_t, self.mean_w)))
      tmp4 = np.eye(d)
      if 'Sigma_q' in params:
        tmp4 -= np.dot(params['Sigma_q'], tmp2)
      Sigma_w = self.Sigma_w - np.dot(tmp3, np.dot(tmp4, tmp1.T))
    else:
      inv_Sig_w = np.linalg.inv(self.Sigma_w)
      inv_Sig_y = np.linalg.inv(self.Sigma_y)
      Sw = np.linalg.inv(inv_Sig_w + np.dot(phi_t.T,np.dot(inv_Sig_y, phi_t)))
      A = np.dot(np.dot(Sw, phi_t.T), inv_Sig_y)
      b = np.dot(Sw, np.dot(inv_Sig_w, self.mean_w))
      mu_w = np.dot(A, mu_q) + b
      if 'Sigma_q' in params:
        Sigma_w = Sw + np.dot(A,np.dot(params['Sigma_q'],A.T))
      else:
        Sigma_w = Sw
    #print "prior: mu_w=", self.mean_w, ", Sig_w=", self.Sigma_w
    #print "phi_t=", phi_t
    #print "mu_q=", mu_q, ", Sig_q=", Sigma_q
    #print "mu_w=", mu_w, "Sw=", Sw, " Sig_w=", Sigma_w
    return proMP(mean_w=mu_w, Sigma_w=Sigma_w, Sigma_y = self.Sigma_y)

  # The marginal distribution (mean and covariance of the resulting Gaussian)
  # for a particular value of t
  def marginal_w(self, Phi_t, **params):
    params.setdefault('ignore_Sigma_y', True)
    res_mean = np.dot(Phi_t, self.mean_w)
    res_cov = np.dot(Phi_t, np.dot(self.Sigma_w, Phi_t.T))
    if not params['ignore_Sigma_y']: 
      res_cov += self.Sigma_y
    return res_mean, res_cov
  
  def _init_EM(self, Phi, y, **params):
    assert len(Phi) == len(y) and len(Phi)>0
    N = len(Phi)
    K, D = np.shape(Phi[0][0])
    params.setdefault('mean_w', np.zeros(D))
    params.setdefault('Sigma_w', np.eye(D))
    params.setdefault('Sigma_y', np.eye(K))
    self.mean_w = params['mean_w']
    self.Sigma_w = params['Sigma_w']
    self.Sigma_y = params['Sigma_y']
    return N,D,K

  def _EM_lowerbound(self, Phi, y, expected_w, variance_w, N, D, **params):
    ans = 0
    inv_sig_w = np.linalg.inv(self.Sigma_w)
    tmp, log_det_sig_w = np.linalg.slogdet(self.Sigma_w)
    inv_sig_y = np.linalg.inv(self.Sigma_y)
    tmp, log_det_sig_y = np.linalg.slogdet(self.Sigma_y)
    for n in xrange(N):
      Tn = len(y[n])
      dif1 = expected_w[n] - self.mean_w
      p1 = log_det_sig_w + np.trace(np.dot(inv_sig_w,variance_w[n])) + np.dot(dif1, np.dot(inv_sig_w,dif1))
      p2 = 0
      for t in xrange(Tn):
        dif2 = y[n][t] - np.dot(Phi[n][t], expected_w[n])
        trace = np.sum(inv_sig_y*np.dot(Phi[n][t], np.dot(variance_w[n], Phi[n][t].T)))
        p2 += log_det_sig_y + trace + np.dot(dif2,np.dot(inv_sig_y,dif2))
      ans += p1 + p2
    return -0.5 * ans

  def _E_step(self, Phi, y, N, **params):
    expected_w = []
    variance_w = []
    inv_sig_w = np.linalg.inv(self.Sigma_w)
    inv_sig_y = np.linalg.inv(self.Sigma_y)
    for n in xrange(N):
      Tn = len(y[n])
      tmp1 = sum(map(lambda t: np.dot(Phi[n][t].T, np.dot(inv_sig_y, y[n][t])), range(Tn)))
      tmp2 = sum(map(lambda t: np.dot(Phi[n][t].T, np.dot(inv_sig_y, Phi[n][t])), range(Tn)))
      tmp2 = (tmp2 + tmp2.T) / 2.0 #For numerical issues (Make positive definite)
      swn = np.linalg.inv(inv_sig_w + tmp2)
      swn = (swn + swn.T) / 2.0 #For numerical issues (Make positive definite)
      wbar = np.dot( swn, tmp1 + np.dot(inv_sig_w,self.mean_w) )
      expected_w.append(wbar)
      variance_w.append(swn)
    return expected_w, variance_w

  def _M_step(self, Phi, y, expected_w, variance_w, N, D, K, **params):
    self.mean_w = sum(expected_w) / N;
    if (params['print_inner_params']): print "mean_w=", self.mean_w
    if (params['print_inner_lb']): print "lb(mean_w)=", self._EM_lowerbound(Phi, y, expected_w, variance_w, N, D, **params)
    sig_w = np.zeros((D,D))
    sig_y = np.zeros((K,K))
    sum_T = 0
    for n in xrange(N):
      Tn = len(y[n])
      dif = expected_w[n] - self.mean_w
      sig_w += variance_w[n] + np.outer(dif,dif)
      for t in xrange(Tn):
        dif2 = y[n][t] - np.dot(Phi[n][t], expected_w[n])
        sig_y += np.outer(dif2,dif2) + np.dot(np.dot(Phi[n][t],variance_w[n]),Phi[n][t].T)
      sum_T += Tn
    self.Sigma_w = sig_w / N
    self.Sigma_w = (self.Sigma_w + self.Sigma_w.T) / 2.0 #for numerical stability (pos-def mat)
    if (params['print_inner_params']): print "Sigma_w=", self.Sigma_w
    if (params['print_inner_lb']): print "lb(Sigma_w)=", self._EM_lowerbound(Phi, y, expected_w, variance_w, N, D, **params)
    self.Sigma_y = sig_y / sum_T
    self.Sigma_y = (self.Sigma_y + self.Sigma_y.T) / 2.0 #for numerical stability (pos-def mat)
    if (params['print_inner_params']): print "Sigma_y=", self.Sigma_y
    if (params['print_inner_lb']): print "lb(Sigma_y)=", self._EM_lowerbound(Phi, y, expected_w, variance_w, N, D, **params)

  def EM_train(self, Phi, y, **params):
    N,D,K = self._init_EM(Phi,y,**params)
    params.setdefault('max_iter', 10)
    params.setdefault('print_lowerbound', False)
    params.setdefault('print_inner_lb', False)
    params.setdefault('print_params', False)
    params.setdefault('print_inner_params', False)
    max_iter = params['max_iter']
    for i in xrange(max_iter):
      expected_w, variance_w = self._E_step(Phi,y,N,**params)
      if (params['print_lowerbound']):
        print "E-step LB: ", self._EM_lowerbound(Phi,y,expected_w,variance_w,N,D,**params)
      self._M_step(Phi, y, expected_w,variance_w,N,D,K,**params)
      if (params['print_lowerbound']):
        print "M-step LB: ", self._EM_lowerbound(Phi,y,expected_w,variance_w,N,D,**params)
    if params['print_params']:
      print "Parameters after optimization:"
      print "mean_w = ", self.mean_w
      print "Sigma_w = ", self.Sigma_w
      print "Sigma_y = ", self.Sigma_y

  @classmethod
  def build_from_stream(cls, stream):
    obj = cls()
    obj.mean_w = stream['mean_w']
    obj.Sigma_w = stream['Sigma_w']
    obj.Sigma_y = stream['Sigma_y']
    return obj

  def to_stream(self):
    ans = {'mean_w': self.mean_w, 'Sigma_w': self.Sigma_w, 'Sigma_y': self.Sigma_y}
    return ans


#Map time to robot angles and variances
class proMP_Indep_Robot_Angles:
  #Attributes:
  #kernel: A kernel object (like proMP_Gauss_Basis, see comments for more info)
  #model: List of proMP objects for every joint

  def __init__(self, kernel, **params):
    self.kernel = kernel
    if 'model' in params: self.model = params['model']
  
  def _get_training_matrices(self, W, **params):
    params.setdefault('robot_freq', 500)
    N = len(W)
    assert N>0
    D = np.shape(W[0])[1]
    Phi = []
    y = []
    for d in xrange(D):
      Phi_d = []
      y_d = []
      for n in xrange(N):
        Tn, dn = np.shape(W[n])
        time = np.linspace(0,1,Tn)
        Phi_n = []
        y_n = []
        tfac = float(params['robot_freq']) / Tn
        for t in xrange(Tn):
          y_nt = [W[n][t,d]]
          if 'Wdot' in params: y_nt.append(params['Wdot'][n][t,d])
          Phi_nt = [self.kernel(time[t])]
          if 'Wdot' in params: Phi_nt.append(tfac*self.kernel.time_deriv(time[t]))
          Phi_n.append(np.array(Phi_nt))
          y_n.append(np.array(y_nt))
        Phi_d.append(Phi_n)
        y_d.append(y_n)
      Phi.append(Phi_d)
      y.append(y_d)
    return Phi, y

  def train(self, W, **params):
    Phi, y = self._get_training_matrices(W, **params)
    D = len(y)
    self.model = [proMP(**params) for d in xrange(D)]
    for d in xrange(D):
      self.model[d].EM_train(Phi[d], y[d], **params)
  
  #Distribution marginalizing w
  def marginal_w(self, t, **params):
    assert t>=0 and t<=1
    params.setdefault('use_pos', True)
    params.setdefault('use_vel', False)
    Phi = []
    if params['use_pos']: Phi.append(self.kernel(t))
    if params['use_vel']: Phi.append(self.kernel.time_deriv(t))
    Phi = np.array(Phi)
    means = []
    covs = []
    for promp in self.model:
      m_mean, m_cov = promp.marginal_w(Phi, **params)
      means.append(m_mean)
      covs.append(m_cov)
    return means, covs

  #Condition the proMP to pass at time t through the point pos_t. If there is
  #uncertainty on the conditioned point pass it as the optional covariance 
  #matrix Sigma_t.
  def condition(self, t, **params):
    phi_t = []
    if 'pos_t' in params: phi_t.append(self.kernel(t))
    if 'vel_t' in params: phi_t.append(self.kernel.time_deriv(t,1))
    if 'acc_t' in params: phi_t.append(self.kernel.time_deriv(t,2))
    phi_t = np.array(phi_t)
    models = []
    for i in xrange(len(self.model)):
      promp = self.model[i]
      mean = []#np.array([mu_t[i]])
      cov_diag = []
      if 'pos_t' in params: mean.append(params['pos_t'][i])
      if 'vel_t' in params: mean.append(params['vel_t'][i])
      if 'acc_t' in params: mean.append(params['acc_t'][i])
      if 'Sigma_pos' in params: cov_diag.append(params['Sigma_pos'][i,i])
      if 'Sigma_vel' in params: cov_diag.append(params['Sigma_vel'][i,i])
      if 'Sigma_acc' in params: cov_diag.append(params['Sigma_acc'][i,i])
      if len(cov_diag) != 0:
        cov = np.diag(cov_diag)
        models.append(promp.condition(phi_t,mean,Sigma_q=cov))
      else:
        models.append(promp.condition(phi_t,mean))
    return proMP_Indep_Robot_Angles(self.kernel, model=models)

  def _compute_particular_traj(self, duration, ws, **params):
    params.setdefault('robot_freq', 500)
    tfac = float(params['robot_freq']) / duration
    ndof = len(self.model)
    q = []
    qdot = []
    qdotdot = []
    for t in xrange(duration):
      time = t / float(duration)
      phi_t = np.array([self.kernel(time), tfac * self.kernel.time_deriv(time,1), tfac**2*self.kernel.time_deriv(time,2)])
      qt = []
      qdott = []
      qdotdott = []
      for i in xrange(ndof):
        yt = np.dot(phi_t, ws[i]) # Don't sample with observation noise (Ignore Sigma_y)
        qt.append( yt[0] )
        qdott.append( yt[1] )
        qdotdott.append( yt[2] )
      q.append(qt)
      qdot.append(qdott)
      qdotdot.append(qdotdott)
    return q, qdot, qdotdot

  def sample_trajectory(self,duration,**params):
    ndof = len(self.model)
    ws = []
    for i in xrange(ndof):
      ws.append(np.random.multivariate_normal(self.model[i].mean_w, self.model[i].Sigma_w).T)
    q, qdot, qdotdot = self._compute_particular_traj(duration, ws, **params)
    return q, qdot, qdotdot

  def get_mean_traj(self, duration, **params):
    ndof = len(self.model)
    ws = []
    for i in xrange(ndof):
      ws.append(self.model[i].mean_w)
    return self._compute_particular_traj(duration, ws, **params)

  def to_stream(self):
    ans = {'kernel': self.kernel.to_stream(), 'model': map(lambda x: x.to_stream(), self.model)}
    return ans

  @classmethod
  def build_from_stream(cls, stream, kernel_stream_constr, model_stream_constr):
    obj = cls(kernel_stream_constr(stream['kernel']))
    obj.model = map(lambda sub_stream: model_stream_constr(sub_stream), stream['model'])
    return obj

#Map time to robot angles and variances for a full model that considers Gaussian
#dependencies between joints. The training matrices for every instance and time
#step are built by stacking for every joint its position and velocity 
#(See get_training_matrices code).
class proMP_Dependent_Robot_Angles:
  #Attributes:
  #kernel: A kernel object (like proMP_Gauss_Basis, see comments for more info)
  #model: One single proMP model for all joints
  #num_joints: Number of joints in the robot

  def __init__(self, kernel, **params):
    self.kernel = kernel
    if 'model' in params: self.model = params['model']
    if 'num_joints' in params: self.num_joints = params['num_joints']

  def _fill_features_zero(self, vec, d):
    ans = np.zeros(self.num_joints*len(vec))
    ans[d*len(vec):(d+1)*len(vec)] = vec
    return ans

  def _get_Phi_t(self, t, pos=True, vel=True, acc=False, **params):
    assert t>=0 and t<=1
    params.setdefault('vel_fac', 1.0) #velocity factor (Usually depends on exec. vel)
    Phi_t = []
    for d in xrange(self.num_joints):
      if pos:
        Phi_t.append( self._fill_features_zero(self.kernel(t), d) )
      if vel:
        Phi_t.append( params['vel_fac']*self._fill_features_zero(self.kernel.time_deriv(t), d) )
      if acc:
        Phi_t.append( params['vel_fac']**2*self._fill_features_zero(self.kernel.time_deriv(t,2), d) )
    return np.array(Phi_t)

  def __init_training(self,W,**params):
    params.setdefault('robot_freq', 500)
    N = len(W)
    assert N>0
    self.num_joints = D = np.shape(W[0])[1]
    Phi = []
    y = []
    for n in xrange(N):
      Tn, dn = np.shape(W[n])
      Phi_n = []
      y_n = []
      tfac = float(params['robot_freq']) / Tn
      time = np.linspace(0,1,Tn)
      for t in xrange(Tn):
        y_nt = []
        for d in xrange(D):
          y_nt.append(W[n][t,d])
          if 'Wdot' in params:
            y_nt.append(params['Wdot'][n][t,d])
        if 'Wdot' in params:
          Phi_n.append(self._get_Phi_t(time[t], True, True, False, vel_fac=tfac))
        else:
          Phi_n.append(self._get_Phi_t(time[t], True, False, False))
        y_n.append(np.array(y_nt))
      Phi.append(Phi_n)
      y.append(y_n)
    return Phi, y

  def train(self, W, **params):
    Phi, y = self.__init_training(W, **params)
    self.model = proMP(**params)
    self.model.EM_train(Phi, y, **params)

  def marginal_w(self, t, **params):
    params.setdefault('use_pos', True)
    params.setdefault('use_vel', False)
    params.setdefault('use_acc', False)
    Phi_t = self._get_Phi_t(t, params['use_pos'], params['use_vel'], params['use_acc'])
    return self.model.marginal_w(Phi_t, **params)

  #Condition the proMP to pass at time t through the point mu_t. If there is
  #uncertainty on the conditioned point pass it as the optional covariance 
  #matrix Sigma_t. Use optional parameters cond_pos and cond_vel to decide
  #on which combination of position and velocity to condition and set mu_t (
  #and possibly Sigma_t accordingly)
  def condition(self, t, **params):
    Phi_t = self._get_Phi_t(t, 'pos_t' in params, 'vel_t' in params, 'acc_t' in params)
    mean = []
    cov_diag = []
    if 'pos_t' in params: mean += params['pos_t'].tolist()
    if 'vel_t' in params: mean += params['vel_t'].tolist()
    if 'acc_t' in params: mean += params['acc_t'].tolist()
    if 'Sigma_pos' in params: cov_diag.append(params['Sigma_pos'])
    if 'Sigma_vel' in params: cov_diag.append(params['Sigma_vel'])
    if 'Sigma_acc' in params: cov_diag.append(params['Sigma_acc'])
    if len(cov_diag) != 0:
      cov = block_diag(*cov_diag)
      model = self.model.condition(Phi_t, mean, Sigma_q=cov)
    else:
      model = self.model.condition(Phi_t, mean)
    return proMP_Dependent_Robot_Angles(self.kernel, model=model, num_joints=self.num_joints)

  def sample_trajectory(self,duration,**params):
    params.setdefault('robot_freq', 500)
    q = []
    qdot = []
    qdotdot = []
    ws = np.random.multivariate_normal(self.model.mean_w, self.model.Sigma_w).T
    tfac = float(params['robot_freq']) / duration
    for t in xrange(duration):
      time = t / float(duration)
      phi_t = self._get_Phi_t(time, True, True, True, vel_fac = tfac)
      #phi_t = np.array([self.kernel(time), tfac * self.kernel.time_deriv(time)])
      yt = np.dot(phi_t, ws)
      qt = []
      qdott = []
      qdotdott = []
      for i in xrange(self.num_joints):
        qt.append( yt[3*i] )
        qdott.append( yt[3*i + 1] )
        qdotdott.append( yt[3*i + 2] )
      q.append(qt)
      qdot.append(qdott)
      qdotdot.append(qdotdott)
    return q, qdot, qdotdot

  def to_stream(self):
    ans = {'kernel': self.kernel.to_stream(), 'model': self.model.to_stream(), 'num_joints': self.num_joints}
    return ans

  @classmethod
  def build_from_stream(cls, stream, kernel_stream_constr, model_stream_constr):
    obj = cls(kernel_stream_constr(stream['kernel']), model = \
        model_stream_constr(stream['model']), num_joints = stream['num_joints'])
    return obj

class Robot_Ball_Overlap:

  def _canonical_constant(self,lamb,nab,lamb_inv,dims=3):
    lsign, lamb_log_det = np.linalg.slogdet(lamb)
    inner = np.dot(nab,np.dot(lamb_inv,nab))
    return -0.5*(dims*np.log(2*np.pi) - lamb_log_det + inner)

  def _canonical_terms(self, mean, cov):
    lam = np.linalg.inv(cov)
    nab = np.dot(lam, mean)
    c = self._canonical_constant(lam,nab,cov)
    return lam, nab, c

  def __init__(self,kinematics):
    self.kinematics = kinematics

  def trajectory_overlap(self, means_ball, covs_ball, means_robot, covs_robot, ball_offset):
    N = len(means_robot)
    dims = 3
    ans = []
    for n in xrange(N):
      lam_ball, nab_ball, c_ball = self._canonical_terms(means_ball[n+ball_offset], \
          covs_ball[n+ball_offset])
      lam_rob, nab_rob, c_rob = self._canonical_terms(means_robot[n],covs_robot[n])
      lam_hat = lam_ball + lam_rob
      nab_hat = nab_ball + nab_rob
      c_hat = self._canonical_constant(lam_hat,nab_hat,np.linalg.inv(lam_hat))
      ans.append( np.exp(c_ball + c_rob - c_hat) )
    return ans

  def trajectory_cost(self, means_ball, covs_ball, means_robot, covs_robot, ball_offset):
    return sum(self.trajectory_overlap(means_ball,covs_ball,means_robot,covs_robot,ball_offset))

  def compute_robot_trajectory(self,t0,T,ball_times,proMP_function):
    Tball = len(ball_times)
    ball_offset = Tball
    means = []
    covs = []
    orients = []
    for t in xrange(Tball):
      trob = (ball_times[t] - t0) / T
      if trob >= 0 and trob <= 1:
        ball_offset = min(ball_offset, t)
        mean_wt, Sigma_wt = proMP_function(trob)
        mean_rt, jac, ori = self.kinematics.position_and_jac(mean_wt)
        jac = jac[0:3,:]
        Sigma_rt = np.dot(jac, np.dot(Sigma_wt, jac.T))
        means.append(mean_rt)
        covs.append(Sigma_rt)
        orients.append(ori)
    return means, covs, orients, ball_offset

  def start_time_convolution(self, T, mean_balls, covs_ball, ball_times, proMP_function, **params):
    rmeans, rcovs, rorient, ball_offset = self.compute_robot_trajectory(ball_times[0], T, ball_times, proMP_function)
    n = len(ball_times) - len(rmeans)
    init_times = np.linspace(ball_times[0],ball_times[-1]-T,n)
    values = []
    for i in xrange(n):
      val = self.trajectory_cost(mean_balls, covs_ball, rmeans, rcovs, i)
      values.append(val)
    return init_times, values

  def opt_start_time(self, T, mean_balls, covs_ball, ball_times, proMP_function, **params):
    init_times, values = self.start_time_convolution(T,mean_balls,covs_ball,ball_times,proMP_function,**params)
    index = np.argmax(values)
    return init_times[index]


class ProbInvKinematics:
  #params:
  #fwd_k: A forward kinematics object

  def __laplace_cost_and_grad(self, theta, mu_theta, inv_sigma_theta, mu_x, inv_sigma_x):
    f_th, jac_th, ori = self.fwd_k.position_and_jac(theta)
    jac_th = jac_th[0:3,:]
    diff1 = theta - mu_theta
    tmp1 = np.dot(inv_sigma_theta, diff1)
    diff2 = f_th - mu_x
    tmp2 = np.dot(inv_sigma_x, diff2)
    nll = 0.5*(np.dot(diff1,tmp1) + np.dot(diff2,tmp2))
    grad_nll = tmp1 + np.dot(jac_th.T,tmp2)
    return nll, grad_nll

  def __init__(self, fwd_kinematics):
    self.fwd_k = fwd_kinematics

  def inv_kin(self, mu_theta, sig_theta, mu_x, sig_x):
    inv_sig_theta = np.linalg.inv(sig_theta)
    inv_sig_x = np.linalg.inv(sig_x)
    cost_grad = lambda theta: self.__laplace_cost_and_grad(theta, mu_theta, inv_sig_theta, mu_x, inv_sig_x)
    cost = lambda theta: cost_grad(theta)[0]
    grad = lambda theta: cost_grad(theta)[1]
    res = opt.minimize(cost, mu_theta, method='BFGS', jac=grad)
    post_mean = res.x
    post_cov = res.hess_inv
    return post_mean, post_cov

def load_stream(filename):
  f = file(filename,'rb')
  dataf = np.load(f)
  ans = dataf['arr_0'].tolist()
  f.close()
  return ans

def save_stream(filename, stream):
  f = file(filename, 'wb')
  np.savez(f, stream)
  f.close()
