
import numpy as np
import random as rnd
import loadData as myld
from proMP import *
import barret
import time as time_module
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
import ball_hit_utils as hit_utils

def plot_exec_traj(t0, q, qdot, qdotdot, exec_traj, **params):
  params.setdefault('robot_freq', 500)
  axis = ['X', 'Y', 'Z']
  deltaT = 1.0 / params['robot_freq']
  time = t0 + deltaT*np.array(range(np.shape(q)[0]))
  exec_q = np.array(exec_traj['q'])
  exec_qdot = np.array(exec_traj['qdot'])
  exec_qdotdot = np.array(exec_traj['qdotdot'])
  exec_time = exec_traj['arm_times']
  ax1 = None
  ax2 = None
  ax3 = None
  for i in xrange(7):
    if ax1 == None:
      ax1 = plt.subplot(7,3,3*i+1)
    else:
      plt.subplot(7,3,3*i+1,sharex=ax1)
    plt.plot(time, q[:,i], 'r')
    plt.plot(exec_time, exec_q[:,i], 'g')
    plt.ylabel('q_' + str(i+1))
    if ax2 == None:
      ax2 = plt.subplot(7,3,3*i+2)
    else:
      plt.subplot(7,3,3*i+2,sharex=ax2)
    plt.plot(time, qdot[:,i], 'r')
    plt.plot(exec_time, exec_qdot[:,i], 'g')
    if ax3 == None:
      ax3 = plt.subplot(7,3,3*i+3)
    else:
      plt.subplot(7,3,3*i+3,sharex=ax3)
    plt.plot(time, qdotdot[:,i], 'r')
    plt.plot(exec_time, exec_qdotdot[:,i], 'g')
  plt.xlabel('Time')
  plt.show()


def test_kinematics():
  kinematics = barret.BarretKinematics()
  Q = np.loadtxt('test_files/input_angles.in')
  Q = Q.T[:,0:7]
  print "Q=", Q
  x = np.loadtxt('test_files/output_pos.out')
  xp, op = kinematics.end_eff_trajectory(Q)
  print "Predicted: ", xp
  print "Real: ", x.T
  diff = x.T - xp
  print "Diff: ", diff

def trainTableTennis(**params):
  kernel_params = params['kernel_params']
  data = myld.loadOkanData()
  strike_data, back_data = myld.segment_okan_data(data)
  kbuilder = KernelBuilder.get_builder(params['kernel_type'])
  kernel = kbuilder(**params['kernel_params'])
  model_fname = params['model_fname']
  if params['promp_type'] == 'indep':
    robot_promp = proMP_Indep_Robot_Angles(kernel)
  elif params['promp_type'] == 'full':
    robot_promp = proMP_Dependent_Robot_Angles(kernel)
  params.setdefault('print_lowerbound', False)
  params.setdefault('print_params', False) 
  params.setdefault('max_iter', 40)
  W = []
  Wdot = []
  for instance in strike_data:
    W.append(instance['q'])
    Wdot.append(instance['qdot'])
  robot_promp.train(W,Wdot,**params)
  stream = robot_promp.to_stream()
  fout = file(model_fname, 'wb')
  np.savez(fout, stream)
  return robot_promp

def trainTableTennisSeb(**params):
  kernel_params = params['kernel_params']
  data = myld.loadSebasData(**params)
  kbuilder = KernelBuilder.get_builder(params['kernel_type'])
  kernel = kbuilder(**params['kernel_params'])
  model_fname = params['model_fname']
  if params['promp_type'] == 'indep':
    robot_promp = proMP_Indep_Robot_Angles(kernel)
  elif params['promp_type'] == 'full':
    robot_promp = proMP_Dependent_Robot_Angles(kernel)
  params.setdefault('print_lowerbound', False)
  params.setdefault('print_params', False) 
  params.setdefault('max_iter', 30)
  W = []
  Wdot = []
  for instance in data:
    W.append(instance['q'])
    Wdot.append(instance['qdot'])
  #robot_promp.train(W, Wdot=Wdot, **params)
  robot_promp.train(W, **params)
  stream = robot_promp.to_stream()
  fout = file(model_fname, 'wb')
  np.savez(fout, stream)
  return robot_promp

def testTableTennis(robot, nsamples, **params):
  params.setdefault('plot_end_eff', False)
  params.setdefault('promp_type', 'indep')
  params.setdefault('kernel_type', 'poly')
  params.setdefault('pre_traj_time', 300)
  params.setdefault('post_traj_time', 100)
  params.setdefault('traj_time', 250)
  params.setdefault('go_init_time', 500)
  params.setdefault('robot_freq', 500)
  params.setdefault('model_fname', 'saved_models/' + params['promp_type'] + '_' + params['kernel_type'] + '_promp.npz')
  model_fname = params['model_fname']
  if os.path.isfile(model_fname):
    print "Loading previous trained model..."
    kbuild = KernelBuilder.get_stream_builder(params['kernel_type'])
    stream = load_stream(model_fname)
    if params['promp_type'] == 'full':
      robot_promp = proMP_Dependent_Robot_Angles.build_from_stream(stream, \
          kbuild, proMP.build_from_stream)
    else:
      robot_promp = proMP_Indep_Robot_Angles.build_from_stream(stream, \
          kbuild, proMP.build_from_stream)
    print "Model loaded. Now sampling trajectories"
  else:
    print "Beginning training..."
    robot_promp = trainTableTennisSeb(print_params=True, print_lowerbound=True, **params)
    print "Training ended. Now sampling and sending trajectories"
  kinematics = barret.BarretKinematics()
  pos0, Sigma0 = hit_utils.proMP_function(robot_promp, 0, **params)
  zero_init_vel_promp = robot_promp.condition(0, vel_t=np.zeros(7), Sigma_vel=0.01*np.eye(7))
  #zero_init_vel_promp = robot_promp.condition(0, vel_t=np.zeros(7), Sigma_vel=0.001*np.eye(7), acc_t=np.zeros(7), Sigma_acc=10*np.eye(7))
  for i in xrange(nsamples):
    barret.go_to_state(robot, pos0, params['go_init_time'])
    time_module.sleep(params['go_init_time']/float(params['robot_freq']))
    #qt,qdot,qdotdot = robot_promp.sample_trajectory(params['traj_time'])
    qt,qdot,qdotdot = zero_init_vel_promp.sample_trajectory(params['traj_time'])
    if params['plot_end_eff']:
      fig = plt.figure()
      ax = fig.add_subplot(111, projection='3d')
      pos, orient = kinematics.end_eff_trajectory(qt)
      ax.plot(pos[:,0],pos[:,1],zs=pos[:,2])
      ax.set_xlabel("X")
      ax.set_ylabel("Y")
      ax.set_zlabel("Z")
      plt.show()
    #q0, qd0 = robot.current_state()
    #q_init, qd_init, qdd_init = barret.plan_poly_traj(pos0, qt[0], params['pre_traj_time'], init_vel=np.zeros(7), des_vel=qdot[0], des_acc=qdotdot[0], prior_prec=0.0001, poly_order=5)
    barret.go_to_state(robot, qt[0], params['pre_traj_time'])
    rob_time = barret.RobotClock(robot)
    t0 = rob_time.time() + 1.0
    qstop, qdotstop, qdotdotstop = barret.stop_traj(qt[-1], qdot[-1], params['post_traj_time'], init_acc=qdotdot[-1], **params)
    qt = qt + qstop
    qdot = qdot + qdotstop
    qdotdot = qdotdot + qdotdotstop
    #time_module.sleep(switch_time / 500.0)
    robot.append_trajectory(qt,qdot,qdotdot=qdotdot,start_time=t0)
    #robot.append_trajectory(qt,qdot,start_time=t0)
    robot.send_program()
    time_module.sleep(2.5)
    exec_traj = robot.get_arm_traj(2000)
    plot_exec_traj(t0, np.array(qt), np.array(qdot), np.array(qdotdot), exec_traj, **params)
    #time_module.sleep(4)


#toyExampleTest()
#test_kinematics()
kernel_params = {'sigma_kernel': 0.45, 'num_basis': 4, 'degree': 3}
#trainTableTennis(kernel_params, print_lowerbound=True, print_params=True, max_iter=30, promp_type='full', model_fname='saved_models/full_promp.npz')
#robot = barret.Robot("wam","7646")
robot = barret.Robot("localhost","7646")
#robot = barret.Robot("blies","7646")
#trajectory_robot_test(robot, kernel_params, 5, True)
testTableTennis(robot, 10, plot_end_eff=False, promp_type='full', kernel_params=kernel_params, remove_bad_files=True, kernel_type="sqexp")
