
import numpy as np

def rot_x(theta):
  M = np.array([[1,0,0,0],\
      [0,np.cos(theta),-np.sin(theta),0],\
      [0,np.sin(theta),np.cos(theta),0],\
      [0,0,0,1]])
  return M

def rot_y(theta):
  M = np.array([[np.cos(theta),0,np.sin(theta),0],\
      [0,1,0,0],\
      [-np.sin(theta),0,np.cos(theta),0],\
      [0,0,0,1]])
  return M

def rot_z(theta):
  M = np.array([[np.cos(theta),-np.sin(theta),0,0],\
      [np.sin(theta),np.cos(theta),0,0],\
      [0,0,1,0],\
      [0,0,0,1]])
  return M

def rot2euler(r):
  beta = np.arctan2(-r[2,0], np.sqrt(r[0,0]**2 + r[1,0]**2))
  alpha = np.arctan2(r[1,0]/np.cos(beta), r[0,0]/np.cos(beta))
  gamma = np.arctan2(r[2,1]/np.cos(beta), r[2,2]/np.cos(beta))
  return alpha, beta, gamma

def euler2rot(alpha, beta, gamma):
  M = np.dot(rot_z(alpha),np.dot(rot_y(beta),rot_x(gamma)))
  return M[0:3,0:3]

A = euler2rot(0.5,1.2,-0.5)
print A
a,b,c = rot2euler(A)
print [a,b,c]
