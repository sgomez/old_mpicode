
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import ball_data
import barret
import time
import ball_hit_utils
import sys

sys.path.append('../kalman')
import kalman
import ball_model

def split_diff_cameras(ball_data):
  ans = {'t1': [], 'x1': [], 't3': [], 'x3': []}
  for inst in ball_data:
    time_id = 't' + str(inst['cam_id'])
    pos_id = 'x' + str(inst['cam_id'])
    ans[time_id].append(inst['time'])
    ans[pos_id].append(inst['pos'])
  return ans

def plot_hist_cam_diff(ball_data):
  prev_time = -1
  prev_pos = []
  distances = []
  for inst in ball_data:
    cpos = np.array(inst['pos'])
    if abs(inst['time'] - prev_time) < 1e-6:
      dist_vec = cpos - prev_pos
      dist = np.sqrt(np.dot(dist_vec, dist_vec))
      distances.append(dist)
    prev_time = inst['time']
    prev_pos = cpos
  plt.hist(distances,30)
  plt.xlabel('Distances')
  plt.title('Distances between cam1 and cam3 ball positions')
  plt.show()

def plot_traj(data, kin):
  obs_traj, obs_orient = kin.end_eff_trajectory(data['q'])
  ball_data = split_diff_cameras(data['ball'])
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')

  #plot cam1
  pos = np.array(ball_data['x1'])
  ax.scatter(pos[:,0], pos[:,1], zs=pos[:,2], c='b')
  #plot cam3
  pos = np.array(ball_data['x3'])
  ax.scatter(pos[:,0], pos[:,1], zs=pos[:,2], c='r')
  #plot obs_traj
  ax.plot(obs_traj[:,0], obs_traj[:,1], zs=obs_traj[:,2], c='r')
  ax.set_title('Recorded Info')
  plt.show()

def plot_incoming_ball(data, end_time, **params):
  t, x = ball_hit_utils.extract_incoming_ball_traj(data['ball'], end_time)
  plt.figure(1)
  for i in xrange(3):
    plt.subplot(3,1,i + 1)
    plt.plot(t,x[:,i],'rx')
    plt.ylabel('x_' + str(i))
  plt.xlabel('Time')
  plt.show()


def plot_each_joint(data, **params):
  params.setdefault('title', 'Joint angle and velocities vs time')
  t = data['rob_time']
  q = data['q']
  qdot = data['qdot']
  tao, ndof = np.shape(q)
  plt.figure(1)
  for i in xrange(ndof):
    plt.subplot(ndof,2,2*i + 1)
    plt.plot(t,q[:,i])
    plt.ylabel('q_' + str(i))
    plt.subplot(ndof,2,2*i + 2)
    plt.plot(t,qdot[:,i])
    plt.ylabel('\dot{q}_' + str(i))
  plt.xlabel('Time')
  plt.show()

def split_traj_and_plot(data, **params):
  params.setdefault('title', 'Splitted joint angles and velocities vs time')
  splitted = ball_hit_utils.zero_cross_vel_split(data['rob_time'], data['q'], data['qdot'])
  sdata = {'q': splitted['q'], 'qdot': splitted['qdot'], 'rob_time': splitted['time']}
  plot_each_joint(sdata, title=params['title'])
  return splitted

def main(argv):
  argc = len(argv)
  if argc == 1:
    print "Error: Pass as argument the filename to plot"
    printHelp(argv)
    return 1
  filename = argv[1]
  f = file(filename,'rb')
  data = np.load(f)
  kin = barret.BarretKinematics()
  plot_each_joint(data)
  splitted = split_traj_and_plot(data)
  plot_incoming_ball(data, splitted['start_time'])
  plot_traj(data, kin)
  plot_hist_cam_diff(data['ball'])

if __name__ == "__main__":
  main(sys.argv)
