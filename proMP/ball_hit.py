
import numpy as np
import proMP
import barret
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
import sys
import ball_data
import ball_hit_utils as hit_utils

sys.path.append('../kalman')
import kalman
import ball_model

def reset_robot(robot):
  robot.clear_program()
  robot.append_clear_ball_obs()
  robot.append_clear_sim_ball()
  robot.send_program()

def go_to_init_mean(robot, promp_fun, des_time):
  mean, cov = promp_fun(0)
  barret.go_to_state(robot, mean, des_time, qdot_des=np.zeros(7))
  time.sleep(des_time / 500.0)

def do_conditioning(robot_promp, kinematics, cond_ixs, t0, T, ball_times, cond_means, cond_covs, ball_offset, **params):
  inv_kin = proMP.ProbInvKinematics(kinematics)
  if 'init_q' in params:
    if 'init_qdot' in params and params['promp_type']=='full':
      init_arr = []
      for d in xrange(7):
        init_arr.append(params['init_q'][d])
        init_arr.append(params['init_qdot'][d])
      assert len(init_arr) == 14
      cond_promp = robot_promp.condition(0, init_arr, cond_pos=True, cond_vel=True) #Condition with no uncertainty
    else:
      if 'init_qdot' in params:
        cond_promp = robot_promp.condition(0, pos_t=params['init_q'], Sigma_pos=0.0001*np.eye(7), vel_t=params['init_qdot'], Sigma_vel=0.0001*np.eye(7))
      else:
        cond_promp = robot_promp.condition(0, pos_t=params['init_q'], Sigma_pos=0.0001*np.eye(7))
    q0m, q0S = hit_utils.proMP_function(cond_promp, 0, **params)
    assert np.allclose(q0m, params['init_q'], 1e-4)
  else:
    cond_promp = robot_promp
  promp_fun = lambda t: hit_utils.proMP_function(cond_promp, t, **params)
  for rob_ix in cond_ixs:
    cond_ix = rob_ix + ball_offset
    hit_time = ball_times[cond_ix]
    rob_time = (hit_time - t0) / T
    mu_th, Sig_th = promp_fun(rob_time)
    #print "prior: t=", hit_time, "mu=", mu_th, "S=", Sig_th
    #print "lhood: mu_x=", ball_means[cond_ix], " S_x=", ball_covs[cond_ix]
    mu_th2, Sig_th2 = inv_kin.inv_kin(mu_th, Sig_th, cond_means[cond_ix], cond_covs[cond_ix])
    #mah_dist = np.sqrt(np.dot(mu_th - mu_th2, np.dot(np.linalg.inv(Sig_th), mu_th - mu_th2)))
    #if mah_dist > 3: continue
    #print "posterior: mu=", mu_th2, "S=", Sig_th2
    cond_promp = cond_promp.condition(rob_time, pos_t=mu_th2, Sigma_pos=Sig_th2) #Condition on the posterior given by inv_kin
    promp_fun = lambda t: hit_utils.proMP_function(cond_promp, t, **params)
  return cond_promp, promp_fun

def hit_ball(robot, robot_promp, kfilter, kinematics, deltaT, **params):
  params.setdefault('init_ball_obs', 6)
  params.setdefault('duration', 0.5)
  params.setdefault('robot_freq', 500)
  params.setdefault('capture_every', 0.010)
  params.setdefault('pred_len', int(round(1.2/deltaT)))
  params.setdefault('sim_ball', False)
  params.setdefault('sim_ball_len', 120)
  params.setdefault('move_init_pos', False)
  params.setdefault('cond_init_q', True)
  params.setdefault('zero_init_vel', True)
  params.setdefault('sleep_soft_margin', True)
  params.setdefault('do_ball_condition', True)
  params.setdefault('sample_traj', False)
  params.setdefault('hard_time_margin', 0.030) #Consider time of thinking and network latencies
  params.setdefault('soft_time_margin', 0.060) #Consider also time of thinking of moving to initial position
  params.setdefault('top_k_cond', 3)
  params.setdefault('deltaT_tol', 2e-3)
  params.setdefault('stop_traj', False)
  rvision = ball_data.BallVision(robot)
  zero_init_vel_promp = robot_promp.condition(0, vel_t=np.zeros(7), Sigma_vel=0.01*np.eye(7))
  if params['zero_init_vel']:
    robot_promp = zero_init_vel_promp
  promp_fun = lambda x: hit_utils.proMP_function(robot_promp, x, **params)
  vfilter = ball_data.BallFilter(kfilter, deltaT=deltaT, deltaT_tol=params['deltaT_tol'])
  overlap = proMP.Robot_Ball_Overlap(kinematics)
  counter = 1
  rob_time = barret.RobotClock(robot) #approximates robot times from PC time
  while True:
    reset_robot(robot)
    go_to_init_mean(robot, promp_fun, 500)
    vfilter.reset()

    if params['sim_ball']:
      if 'sim_ball_file' in params:
        sim_ball_file = file(params['sim_ball_file'], 'rb')
        sim_ball_data = np.load(sim_ball_file)
        sim_ball_traj = sim_ball_data['raw_ball_obs5']
        rob_time.sync(robot)
        #sim_ball_time = deltaT*np.array(range(len(sim_ball_traj))) + 3.0 + rob_time.time()
        sim_ball_time = sim_ball_data['raw_ball_times5']
        sim_ball_time += 3.0 + rob_time.time() - sim_ball_time[0]
        sim_ball_file.close()
      else:
        kf_sampler = kfilter if not 'kf_sampler' in params else params['kf_sampler']
        sim_ball_traj, _ = kf_sampler.generate_sample(params['sim_ball_len'], actions=np.ones((params['sim_ball_len'], 1)))
        rob_time.sync(robot)
        sim_ball_time = deltaT*np.array(range(params['sim_ball_len'])) + 3.0 + rob_time.time() #Start sending balls in 3 sec
      print "Sending simulated ball trajectory to the robot"
      robot.append_sim_ball(sim_ball_time, sim_ball_traj)
      robot.send_program()

    #1) Capture some ball observations (ex. 6, take around 100ms)
    while not rvision.get_raw_ball_observations(vfilter):
      time.sleep(params['capture_every'])
      #print ".",

    rob_time.sync(robot)
    start_time = rob_time.time()
    print "Got some good ball observations ... (t_0 = ", vfilter.ball_time[0], "s, t_f = ", vfilter.ball_time[-1], " s, t = ",\
        start_time, " s, len = ", len(vfilter.ball_time), ")"
    print "first = ", vfilter.ball_obs[0], " last = ", vfilter.ball_obs[-1]
    to_save = {'ball_times1': list(vfilter.ball_time), 'ball_obs1': list(vfilter.ball_obs)}

    #2) Compute optimal t0 from those observations (around 200ms)
    means,covs,Pvals,As,Bs,Cs,Ds = kfilter.forward_recursion(np.array(vfilter.ball_obs),np.ones((params['pred_len'],1)),deltaTs=vfilter.deltaTs)
    ball_means = map(lambda mean: mean[range(0,6,2)], means)
    ball_covs = map(lambda cov: cov[range(0,6,2),:][:,range(0,6,2)], covs)
    ball_times = deltaT*np.array(xrange(params['pred_len'])) + vfilter.ball_time[0]
    T = params['duration']
    start_times, start_values = overlap.start_time_convolution(T,ball_means,ball_covs,ball_times,promp_fun)
    t0 = start_times[np.argmax(start_values)]
    to_save.update({'kf_means2': list(ball_means), 'kf_covs2': list(ball_covs), \
        'kf_times2': list(ball_times), 'T': T, 't0': t0, 'deltaT': deltaT, \
        'start_times': start_times, 'start_values': start_values})
    print "Optimal fire time: ", t0
    curr_time = rob_time.time()
    print "Current time: ", curr_time, ", Time spent computing t0: ", curr_time-start_time
    time_to_act = t0 - curr_time
    print "Time to act after computing t0: ", time_to_act
    if time_to_act < params['hard_time_margin']:
      print "Time limit exceded, aborting"
      continue
    #2.1) Do conditioning and go to that initial position
    if params['move_init_pos']:
      rmeans, rcovs, rorient, ball_offset = overlap.compute_robot_trajectory(t0, T, ball_times, promp_fun)
      overlaps = overlap.trajectory_overlap(ball_means, ball_covs, rmeans, rcovs, ball_offset)
      cond_ixs = hit_utils.top_k_hitting_points(overlaps, params['top_k_cond'])
      if params['do_ball_condition']:
        cond_promp, cond_fun = do_conditioning(robot_promp, kinematics, cond_ixs, t0, T, ball_times, ball_means, ball_covs, ball_offset, **params)
      else:
        cond_promp = robot_promp
        cond_fun = promp_fun
      q0, q0dot = cond_fun(0)
      q0dot = np.zeros(7) if params['zero_init_vel'] else q0dot[0]
      print 'q0=', q0
      time_to_act = t0 - rob_time.time()
      going_time_steps = int(500*time_to_act) - 10 #Subtracting a small number of steps to account for latencies
      barret.go_to_state(robot, q0, going_time_steps, qdot_des=q0dot)
      curr_time = rob_time.time()
      print "Current time after computing initial position: ", curr_time, ", Time spent computing so far: ", curr_time-start_time
      #time.sleep(going_time)
    else:
      tmp1, tmp2 = robot.current_state()
      q0 = tmp1[0]
      q0dot = tmp2[0]

    #3) Get the other ball observations seen so far
    time_to_act = t0 - rob_time.time()
    time_to_sleep = time_to_act - params['soft_time_margin']
    if time_to_sleep>0.01 and params['sleep_soft_margin']: 
      print "Sleeping for ", time_to_sleep, " s"
      time.sleep(time_to_sleep)
    rvision.get_raw_ball_observations(vfilter)
    to_save.update({'ball_times3': list(vfilter.ball_time), 'ball_obs3': list(vfilter.ball_obs),\
        'raw_ball_times3': list(vfilter.raw_ball_time), 'raw_ball_obs3': list(vfilter.raw_ball_obs), \
        'hidden': list(vfilter.is_hidden)})
    curr_time = rob_time.time()
    print "Got new observations, time spent so far: ", curr_time-start_time, "s"

    #4) Condition the proMP on hitting the ball and initial position and vel
    means,covs,Pvals,As,Bs,Cs,Ds = kfilter.forward_recursion(np.array(vfilter.ball_obs),np.ones((params['pred_len'],1)), hidden=vfilter.is_hidden, deltaTs=vfilter.deltaTs)
    ball_means = map(lambda mean: mean[range(0,6,2)], means)
    ball_covs = map(lambda cov: cov[range(0,6,2),:][:,range(0,6,2)], covs)
    rmeans, rcovs, rorient, ball_offset = overlap.compute_robot_trajectory(t0, T, ball_times, promp_fun)
    overlaps = overlap.trajectory_overlap(ball_means, ball_covs, rmeans, rcovs, ball_offset)
    cond_ixs = hit_utils.top_k_hitting_points(overlaps, params['top_k_cond'])
    #q0, q0dot = promp_fun(0)
    if params['do_ball_condition']:
      if params['cond_init_q']:
        cond_promp, cond_fun = do_conditioning(robot_promp, kinematics, cond_ixs, t0, T, ball_times, ball_means, ball_covs, ball_offset, init_q=q0, init_qdot=q0dot, **params)
      else:
        cond_promp, cond_fun = do_conditioning(robot_promp, kinematics, cond_ixs, t0, T, ball_times, ball_means, ball_covs, ball_offset, **params)
    else:
      cond_promp = robot_promp
      cond_fun = promp_fun

    rmeans_cond, rcovs_cond, rorient_cond, ball_offset_cond = overlap.compute_robot_trajectory(t0, T, ball_times, cond_fun)
    to_save.update({'kf_means4':ball_means, 'kf_covs4': ball_covs, 'rmeans': rmeans, 'rcovs': rcovs, 'cond_rmeans': rmeans_cond, 'cond_rcovs': rcovs_cond, 'kf_times': ball_times, 'ball_offset': ball_offset, 'ball_offset_cond': ball_offset_cond, 'overlaps': overlaps})

    #5) Send the learned trajectory to start at time t0
    traj_steps = int(round(T*params['robot_freq']))
    if params['sample_traj']:
      q, qdot, qdotdot = cond_promp.sample_trajectory(traj_steps)
    else:
      q, qdot, qdotdot = cond_promp.get_mean_traj(traj_steps)
    if params['stop_traj']:
      qstop, qdotstop, qdotdotstop = barret.stop_traj(q[-1], qdot[-1], 50, init_acc=qdotdot[-1], poly_order=4, **params)
      q += qstop
      qdot += qdotstop
      qdotdot += qdotdotstop
    to_save.update({'q': q, 'qdot': qdot, 'qdotdot': qdotdot})
    curr_time = rob_time.time()
    time_to_act = t0 - curr_time
    print "All finished. Sending data to robot..., time to act is ", time_to_act, "s, time spent so far is ", curr_time-start_time
    robot.append_trajectory(q, qdot, qdotdot=qdotdot, start_time=(t0-0.006))
    robot.send_program()

    #6) Get more relevant info
    rvision.get_raw_ball_observations(vfilter)
    to_save.update({'ball_times5': list(vfilter.ball_time), 'ball_obs5': list(vfilter.ball_obs), 'raw_ball_times5': list(vfilter.raw_ball_time), 'raw_ball_obs5': list(vfilter.raw_ball_obs)})
    time.sleep(2)
    executed_traj = robot.get_arm_traj(2000)
    rvision.get_raw_ball_observations(vfilter)
    to_save.update({'ball_times6': list(vfilter.ball_time), 'ball_obs6': list(vfilter.ball_obs), 'raw_ball_times6': list(vfilter.raw_ball_time), 'raw_ball_obs6': list(vfilter.raw_ball_obs)})
    to_save.update({'exec_q': executed_traj['q'], 'exec_qdot': executed_traj['qdot'], 'exec_qdotdot': executed_traj['qdotdot'], 'exec_time': executed_traj['arm_times']})
    robot.clear_program()
    robot.append_clear_ball_obs()
    robot.send_program()

    #last) Save data and get ready for next iteration
    filename = 'tmp_files/ball_hit_' + hit_utils.new_time_id() + '.npz'
    f = file(filename, 'wb')
    np.savez(f, **to_save)
    f.close()
    counter += 1

def run_hit_ball(argv, **params):
  params.setdefault('host', 'localhost')
  params.setdefault('promp_type', 'indep')
  params.setdefault('kernel_type', 'sqexp')
  params.setdefault('model_fname', 'saved_models/' + params['promp_type'] + '_' + params['kernel_type'] + '_promp.npz')
  #params.setdefault('kfilter_fname', 'saved_models/full_ball_em.npz')
  params.setdefault('kfilter_fname', 'saved_models/trained_kf.npz')
  params.setdefault('sim_kf_sampler', 'saved_models/no_prior_P0_kf.npz')
  robot = barret.Robot(params['host'],"7646")
  model_fname = params['model_fname']
  kalman_fname = params['kfilter_fname']

  if argv[1]=='sim':
    if len(argv)>=3:
      params['sim_ball_file'] = argv[2]
    else:
      kf_sampler, deltaT = ball_model.load_ball_model(params['sim_kf_sampler'])
      params['kf_sampler'] = kf_sampler

  kernel_builder = proMP.KernelBuilder.get_stream_builder(params['kernel_type'])
  if params['promp_type'] == 'full':
    promp_builder = lambda stream: proMP.proMP_Dependent_Robot_Angles.build_from_stream(stream,\
        kernel_builder, proMP.proMP.build_from_stream)
  elif params['promp_type'] == 'indep':
    promp_builder = lambda stream: proMP.proMP_Indep_Robot_Angles.build_from_stream(stream,\
        kernel_builder, proMP.proMP.build_from_stream)
  if os.path.isfile(model_fname):
    print "Loading previous trained proMP model..."
    stream = proMP.load_stream(model_fname)
    robot_promp = promp_builder(stream)
  else:
    print "Error: The proMP file ", model_fname, " does not exist"
    return False
  kinematics = barret.BarretKinematics()
  if os.path.isfile(kalman_fname):
    print "Loading previous trained ball model..."
    kf, deltaT = ball_model.load_ball_model(kalman_fname)
  else:
    print "Error: The ball model file ", model_fname, " does not exist"
    return False
  hit_ball(robot, robot_promp, kf, kinematics, deltaT, **params)
  return 0

def printHelp(argv):
  print "Usage: ", argv[0], " <command> (<options>)"
  print "Commands:"
  print "h, help: Prints this help message"
  print "run: Runs the ball hitting task in the robot"
  print "plot <file> <plot_type>: Plots the ball and end effector trajectory saved in file <file>"

def plot_step2(data):
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  ax.scatter(data['ball_obs1'][:,0], data['ball_obs1'][:,1], zs=data['ball_obs1'][:,2], c='r')
  ax.plot(data['kf_means2'][0:100,0],data['kf_means2'][0:100,1], zs=data['kf_means2'][0:100,2], c='b')
  ax.set_title('Ball observations and predicted trajectory in 3d')
  plt.show()
  times = map(lambda ix: data['ball_times1'][ix+1] - data['ball_times1'][ix], range(len(data['ball_times1'])-1))
  plt.hist(times)
  plt.title('Delta time histogram')
  plt.show()
  return 0

def plot_step4(data):
  kin = barret.BarretKinematics()
  traj, orient = kin.end_eff_trajectory(data['q'])
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  ax.scatter(data['ball_obs3'][:,0], data['ball_obs3'][:,1], zs=data['ball_obs3'][:,2], c='r')
  ax.plot(data['kf_means4'][0:100,0],data['kf_means4'][0:100,1], zs=data['kf_means4'][0:100,2], c='b')
  ax.plot(traj[:,0], traj[:,1], zs=traj[:,2], c='r')
  ax.set_title('Ball observations, predicted ball and robot trajectories in 3d')
  plt.show()
  times = map(lambda ix: data['ball_times3'][ix+1] - data['ball_times3'][ix], range(len(data['ball_times3'])-1))
  plt.hist(np.array(times), 30)
  plt.title('Delta time histogram')
  return 0

def plot_2d_dist(data):
  axis = ['X', 'Y', 'Z']
  ax1 = plt.subplot(5,1,1)
  btime = data['kf_times2']
  ptime = data['ball_times6']
  ball_offset = data['ball_offset']
  ball_obs = data['ball_obs6']
  #raw_ball_obs = data['raw_ball_obs6']
  #raw_ball_times = data['raw_ball_times6']
  is_hidden = data['hidden']
  ball_decision_time = data['ball_times3']
  ball_obs_decision = np.array(data['ball_obs3'])
  filter_means = data['kf_means4']
  deltaT = data['deltaT']
  filter_dev = np.array(map(lambda cov_mat: np.sqrt(np.diag(cov_mat)), data['kf_covs4']))
  prior_rmeans = data['rmeans']
  rmeans = data['cond_rmeans']
  rob_dev = np.array(map(lambda cov_mat: np.sqrt(np.diag(cov_mat)), data['cond_rcovs']))
  prior_rdev = np.array(map(lambda cov_mat: np.sqrt(np.diag(cov_mat)), data['rcovs']))
  rtime = deltaT*np.array(xrange(len(rmeans))) + data['t0']
  rmeans = np.array(rmeans)
  plt.ylabel('Prior overlaps')
  plt.plot(rtime,data['overlaps'],'r')
  lim_ix = min(data['ball_offset'] + len(rtime), len(btime))
  obs_ix = np.nonzero(is_hidden==False)[0]
  hid_ix = np.nonzero(is_hidden)[0]
  for d in xrange(3):
    #print ptime, data['kf_times'], rtime
    plt.subplot(5,1,d+2,sharex=ax1)
    #Real ball observations
    plt.plot(ptime,ball_obs[:,d],'bx')
    plt.plot(ball_decision_time[obs_ix],ball_obs_decision[obs_ix,d],'go')
    plt.plot(ball_decision_time[hid_ix],ball_obs_decision[hid_ix,d],'ro')
    #Ball prediction
    plt.plot(btime[0:lim_ix],filter_means[0:lim_ix,d],'b')
    plt.fill_between(btime[0:lim_ix], filter_means[0:lim_ix,d]-filter_dev[0:lim_ix,d], filter_means[0:lim_ix,d]+filter_dev[0:lim_ix,d], facecolor='blue', alpha=0.5)

    #Robot posterior distribution
    plt.fill_between(rtime, rmeans[:,d]-rob_dev[:,d], rmeans[:,d]+rob_dev[:,d], facecolor='green', alpha=0.5)
    plt.plot(rtime, rmeans[:,d], 'g')
    plt.ylabel(axis[d])
    #Robot prior distribution
    plt.plot(rtime, prior_rmeans[:,d], 'r')
    plt.fill_between(rtime, prior_rmeans[:,d]-prior_rdev[:,d], prior_rmeans[:,d]+prior_rdev[:,d], facecolor='red', alpha=0.3)
    plt.ylabel(axis[d])
  
  prior_dist_time, prior_dist_val = hit_utils.rob_ball_dist(btime, data['kf_means2'], data['rmeans'], data['t0'])
  post_dist_time, post_dist_val = hit_utils.rob_ball_dist(btime, data['kf_means2'], data['cond_rmeans'], data['t0'])
  print "Min dist between prior mean robot and ball: ", min(prior_dist_val)
  print "Min dist between posterior mean robot and ball: ", min(post_dist_val)
  print "t0: ", data['t0']

  plt.subplot(5,1,5,sharex=ax1)
  plt.plot(prior_dist_time, prior_dist_val, 'b')
  plt.plot(post_dist_time, post_dist_val, 'r')
  plt.ylabel('Ball - racket distance')
  plt.xlabel('Time')
  plt.show()

def plot_exec_traj(data, **params):
  params.setdefault('robot_freq', 500)
  axis = ['X', 'Y', 'Z']
  deltaT = 1.0 / params['robot_freq']
  q = data['q']
  qdot = data['qdot']
  qdotdot = data['qdotdot']
  time = data['t0'] + deltaT*np.array(range(np.shape(q)[0]))
  exec_q = data['exec_q']
  exec_qdot = data['exec_qdot']
  exec_qdotdot = data['exec_qdotdot']
  exec_time = data['exec_time']
  ax1 = None
  ax2 = None
  ax3 = None
  for i in xrange(7):
    if ax1 == None:
      ax1 = plt.subplot(7,3,3*i+1)
    else:
      plt.subplot(7,3,3*i+1,sharex=ax1)
    plt.plot(time, q[:,i], 'r')
    plt.plot(exec_time, exec_q[:,i], 'g')
    plt.ylabel('q_' + str(i+1))
    if ax2 == None:
      ax2 = plt.subplot(7,3,3*i+2)
    else:
      plt.subplot(7,3,3*i+2,sharex=ax2)
    plt.plot(time, qdot[:,i], 'r')
    plt.plot(exec_time, exec_qdot[:,i], 'g')
    if ax3 == None:
      ax3 = plt.subplot(7,3,3*i+3)
    else:
      plt.subplot(7,3,3*i+3,sharex=ax3)
    plt.plot(time, qdotdot[:,i], 'r')
    plt.plot(exec_time, exec_qdotdot[:,i], 'g')
  plt.xlabel('Time')
  plt.show()

def plot_final(data):
  plot_exec_traj(data)
  plot_2d_dist(data)
  kin = barret.BarretKinematics()
  planned_traj, planned_orient = kin.end_eff_trajectory(data['q'])
  real_traj, real_orient = kin.end_eff_trajectory(data['exec_q'])
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  ax.scatter(data['ball_obs6'][:,0], data['ball_obs6'][:,1], zs=data['ball_obs6'][:,2], c='b')
  ax.plot(data['kf_means4'][0:100,0],data['kf_means4'][0:100,1], zs=data['kf_means4'][0:100,2], c='b')
  ax.scatter(data['raw_ball_obs6'][:,0], data['raw_ball_obs6'][:,1], zs=data['raw_ball_obs6'][:,2], c='r')
  ax.plot(planned_traj[:,0], planned_traj[:,1], zs=planned_traj[:,2], c='r')
  ax.plot(real_traj[:,0], real_traj[:,1], real_traj[:,2], c='g')
  ax.set_title('Ball observations, predicted ball and robot trajectories in 3d')
  plt.show()
  return 0

def plot(argv):
  argc = len(argv)
  if argc < 4:
    print "Error, the filename to plot and plot type are required"
    return 1
  f = file(argv[2],'rb')
  data = np.load(f)
  plot_type = argv[3]
  if plot_type == 'step2':
    return plot_step2(data)
  elif plot_type == 'step4':
    return plot_step4(data)
  elif plot_type == 'final':
    return plot_final(data)
  else:
    print "Error: Plot type ", plot_type, " is not recognized"
    return 1

def main(argv):
  argc = len(argv)
  if argc == 1:
    print "Error: At least one command is required"
    printHelp(argv)
    return 1
  comm = argv[1]
  if comm=="h" or comm=="help":
    printHelp(argv)
    return 0
  elif comm=="run":
    return run_hit_ball(argv, host="wam")
  elif comm=="sim":
    return run_hit_ball(argv, host="localhost", sim_ball=True)
  elif comm=="plot":
    return plot(argv)
  else:
    print "Command ", comm, " is not recognized"
    return 1

if __name__ == "__main__":
  main(sys.argv)
