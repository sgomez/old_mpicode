
import barret
import time
import ball_data
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sys
import getopt
import time

def trajectory_splitter_callback(data, time, pos):
  ctime, cpos = ball_data.hypercube_filter_ball_data(data)
  time += ctime
  pos += cpos
  return len(ctime)>0 and len(cpos)>0

def trajectory_splitter(robot, rvision, call_back, **params):
  params.setdefault('window_size', 7)
  params.setdefault('capture_every', 0.1)
  params.setdefault('max_idle_counter', 3)
  pos_state = []
  time_state = []
  window = []
  sign = 0
  i = 0
  while i<params['num_trajectories']:
    trajectory_ended = False
    traj_time = []
    traj_pos = []
    while not trajectory_ended:
      counter = 0
      time_arr = []
      pos_arr = []
      mycallback = lambda x: trajectory_splitter_callback(x, time_arr, pos_arr)
      while not rvision.get_raw_ball_observations(mycallback):
        if len(pos_state) > 0:
          counter += 1
          if counter == params['max_idle_counter']:
            trajectory_ended = True
            break
        time.sleep(params['capture_every'])
      if trajectory_ended:
        traj_time = time_state
        traj_pos = pos_state
        time_state = []
        pos_state = []
        break
      for j in xrange(len(pos_arr)):
        if len(pos_state)>0:
          window.append(pos_arr[j][1] - pos_state[-1][1]) #only push y value
          if len(window) > params['window_size']:
            window.pop(0)
        pos_state.append(pos_arr[j])
        time_state.append(time_arr[j])
        if len(window) == params['window_size']:
          med = np.median(window)
          csign = np.sign(med)
          if sign != 0 and sign != csign:
            traj_time = time_state
            traj_pos = pos_state
            trajectory_ended = True
            time_state = []
            pos_state = []
            sign = 0
            window = []
          sign = csign
    if call_back(i, traj_time, traj_pos):
      i+=1

def get_filename(dirname, index):
  if dirname[-1] != '/':
    filename = dirname + '/'
  else:
    filename = dirname
  filename += 'traj' + str(index) + '.npz'
  return filename

def save_trajectory(dirname, index, time, pos, observed):
  filename = get_filename(dirname, index)
  f = file(filename, 'wb')
  np.savez(f, time=time, pos=pos, observed = observed)
  f.close()
  print "Saved file ", filename
  return True

def filter_save_trajectory(dirname, index, time, pos):
  print "Candidate: N=", len(pos)
  if len(pos)<30:
    return False
  X = np.array(pos)
  rx = ball_data.line_ransac(time, X[:,0], num_iter=30, tol=0.07)
  ry = ball_data.line_ransac(time, X[:,1], num_iter=30, tol=0.12)
  print "rx=",rx['best_value'],", ry=",ry['best_value']
  N = len(pos)
  if rx['best_value'] > 0.8*N and ry['best_value'] > 0.8*N:
    observed = map(lambda ix: rx['inside'][ix] and ry['inside'][ix], xrange(N))
    return save_trajectory(dirname, index, time, pos, observed)
  else:
    return False

def plot_trajectory(time, pos, observed):
  fig = plt.figure()
  obs_ix = np.nonzero(observed==True)
  hid_ix = np.nonzero(observed==False)
  ax = fig.add_subplot(111, projection='3d')
  #ax.plot(pos[obs_ix,0],pos[obs_ix,1],zs=pos[obs_ix,2])
  ax.scatter(pos[obs_ix,0],pos[obs_ix,1],zs=pos[obs_ix,2],c='b')
  ax.scatter(pos[hid_ix,0],pos[hid_ix,1],zs=pos[hid_ix,2],c='r')
  ax.set_xlabel("X")
  ax.set_ylabel("Y")
  ax.set_zlabel("Z")
  #for d in xrange(3):
  #  ax = fig.add_subplot(1,d,1)
  #  ax.plot(time, pos[:,d])
  plt.show()

def plot_many(dirname, from_ix, num_traj):
  for i in xrange(num_traj):
    f = file(get_filename(dirname, i+from_ix), 'rb')
    data = np.load(f)
    time = data['time']
    pos = data['pos']
    observed = data['observed']
    plot_trajectory(time, pos, observed)
    f.close()

def capture_ball_data(**params):
  params.setdefault('num_trajectories', 10)
  params.setdefault('plot_ball_3d', True)
  params.setdefault('base_index', 1)
  params.setdefault('dir','./')
  robot = barret.Robot(params['host'], params['port'])
  rvision = ball_data.BallVision(robot)
  trajectory_splitter(robot, rvision, lambda x,y,z: filter_save_trajectory(params['dir'],\
      x+params['base_index'],y,z), **params)
  if params['plot_ball_3d']:
    plot_many(params['dir'], params['base_index'], params['num_trajectories'])

capture_ball_data(host='10.42.31.249', port='7646', num_trajectories=30, dir='./saved_traj',
    base_index=20)
