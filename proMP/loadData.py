
import numpy as np
import matplotlib.pyplot as plt
import ball_hit_utils as my_utils
import os

def loadOkanData():
  dropSet = set([17,20,22,23,24,29,56]) #some datasamples are no good
  real_set = [i for i in range(15,66) if not i in dropSet]
  file_path = "./okan_data/unifyData"
  data = []
  dof = 7 #There are 7 degrees of freedom in this robotic arm
  for i in real_set:
      mat = np.loadtxt(file_path + str(i) + ".txt")
      robot = mat[:,-14:]
      q = robot[:,0:dof] #angular positions
      qdot = robot[:,dof:] #angular velocities
      data.append({'file_num': i, 'q': q, 'qdot': qdot, 'raw': mat})
  return data

def respects_joint_limits(data, eps, **params):
  params.setdefault('verbose', True)
  min_angle = [-2.6, -2.0, -2.8, -0.9, -4.8, -1.6, -2.2]
  max_angle = [2.6, 2.0, 2.8, 3.1, 1.3, 1.6, 2.2]
  for i in xrange(7):
    if np.max(data['q'][:,i]) + eps > max_angle[i]:
      if params['verbose']: print "Max limit violated in joint ", i+1, " by ", np.max(data['q'][:,i])-max_angle[i]
      return False
    if np.min(data['q'][:,i]) - eps < min_angle[i]:
      if params['verbose']: print "Min limit violated in joint ", i+1, " (got: ", np.min(data['q'][:,i]), ", lim: ", min_angle[i], ")"
      return False
  return True

def loadSebasData(**params):
  params.setdefault('folder', './saved_traj/')
  params.setdefault('verbose', False)
  params.setdefault('remove_bad_files', False)
  ans = []
  for fname in os.listdir(params['folder']):
    if not fname.endswith('.npz'): continue
    f = file(params['folder'] + fname, 'rb')
    data = np.load(f)
    if not respects_joint_limits(data, 0.2, **params):
      if params['remove_bad_files']:
        print "Removing file ", fname, " because violates joint limits"
        os.remove(params['folder'] + fname)
      else:
        print "Skipping file ", fname, " because violates joint limits"
      continue
    data = my_utils.reverse_raw_robot_data(data)
    splitted = my_utils.zero_cross_vel_split(data['time'], data['q'], data['qdot'])
    splitted['fname'] = fname
    ans.append(splitted)
  print "Training data from ", len(ans), " files"
  return ans

def plot_trajectory_joints(q, qdot):
  tao, ndof = np.shape(q)
  plt.figure(1)
  for i in xrange(ndof):
    plt.subplot(ndof,2,2*i + 1)
    plt.plot(q[:,i])
    plt.subplot(ndof,2,2*i + 2)
    plt.plot(qdot[:,i])
  plt.show()

def segment_okan_data(okan_data):
  segm_file = open('./okan_data/split_points.txt')
  segm_data = {}
  for line in segm_file:
    s = line.split()
    segm_data[int(s[0])] = map(int,s[1:])
  strike_data = []
  back_data = []
  for instance in okan_data:
    i = instance['file_num']
    #print "processing instance ", i
    if (i in segm_data):      
      segm_ids = segm_data[i]
      #print segm_ids
      strike_inst = {'file_num': i, 'q': instance['q'][segm_ids[0]:segm_ids[1],:],
          'qdot': instance['qdot'][segm_ids[0]:segm_ids[1],:]}
      back_inst = {'file_num': i, 'q': instance['q'][segm_ids[1]:segm_ids[2],:],
          'qdot': instance['qdot'][segm_ids[1]:segm_ids[2],:]}
      strike_data.append(strike_inst)
      back_data.append(back_inst)
    #print i, " processed successfully"
  return strike_data, back_data
