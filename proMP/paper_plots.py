
import numpy as np
import random as rnd
import loadData as myld
from proMP import *
import barret
import time as time_module
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
from matplotlib2tikz import save as tikz_save

import ball_data
import ball_hit_utils as hit_utils
import sys
import sketch_utils as sketch

def plot_joint_dist(robot_promp, **params):
  params.setdefault('traj_instances', [])
  params.setdefault('tex_file', 'out.tex')
  plt.figure(1)
  ndof = len(params['joints'])
  ncols = params['num_cols']
  means = []
  stds = []
  prior_means = []
  prior_stds = []
  mean_times = np.linspace(0,1,500)
  for t in mean_times:
    m, S = hit_utils.proMP_function(robot_promp, t, **params)
    means.append(m)
    stds.append(np.sqrt(np.diag(S)))
    if 'prior_proMP' in params:
      m, S = hit_utils.proMP_function(params['prior_proMP'], t, **params)
      prior_means.append(m)
      prior_stds.append(np.sqrt(np.diag(S)))
  means= np.array(means)
  stds = np.array(stds)
  prior_means = np.array(prior_means)
  prior_stds = np.array(prior_stds)
  data = params['traj_instances']
  for i in xrange(len(params['joints'])):
    cjoint = params['joints'][i]
    for j in xrange(len(data)):
      q = data[j]['q']
      qdot = data[j]['qdot']
      time = data[j]['time']
      T = time[-1] - time[0]
      if T==0:
        print 'Error in instance ', j
        continue
      t = (time - time[0]) / T
#      tfac = 1 / T
#      qdot *= (1 / tfac)
      plt.subplot(ndof,ncols,ncols*i + 1)
      plt.plot(t,q[:,cjoint], 'g', lw=1.0, alpha=0.5)
      plt.ylabel('q_' + str(cjoint))
#      plt.subplot(ndof,2,2*i + 2)
#      plt.plot(t,qdot[:,cjoint], 'b', lw=1.0)
    plt.subplot(ndof,ncols,ncols*i + 1)
    plt.plot(mean_times, means[:,cjoint], 'r', lw=4.0)
#    plt.fill_between(mean_times, means[:,cjoint]-stds[:,cjoint], means[:,cjoint]+stds[:,cjoint], facecolor='blue', alpha=0.3) 
    plt.plot(mean_times, means[:,cjoint]-stds[:,cjoint], 'r--', lw=2)
    plt.plot(mean_times, means[:,cjoint]+stds[:,cjoint], 'r--', lw=2)
    if 'mu_th' in params:
      plt.errorbar(params['rob_time'], params['mu_th'][cjoint], yerr=np.sqrt(params['Sigma_th'][cjoint,cjoint]), fmt='o', ecolor='blue')
    if 'prior_proMP' in params:
      plt.plot(mean_times, prior_means[:,cjoint], 'g', lw=1)
  #    plt.fill_between(mean_times, means[:,cjoint]-stds[:,cjoint], means[:,cjoint]+stds[:,cjoint], facecolor='blue', alpha=0.3) 
      plt.plot(mean_times, prior_means[:,cjoint]-prior_stds[:,cjoint], 'g--', lw=1)
      plt.plot(mean_times, prior_means[:,cjoint]+prior_stds[:,cjoint], 'g--', lw=1)
    plt.ylabel('$q_' + str(cjoint) + '$ [rad]')
#    plt.subplot(ndof,ncols,ncols*i + 2)
#    plt.plot(np.linspace(0,1,500), qdot[:,cjoint], 'r', lw=3.0)
#    plt.ylabel('\\dot{q}_' + str(cjoint))

  plt.xlabel('Time [s]')
  tikz_save(params['tex_file'], figureheight = '\\figureheight', figurewidth = '\\figurewidth')
  plt.show()


def plot_joint_promp(**params):
  params.setdefault('joints', [0,1,2])
  params.setdefault('robot_freq', 500)
  params.setdefault('num_cols', 1)
  params.setdefault('promp_type', 'full')
  params.setdefault('kernel_type', 'sqexp')
  params.setdefault('model_fname', 'saved_models/' + params['promp_type'] + '_' + params['kernel_type'] + '_promp.npz')
  params.setdefault('plot_instances', [5,10,15,20,25,9,11])
  params.setdefault('tex_file', 'joint_promp_prior.tex')
  if os.path.isfile(params['model_fname']):
    print "Loading previous trained model..."
    kbuild = KernelBuilder.get_stream_builder(params['kernel_type'])
    stream = load_stream(params['model_fname'])
    if params['promp_type'] == 'full':
      robot_promp = proMP_Dependent_Robot_Angles.build_from_stream(stream, \
          kbuild, proMP.build_from_stream)
    else:
      robot_promp = proMP_Indep_Robot_Angles.build_from_stream(stream, \
          kbuild, proMP.build_from_stream)
  else:
    print "Error: File ", params['model_fname'], " not found"
    return -1
  
  data = myld.loadSebasData(**params)
  data_inst = map(lambda ix : data[ix] , params['plot_instances'])
  print "Plotting joint prior"
  plot_joint_dist(robot_promp, traj_instances=data_inst, **params)
  return robot_promp


def plot_joint_posterior(prior_promp, **params):
  params.setdefault('tex_file', 'joint_promp_posterior.tex')
  data = params['step_data']
  mu_th = data['mu_th']
  Sigma_th = data['Sigma_th']
  cond_promp = prior_promp.condition(data['rob_time'], pos_t=mu_th, Sigma_pos=Sigma_th)
  print "Plotting joint posterior"
  plot_joint_dist(cond_promp, mu_th=mu_th, Sigma_th=Sigma_th, rob_time=data['rob_time'], prior_proMP=prior_promp, **params)
  return cond_promp


def joint_space_plots(**params):
  prior = plot_joint_promp(**params)
  cond_promp = plot_joint_posterior(prior, **params)
  return cond_promp

def sketch3D_traj_dist(task_mean, task_cov, **params):
  params.setdefault('main_axis', 1)
  params.setdefault('min_y_val', -1.9)
  params.setdefault('max_y_val', -0.3)
  params.setdefault('scale', 10)
  f_task_mean = []
  f_task_cov = []
  for i in xrange(len(task_mean)):
    if task_mean[i][1] > params['min_y_val'] and task_mean[i][1] < params['max_y_val']:
      f_task_mean.append(task_mean[i])
      f_task_cov.append(task_cov[i])
  trans_polys = sketch.traj_dist_polys(f_task_mean, f_task_cov, **params)
  surface = sketch.poly_surface(trans_polys, **params)
  sketch.surf_sketch(surface, **params)

def task_space_plots(step_data, **params):
  params.setdefault('tex_file', 'task_space.tex')
  params.setdefault('stdev_mult', 2.0)
  params.setdefault('plot_obs', True)
  params.setdefault('dims', [0,1,2])
  #ax1 = plt.subplot(3,1,1)
  kinematics = barret.BarretKinematics()
  overlap = Robot_Ball_Overlap(kinematics)
  axis = ['X', 'Y', 'Z']
  ball_time = step_data['ball_times']
  ball_means = np.array(step_data['ball_means'])
  ball_covs = step_data['ball_covs']
  sketch3D_traj_dist(ball_means, ball_covs, filename='ball_dist_cov.sk', fillcolor='blue', \
      opacity=0.3, main_axis=1, def_name='ball_dist_cov')
  ball_devs = np.array(map(lambda x: np.sqrt(np.diag(x)), ball_covs))
  prior_rmeans = np.array(step_data['prior_rmeans'])
  prior_rcovs = step_data['prior_rcovs']
  prior_rdevs = np.array(map(lambda x: np.sqrt(np.diag(x)), prior_rcovs))
  t0 = step_data['t0']
  deltaT = 1.0/60.0
  rtime = t0 + deltaT*np.array(range(len(step_data['prior_rmeans'])))
  opt_T = 0.5 #rtime[-1] - rtime[0]
  ncols = 1
  nrows = len(params['dims'])
  std_mult = params['stdev_mult']
  if 'cond_promp' in params:
    proMP_fun = lambda t: hit_utils.proMP_function(params['cond_promp'], t, **params)
    post_rmeans, post_rcovs, post_rorient, ball_offset = overlap.compute_robot_trajectory(t0, opt_T, ball_time, proMP_fun)
    post_rmeans = np.array(post_rmeans)
    post_rdevs = np.array(map(lambda x: np.sqrt(np.diag(x)), post_rcovs))
    print prior_rcovs[0]
    print prior_rdevs[0]
    sketch3D_traj_dist(prior_rmeans, prior_rcovs, filename='prior_dist_cov.sk', fillcolor='gray', \
        opacity=0.3, main_axis=1, def_name='prior_dist_cov')
    sketch3D_traj_dist(post_rmeans, post_rcovs, filename='post_dist_cov.sk', fillcolor='red', \
        opacity=0.3, main_axis=1, def_name='post_dist_cov')
    #ncols = 2
    print np.shape(prior_rmeans), np.shape(post_rmeans)
  for r in xrange(nrows):
    d = params['dims'][r]
    ay = None
    for c in xrange(ncols):
      if ay == None:
        ay = plt.subplot(nrows,ncols,ncols*r+c+1)
      else:
        plt.subplot(nrows,ncols,ncols*r+c+1,sharey=ay)
      if params['plot_obs']: 
        plt.plot(step_data['obs_t'], step_data['obs_x'][:,d], 'ko', ms=8)
        plt.plot(step_data['fut_t'], step_data['fut_x'][:,d], 'yo', ms=8)
      plt.plot(ball_time,ball_means[:,d],'b',lw=2)
      plt.plot(ball_time, ball_means[:,d]-std_mult*ball_devs[:,d],'b--',lw=1) 
      plt.plot(ball_time, ball_means[:,d]+std_mult*ball_devs[:,d], 'b--',lw=1)
      plt.axvline(step_data['rob_time']*opt_T + t0, color='k', lw=2)
      if c == 0:
        plt.plot(rtime, prior_rmeans[:,d], 'r', lw=2.0)
        plt.plot(rtime, prior_rmeans[:,d]+std_mult*prior_rdevs[:,d], 'r--', lw=1.0)
        plt.plot(rtime, prior_rmeans[:,d]-std_mult*prior_rdevs[:,d], 'r--', lw=1.0)
        if 'cond_promp' in params:
          plt.plot(rtime, post_rmeans[:,d], 'g', lw=2)
          plt.plot(rtime, post_rmeans[:,d]+std_mult*post_rdevs[:,d], 'g--', lw=1)
          plt.plot(rtime, post_rmeans[:,d]-std_mult*post_rdevs[:,d], 'g--', lw=1)
      if c==1:
        plt.plot(rtime, post_rmeans[:,d], 'g', lw=2)
        plt.plot(rtime, post_rmeans[:,d]+std_mult*post_rdevs[:,d], 'g--', lw=1)
        plt.plot(rtime, post_rmeans[:,d]-std_mult*post_rdevs[:,d], 'g--', lw=1)
      if d == 2:
        plt.xlabel('Time')
      if c == 0:
        plt.ylabel(axis[d])
  tikz_save(params['tex_file'], figureheight = '\\figureheight', figurewidth = '\\figurewidth')
  plt.show()

def plot_sim_results(**params):
  params.setdefault('sim_fname', 'tmp_files/iros_sim_1.npz')
  params.setdefault('tex_file', 'sim_hist.tex')
  sim_file = file(params['sim_fname'], 'rb')
  sim_data = np.load(sim_file)
  prior = np.array(sim_data['prior_dist'])
  post = np.array(sim_data['post_dist'])
  #no_far_away = np.nonzero(prior < 0.5)[0]
  #prior = prior[no_far_away]
  #post = post[no_far_away]
  bins = np.linspace(0, 1, 100)
  plt.hist(prior,bins,alpha=0.3,label='prior')
  plt.hist(post,bins,alpha=0.5,label='conditioned')
  plt.legend(loc='upper right')
  plt.xlabel('Distance [m]')
  tikz_save(params['tex_file'], figureheight = '\\figureheight', figurewidth = '\\figurewidth')
  plt.show()
  prior_prob = len(np.nonzero(prior<0.08)[0]) / float(len(prior))
  post_prob = len(np.nonzero(post<0.08)[0]) / float(len(post))
  print "Probability of hitting: Prior=", prior_prob, " Post=", post_prob
  print "Dist Racket Ball: Prior=", np.mean(prior), " \pm ", np.std(prior), " Post=", np.mean(post), " \pm ", np.std(post)

def do_plots(**params):
  params.setdefault('promp_type', 'full')
  params.setdefault('kernel_type', 'sqexp')
  params.setdefault('model_fname', 'saved_models/' + params['promp_type'] + '_' + params['kernel_type'] + '_promp.npz')
  params.setdefault('joints', [0])
  params.setdefault('robot_freq', 500)
  params.setdefault('num_cols', 1)
  params.setdefault('step_fname', 'tmp_files/decision_kalman_step.npz')

  step_file = file(params['step_fname'], 'rb')
  step_data = np.load(step_file)

  cond_promp = joint_space_plots(step_data=step_data, **params)
  task_space_plots(step_data, cond_promp=cond_promp, dims=[2], **params)
  plot_sim_results()


do_plots()
