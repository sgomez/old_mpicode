import sys
sys.path.append('../kalman')

import numpy as np
import kalman as kalman
import matplotlib.pyplot as plt
import ball_model
import random
import os
import proMP
import barret
import time
import ball_hit_utils as hit_utils
import ball_data


def promp_opt_hit_manifold(ball_means, ball_covs, ball_times, promp_robot, overlap, kinematics, inv_kin, **params):
  params.setdefault('top_k_cond', 1)
  opt_T = 0.5 #Do proMP strike in this time (seconds)
  proMP_fun = lambda t: hit_utils.proMP_function(promp_robot, t, **params)
  start_times, start_values = overlap.start_time_convolution(opt_T,ball_means,ball_covs,ball_times,proMP_fun)
  t0 = start_times[np.argmax(start_values)]
  rmeans, rcovs, rorient, ball_offset = overlap.compute_robot_trajectory(t0, opt_T, ball_times, proMP_fun) #get robot end effector dist
  overlaps = overlap.trajectory_overlap(ball_means, ball_covs, rmeans, rcovs, ball_offset)

  #Now the conditioning part
  cond_ixs = hit_utils.top_k_hitting_points(overlaps, params['top_k_cond'])
  cond_promp = promp_robot
  to_save = {'ball_means': ball_means, 'ball_covs': ball_covs, 'ball_times': ball_times, 'overlaps': overlaps, \
      'start_times': start_times, 'start_values': start_values, 't0': t0, 'prior_rmeans': rmeans, \
      'prior_rcovs': rcovs, 'prior_rorient': rorient, 'ball_offset': ball_offset, 'cond_ixs': cond_ixs}

  for rob_ix in cond_ixs:
    cond_ix = rob_ix + ball_offset
    hit_time = ball_times[cond_ix]
    rob_time = (hit_time - t0) / opt_T
    mu_th, Sig_th = proMP_fun(rob_time)
    mu_th2, Sig_th2 = inv_kin.inv_kin(mu_th, Sig_th, ball_means[cond_ix], ball_covs[cond_ix])
    mu_x1, jac_x1, ori_x1 = kinematics.position_and_jac(mu_th)
    mu_x2, jac_x2, ori_x2 = kinematics.position_and_jac(mu_th2)
    to_save.update({'mu_th': mu_th2, 'Sigma_th': Sig_th2, 'rob_time': rob_time})
    cond_promp = cond_promp.condition(rob_time, pos_t=mu_th2, Sigma_pos=Sig_th2) #Condition on the posterior given by inv_kin
    proMP_fun = lambda t: hit_utils.proMP_function(cond_promp, t, **params)
  #fout = file('test_kalman_step.npz', 'wb')
  #np.savez(fout, **to_save)
  #fout.close()
  return t0, opt_T, start_times, start_values, overlaps, cond_promp, proMP_fun, to_save

def index_virtual_hit_plane(obs, **params):
  params.setdefault('y_value', 0) #default time -> the net
  i = 0
  while i < len(obs) and obs[i][1] < params['y_value']: i += 1
  return i

def list_find_first(l, pred):
  for i in xrange(len(l)):
    if pred(l[i]): return i
  return -1

def min_traj_dist(t1, x1, t2, x2, **params):
  params.setdefault('epsilon', 0.005)
  i = 0
  j = 0
  ans = 1e100 #start with infinity
  while i<len(t1) and j<len(t2):
    if abs(t1[i] - t2[j]) < params['epsilon']: # and ans>np.linalg.norm(x1[i] - x2[j]):
      #print t1[i], x1[i], t2[j], x2[j], x1[i]-x2[j], np.linalg.norm(x1[i] - x2[j])
      ans = min(ans, np.linalg.norm(x1[i] - x2[j]))
    if t1[i] < t2[j]:
      i += 1
    else:
      j += 1
  return ans

def apply_prob_oper(promp, kf, overlap, kinematics, inv_kin, ball_time, ball_obs, actions, **params):
  means,covs,Pvals,As,Bs,Cs,Ds = kf.forward_recursion(ball_obs,actions)
  ball_means = map(lambda mean: mean[range(0,6,2)], means)
  ball_covs = map(lambda covs: covs[range(0,6,2),:][:,range(0,6,2)], covs)
  t0, opt_T, start_times, start_values, overlaps1, cond_promp, new_promp_fun, to_save = promp_opt_hit_manifold(ball_means,\
      ball_covs, ball_time, promp, overlap, kinematics, inv_kin, **params)
  return t0, cond_promp, to_save

def simulate_ball_traj(**params):
  params.setdefault('promp_type', 'full')
  params.setdefault('kernel_type', 'sqexp')
  params.setdefault('model_fname', '../proMP/saved_models/' + params['promp_type'] + '_' + params['kernel_type'] + '_promp.npz')
  params.setdefault('kfilter_fname', 'saved_models/full_ball_em.npz')
  #params.setdefault('kfilter_fname', 'saved_models/old_trained_kf.npz')
  params.setdefault('a_mat', 'linear')
  params.setdefault('zero_init_vel', False)
  params.setdefault('n_experiments', 1)
  params.setdefault('traj_duration', 0.5)
  params.setdefault('ball_duration', 1.5)
  params.setdefault('robot_freq', 500)
  params.setdefault('stop_y_val', -0.76 - (2.74/2))
  kf, deltaT = ball_model.load_ball_model(params['kfilter_fname'], **params)
  kinematics = barret.BarretKinematics()
  inv_kin = proMP.ProbInvKinematics(kinematics)
  overlap = proMP.Robot_Ball_Overlap(kinematics)
  promp_stream = proMP.load_stream(params['model_fname'])
  kernel_builder = proMP.KernelBuilder.get_stream_builder(params['kernel_type'])
  if params['promp_type'] == 'full':
    promp_robot = proMP.proMP_Dependent_Robot_Angles.build_from_stream(promp_stream, kernel_builder, proMP.proMP.build_from_stream)
  else:
    promp_robot = proMP.proMP_Indep_Robot_Angles.build_from_stream(promp_stream, kernel_builder, proMP.proMP.build_from_stream)
  zero_init_vel_promp = promp_robot.condition(0, vel_t=np.zeros(7), Sigma_vel=0.01*np.eye(7))
  if params['zero_init_vel']:
    promp_robot = zero_init_vel_promp
  promp_fun = lambda t: hit_utils.proMP_function(promp_robot, t, **params)
  N = params['n_experiments']
  num_balls = int(params['ball_duration']/ deltaT)
  rob_steps = int(params['traj_duration'] * params['robot_freq'])
  prior_dist = []
  post_dist = []
  for n in xrange(N):
    ball_time = deltaT*np.array(range(num_balls))
    actions = np.ones((num_balls,1))
    ball_pos, noiseless_obs = kf.generate_sample(num_balls, actions=actions, **params)
    stop_ix = index_virtual_hit_plane(ball_pos, y_value = params['stop_y_val'])
    t0, cond_promp, to_save = apply_prob_oper(promp_robot, kf, overlap, kinematics, inv_kin, ball_time, ball_pos[0:stop_ix], actions, **params)
    #stop_ix = list_find_first(ball_time, lambda x: x > t0) - 1
    stop_ix = to_save['cond_ixs'][0] + to_save['ball_offset'] - 1
    t0, cond_promp,_ = apply_prob_oper(promp_robot, kf, overlap, kinematics, inv_kin, ball_time, ball_pos[0:stop_ix], actions, **params)
    q_prior, qd_prior, qdd_prior = promp_robot.sample_trajectory(rob_steps)
    q_post, qd_post, qdd_post = cond_promp.sample_trajectory(rob_steps)
    x_prior, ori = kinematics.end_eff_trajectory(q_prior)
    x_post, ori = kinematics.end_eff_trajectory(q_post)
    rob_time = t0 + (1.0/params['robot_freq']) * np.array(range(rob_steps))
    prior_dist.append( min_traj_dist(ball_time, noiseless_obs, rob_time, x_prior) )
    post_dist.append( min_traj_dist(ball_time, noiseless_obs, rob_time, x_post) )
  f_sim = file('tmp_files/iros_sim_3.npz', 'wb')
  to_save = {'prior_dist': prior_dist, 'post_dist': post_dist} 
  np.savez(f_sim, **to_save)
  return prior_dist, post_dist

simulate_ball_traj(n_experiments=10000)

