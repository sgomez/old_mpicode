
import numpy as np
import time

def soft_cam_mix(red_cam_pos, blue_cam_pos):
  start_y_blue = -2.5
  end_y_red = -1.1
  curr_y = red_cam_pos[1]
  alpha = (curr_y-start_y_blue) / (end_y_red - start_y_blue)
  if alpha<0.0 or alpha>1.0:
    return red_cam_pos
  else:
    return alpha*red_cam_pos + (1-alpha)*blue_cam_pos

def hypercube_filter_ball_data(data, **params):
  params.setdefault('offset', 0.05) #Add 5 cm offset
  params.setdefault('max_ball_height', 1.0) #maximum ball height above table
  offset = params['offset'] 
  tableWidth = 1.526
  tableLenght = 2.74
  tableHeight = 0.76
  maxBallH = params['max_ball_height']
  floor = -1.75
  Xrange = [-0.5*tableWidth - offset,0.5*tableWidth + offset]
  Yrange = [-3.5 - offset, offset]
  Zrange = [floor + tableHeight - offset, floor + tableHeight + maxBallH + offset]
  ranges = [Xrange, Yrange, Zrange]
  time = []
  pos = []
  for instance in data:
    inside = True
    if 'only_cam' in params:
      if instance['cam_id'] != params['only_cam']: continue
    for i in xrange(3):
      if instance['pos'][i] < ranges[i][0] or instance['pos'][i] > ranges[i][1]:
        inside = False
    if inside:
      cpos = np.array(instance['pos'])
      if len(time)==0 or time[-1]!=instance['time']:
        time.append(instance['time'])
        pos.append(cpos)
      else:
        pos[-1] = (pos[-1] + cpos) / 2.0
        #if instance['cam_id']==3: pos[-1] = soft_cam_mix(cpos, pos[-1])
        #else: pos[-1] = soft_cam_mix(pos[-1], cpos)
  return time, pos

def ensure_deltaT(inst, **params):
  params.setdefault('deltaT', 0.002)
  params.setdefault('tol', 1e-3)
  time = []
  X = []
  obs = []
  obs_set = []
  for i in xrange(len(inst['time'])):
    append_i = False
    if len(time)==0:
      append_i = True
    else:
      dt = inst['time'][i]-time[-1]
      if abs(dt-params['deltaT']) < params['tol']:
        append_i = True
      elif dt > params['deltaT']:
        ntimes = int(round(dt / params['deltaT']))
        ndt = dt / ntimes
        if abs(ndt-params['deltaT']) < params['tol']:
          for tmp in xrange(ntimes-1):
            time.append(time[-1] + ndt)
            X.append(X[-1])
            obs.append(False)
          append_i = True
    if append_i:
      time.append(inst['time'][i])
      X.append(inst['X'][i,:])
      obs.append(inst['observed'][i])
      if obs[-1]: obs_set.append(len(X)-1)
  return {'time': time, 'X': np.array(X), 'observed': obs, 'obs_set': obs_set}

def line_ransac_comp_model(x, y, ixs):
  Xmat = []
  yvec = []
  for i in ixs:
    Xmat.append([1,x[i]])
    yvec.append([y[i]])
  w = np.dot(np.linalg.pinv(Xmat), yvec)
  return w

def line_ransac_inside(x, y, w, tol):
  N = len(x)
  curr_vote = []
  inside = []
  for i in xrange(N):
    y_i = w[0] + w[1]*x[i]
    if abs(y_i-y[i]) < tol:
      curr_vote.append(i)
      inside.append(True)
    else:
      inside.append(False)
  return curr_vote, inside

def line_ransac(x, y, **params):
  params.setdefault('group_size', 4)
  params.setdefault('num_iter', 100)
  params.setdefault('tol', 0.05)
  ans = {'best_value': 0}
  N = len(x)
  for it in xrange(params['num_iter']):
    ixs = np.random.choice(N, params['group_size'], replace=False)
    w = line_ransac_comp_model(x, y, ixs)
    curr_vote, inside = line_ransac_inside(x, y, w, params['tol'])
    if len(curr_vote) > ans['best_value']:
      ans['best_value'] = len(curr_vote)
      ans['support_pts'] = curr_vote
      ans['model'] = w
      ans['inside'] = inside
  #Recompute the model with the support points
  w = line_ransac_comp_model(x, y, ans['support_pts'])
  curr_vote, inside = line_ransac_inside(x, y, w, params['tol'])
  ans['model'] = w
  ans['best_value'] = len(curr_vote)
  ans['support_pts'] = curr_vote
  ans['inside'] = inside
  return ans

#Idea: Separate by center of the table for instance (That is y=-2.13) between cam1 and cam3
#and take cam3 until end of table (y=-0.8) of time more that it should.
def split_merge_raw_data(raw_ball_data, **params):
  params.setdefault('y_thresh0', -2.13) #y threshold between cam3 and cam1
  params.setdefault('y_thresh1', -0.8) #y threshold between cam1 and end
  params.setdefault('max_time', 0.7)
  ans = []
  for instance in raw_ball_data:
    raw_obs = instance['obs']
    proc_obs = []
    state = 0
    for r_obs in raw_obs:
      if len(proc_obs)>0 and abs(proc_obs[-1]['time'] - r_obs['time']) < 1e-6: continue
      if len(proc_obs)>0 and (r_obs['time'] - proc_obs[0]['time']) > params['max_time']: state=2
      if r_obs['cam_id']==3 and state==0:
        proc_obs.append({'time': r_obs['time'], 'pos': r_obs['pos']})
        if r_obs['pos'][1] > params['y_thresh0']:
          state = 1
      elif r_obs['cam_id']==1 and state==1:
        proc_obs.append({'time': r_obs['time'], 'pos': r_obs['pos']})
        if r_obs['pos'][1] > params['y_thresh1']:
          state = 2
    ans.append(proc_obs)
  return ans

#Use ransac on (x,y) to mark outliers on merged data
def ransac_mark_outliers(merged_data, **params):
  params.setdefault('ransac_group_size', 10)
  params.setdefault('ransac_tol', 0.10)
  x = []
  y = []
  z = []
  time = []
  for inst in merged_data:
    x.append(inst['pos'][0])
    y.append(inst['pos'][1])
    z.append(inst['pos'][2])
    time.append(inst['time'])
  ransac_result = line_ransac(x, y, group_size=params['ransac_group_size'], tol=params['ransac_tol'], **params)
  X = np.array([x,y,z]).T
  return {'time': time, 'X': X, 'observed': ransac_result['inside'], 'obs_set': ransac_result['support_pts']}

class BallFilter:
  #ball_time: Filtered ball observation times
  #ball_obs: Filtered ball observation positions
  #raw_ball_obs: Observations as recevied from cameras
  #raw_ball_time: Raw ball observation time
  #kf: The kalman filter object
  #kf_state: The kalman filter forward recursion state
  #state: Integer value that represent a state DFA
  #params: A dictionary of params

  def reset(self):
    self.ball_obs = []
    self.ball_time = []
    self.raw_ball_obs = []
    self.raw_ball_time = []
    self.is_hidden = []
    self.state = 0

  def __ransac_dir(self, rs):
    X = np.array(self.ball_obs)
    p0 = X[rs['support_pts'][0], :]
    p1 = X[rs['support_pts'][-1], :]
    v = p1 - p0
    return v

  def __good_ransac_dir(self, rs):
    if not self.params['force_ransac_dir']: return True
    if len(rs['support_pts']) < 2: return False
    v = self.__ransac_dir(rs)
    return v[1] > 0.0 and v[0] <= 0.0

  def __init__(self, kfilter, **params):
    params.setdefault('min_init_total_obs', 16)
    params.setdefault('min_init_obs', 10)
    params.setdefault('min_init_support', 6)
    params.setdefault('max_mah_dist', 4.0)
    params.setdefault('force_ransac_dir', True)
    params.setdefault('deltaT_tol', 2e-3)
    self.params = params
    self.kf = kfilter
    self.reset()

  def __call__(self, data):
    if self.state != 0:
      ctime, cpos = hypercube_filter_ball_data(data)
    else:
      ctime, cpos = hypercube_filter_ball_data(data, only_cam=3)
    #pdata = split_merge_raw_data(data, y_thresh1=0.0)
    #pdata = ransac_mark_outliers(pdata)
    nval = len(ctime)
    if nval == 0: return False
    if self.state == 0:
      self.raw_ball_obs += cpos
      self.raw_ball_time += ctime
      self.ball_obs += cpos
      self.ball_time += ctime
      N = len(self.ball_obs)
      if N < self.params['min_init_total_obs']: return False
      if 'deltaT' in self.params:
        tmp = ensure_deltaT({'time': self.ball_time, 'X': np.array(self.ball_obs), \
            'observed': [True for i in xrange(len(self.ball_time))]}, \
            deltaT=self.params['deltaT'], tol=self.params['deltaT_tol'])
        self.ball_time = tmp['time']
        self.ball_obs = tmp['X'].tolist()
        if len(self.ball_obs) < self.params['min_init_obs']: return False
      X = np.array(self.ball_obs)
      rs = line_ransac(X[:,0], X[:,1], num_iter=30, tol=0.08)
      if rs['best_value'] < self.params['min_init_support'] or not self.__good_ransac_dir(rs):
        self.raw_ball_obs = self.ball_obs = []
        self.raw_ball_time = self.ball_time = []
        return False
      self.is_hidden = map(lambda x: not x, rs['inside'])
      #self.ball_obs = map(lambda ix: self.ball_obs[ix], rs['support_pts'])
      #self.ball_time = map(lambda ix: self.ball_time[ix], rs['support_pts'])
      deltaTs = self.kf.get_delta_Ts(self.ball_time)
      self.deltaTs = deltaTs
      means, covs, Pvals, As, Bs, Cs, Ds = self.kf.forward_recursion(np.array(self.ball_obs),\
          np.ones((len(self.ball_obs),1)), hidden = self.is_hidden, deltaTs=deltaTs)
      self.kf_state = {'means': means, 'covs': covs, 'P': Pvals, 'A': As, 'B': Bs, 'C': Cs, 'D': Ds}
      self.state = 1
      return True
    elif self.state == 1:
      At = self.kf_state['A'][-1]
      Bt = self.kf_state['B'][-1]
      Ct = self.kf_state['C'][-1]
      Dt = self.kf_state['D'][-1]
      P = self.kf_state['P'][-1]
      mu = self.kf_state['means'][-1]
      for i in xrange(nval):
        self.raw_ball_obs.append(cpos[i])
        self.raw_ball_time.append(ctime[i])
        dt = ctime[i] - self.ball_time[-1]
        if 'deltaT' in self.params and dt+self.params['deltaT_tol'] < self.params['deltaT']:
          continue
        hidden = False
        x = cpos[i]
        un = np.array([1])
        tmp = np.linalg.inv(np.dot(Ct,np.dot(P,Ct.T)) + self.kf.Sigma)
        K = np.dot(P,np.dot(Ct.T,tmp))
        pred = np.dot(Ct,np.dot(At,mu)) + np.dot(Ct,np.dot(Bt,un)) + np.dot(Dt, un)
        pred_cov = np.dot(Ct, np.dot(P, Ct.T)) + self.kf.Sigma
        mah_dist = np.sqrt(np.dot(x-pred,np.dot(np.linalg.inv(pred_cov),x-pred)))
        if mah_dist > self.params['max_mah_dist']:
          hidden = True # x = pred
          V = P
        else:
          V = P - np.dot(K,np.dot(Ct,P))
        mu = np.dot(At,mu) + np.dot(Bt,un) + np.dot(K,x - pred)
        V = (V + V.T) / 2 #For numerical reasons
        state = {'t': len(self.ball_obs)+1, 'prev_z': mu, 'action': np.array([1]), \
            'A': self.kf.A, 'B': self.kf.B, 'C': self.kf.C, 'D': self.kf.D}
        At = self.kf.A.mat(state, None, deltaT=dt)
        Bt = self.kf.B.mat(state, None, deltaT=dt)
        Ct = self.kf.C.mat(state, None, deltaT=dt)
        Dt = self.kf.D.mat(state, None, deltaT=dt)
        P = self.kf.Gamma + np.dot(At,np.dot(V,At.T))
        P = (P + P.T) / 2 #For numerical reasons
        self.kf_state['means'].append(mu)
        self.kf_state['covs'].append(V)
        self.kf_state['P'].append(P)
        self.kf_state['A'].append(At)
        self.kf_state['B'].append(Bt)
        self.kf_state['C'].append(Ct)
        self.kf_state['D'].append(Dt)
        self.ball_obs.append(x)
        self.ball_time.append(ctime[i])
        self.is_hidden.append(hidden)
        self.deltaTs.append(dt)
      return True

class BallVision:
  #robot: Class to comunicate with the robot
  #pos_buff, time_buff: Position and time buffers

  def __init__(self, robot):
    robot.append_clear_ball_obs()
    robot.send_program()
    self.robot = robot
    self.pos_buff = []
    self.time_buff = []

  def get_raw_ball_observations(self, callback):
    self.robot.append_get_ball_obs()
    raw = self.robot.send_program()
    if 'ball_pos' in raw:
      return callback(raw['ball_pos'])
    return False
  
  def get_ball_trajectory(self, **params):
    params.setdefault('capture_every', 0.1)
    params.setdefault('time_between_shots', 0.15)
    params.setdefault('max_balls_capture', 100)
    params.setdefault('min_balls_capture', 3)
    pos = self.pos_buff
    times = self.time_buff
    no_ball_time = 0.0
    while no_ball_time < params['time_between_shots'] or len(pos)==0:
      self.robot.append_get_ball_obs()
      ans = self.robot.send_program()
      if 'ball_pos' in ans:
        ts, ps = hypercube_filter_ball_data(ans['ball_pos'], **params)
        if len(ts)>0 and len(times)>0 and ts[0]>(times[-1] + params['time_between_shots']):
          if len(times)>=params['min_balls_capture']:
            self.pos_buff = ps
            self.time_buff = ts
            return times, pos
          else:
            times = []
            pos = []
        times += ts
        pos += ps
        if len(pos) > params['max_balls_capture']:
          break
        no_ball_time = 0.0
      else:
        no_ball_time += params['capture_every']
      time.sleep(params['capture_every'])
    self.pos_buff = []
    self.time_buff = []
    return times, pos
