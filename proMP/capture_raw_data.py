
import numpy as np
import ball_data
import barret
import time
import ball_hit_utils
import sys

sys.path.append('../kalman')
import kalman
import ball_model

class RawVision:
  #raw_data: The data as returned by the robot
  #vfilter: Some filter for ball data

  def __init__(self, vfilter, **params):
    self.vfilter = vfilter
    self.raw_data = []

  def __call__(self, data):
    if len(self.raw_data) > 0:
      self.raw_data += data
      self.vfilter(data)
      return True
    elif self.vfilter(data):
      self.raw_data += data
      return True
    return False

  def reset(self):
    self.vfilter.reset()
    self.raw_data = []

def default_vfilter(**params):
  params.setdefault('kalman_fname', 'saved_models/trained_kf.npz')
  kf, deltaT = ball_model.load_ball_model(params['kalman_fname'])
  vfilter = ball_data.BallFilter(kf)
  return vfilter

def default_init_pos(**params):
  return np.array([1.70692251,0.07026362,-0.1821418,1.5246011,-1.44901637,-0.01368657,0.21979911])

def capture_raw_data(**params):
  params.setdefault('host', 'wam')
  params.setdefault('port', '7646')
  params.setdefault('capture_every', 0.1)
  params.setdefault('capture_for', 3)
  params.setdefault('robot_freq', 500)
  params.setdefault('move_init_pos', False)
  params.setdefault('move_init_pos_steps', 1000)
  rob = barret.Robot(params['host'],params['port'])
  rvision = ball_data.BallVision(rob)
  raw_vision = RawVision(default_vfilter())
  while True:
    if params['move_init_pos']:
      while True:
        y = raw_input("Type 'y' to go to initial position (Make sure nobody stands near robot): ")
        if y == 'y':
          break
      q0 = default_init_pos()
      barret.go_to_state(rob, q0, params['move_init_pos_steps'], qdot_des=np.zeros(7))
      time.sleep(params['move_init_pos_steps'] / float(params['robot_freq']))
    print "Moving done. Waiting for ball observations"
    rob.append_clear_ball_obs()
    rob.send_program()
    while not rvision.get_raw_ball_observations(raw_vision):
      time.sleep(params['capture_every'])
    print "Got some good ball observations ..."
    #after we know it started sleep while capturing
    time.sleep(params['capture_for']-0.3)

    #and request the captured data
    rvision.get_raw_ball_observations(raw_vision)
    exec_traj = rob.get_arm_traj( int(params['capture_for']*params['robot_freq']) )

    #save
    to_save = {'ball': raw_vision.raw_data, 'q': np.array(exec_traj['q']), \
        'qdot': np.array(exec_traj['qdot']), 'rob_time': np.array(exec_traj['arm_times'])}
    filename = 'saved_traj/traj_' + ball_hit_utils.new_time_id() + '.npz'
    print "Saving to ", filename
    f = file(filename, 'wb')
    np.savez(f, **to_save)
    f.close()

    #cleanup
    raw_vision.reset()
    rob.clear_program()
    rob.append_clear_ball_obs()
    rob.send_program()

capture_raw_data()
