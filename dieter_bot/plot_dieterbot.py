""" Plots a Dieter's robot trajectory
"""

import numpy as np
import matplotlib.pyplot as plt
import argparse

def plot_dieterbot_traj(X, actions):
    plt.figure(1)
    for d in range(4):
        x = X[:,d]
        plt.subplot(3,4,d+1)
        plt.plot(x)
        plt.title("q{}".format(d))
        plt.ylabel("rad")
    for d in range(4):
        plt.subplot(3,4,d+5)
        plt.plot(X[:,4+2*d], color='b')
        plt.plot(X[:,5+2*d], color='r')
        plt.title("p_{},p_{}".format(2*d, 2*d+1))
    for d in range(4):
        plt.subplot(3,4,d+9)
        plt.plot(actions[:,2*d], color='b')
        plt.plot(actions[:,1+2*d], color='r')
        plt.title("u_{},u_{}".format(2*d,2*d+1))
    plt.show()

def plot_lengths(X, actions):
    L = [X[i].shape[0] for i in range(len(X))]
    plt.hist(L, bins=30)
    plt.xlabel("Duration")
    plt.ylabel("Fequency")
    plt.title("Duration of the time series in the dataset")
    plt.show()

def main(args):
    data = np.load(args.data, allow_pickle=True)
    plot_lengths(data["X"], data["actions"])
    plot_dieterbot_traj(data["X"][args.n], data["actions"][args.n])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("data", help="Folder where the original raw data is stored")
    parser.add_argument("--n", type=int, default=0, help="Index of the trajectory to plot")
    args = parser.parse_args()
    main(args)
