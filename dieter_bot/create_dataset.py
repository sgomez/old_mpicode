""" Loads the data set created with Dieter's robot
"""

import argparse
import os
import json
import time
import math
import numpy as np
import json
import re

def load_file(fname, remove_last=None):
    with open(fname, "r") as f:
        d = json.load(f)
        states = np.array(d["ob"])
        actions = np.array(d["action"])
        q = states[:,0:4]
        pressures = states[:,8:16]
    X = np.concatenate((q,pressures), axis=1)
    if remove_last is not None:
        X = X[:-remove_last,:]
        actions = actions[:-remove_last,:]
    return X, actions

def list_files(path, pattern = '\d+-\d+'):
    ans = []
    for folder, subs, files in os.walk(path):
        for f in files:
            if re.match(pattern, f):
                ans.append(os.path.join(folder, f))
    return ans

def load_folder(folder_name, remove_last=None):
    files = list_files(folder_name)
    print("{} files loaded".format(len(files)))
    X = []
    actions = []
    for i, fname in enumerate(files):
        X_i, actions_i = load_file(fname, remove_last=remove_last)
        X.append(X_i)
        actions.append(actions_i)
    return np.array(X), np.array(actions)

def main(args):
    X, actions = load_folder(args.folder, remove_last=1)
    np.savez(args.dest, X=X, actions=actions)
    print("Data saved")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("folder", help="Folder where the original raw data is stored")
    parser.add_argument("dest", help="File where the consolidated data shall be stored")
    args = parser.parse_args()
    main(args)
