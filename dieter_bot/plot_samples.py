
import traj_pred.trajectory as traj
import traj_pred.utils as utils
import numpy as np
import matplotlib.pyplot as plt
import time

import argparse
import logging

def main(args):
    logging.basicConfig(level=logging.INFO)
    with open(args.data,'rb') as f:
        data = np.load(f, encoding='latin1', allow_pickle=True)
        X = data['X']
        actions = data['actions']
    pred = traj.load_model(args.model)
    D = 3
    T = pred.length
    num_samples = 5
    print(T)

    for n in range(args.n):
        u = actions[n]
        x = np.array(X[n])
        nwindows = len(x) - T
        if nwindows < 0: continue

        start_ix = np.random.randint(low=0, high=nwindows)
        given_ix = np.random.randint(low=0, high=T-1)
        prev_obs = x[start_ix:start_ix+given_ix+1]
        action_window = u[start_ix:start_ix+T]

        t1 = time.time()
        y = pred.dense_traj_sample(prev_obs, action_window, num_samples=num_samples)
        t2 = time.time()
        logging.info("Prediction latency: {}".format(t2-t1))
        

        win_ix = np.arange(start_ix, start_ix+T)
        plt.figure(1)
        for d in range(4):
            plt.subplot(3,4,d+1)
            plt.plot(x[:,d], color='b')
            plt.axvline(x=given_ix+start_ix, color='k')
            for i in range(num_samples):
                plt.plot(win_ix, y[i,:,d], color='g')
            plt.title("q{}".format(d))
            plt.ylabel("rad")
        for d in range(4):
            plt.subplot(3,4,d+5)
            plt.plot(x[:,4+2*d], color='b')
            plt.axvline(x=given_ix, color='k')
            for i in range(num_samples):
                plt.plot(win_ix, y[i,:,4+2*d], color='g')
            plt.title("p_{}".format(2*d))

            plt.subplot(3,4,d+9)
            plt.plot(x[:,5+2*d], color='b')
            plt.axvline(x=given_ix, color='k')
            for i in range(num_samples):
                plt.plot(win_ix, y[i,:,8+2*d], color='g')
            plt.title("p_{}".format(2*d+1))

        plt.show()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="File with the normalized training/validation data")
    parser.add_argument('model', help="Path where the model is stored")
    parser.add_argument('--n', type=int, default=5, help="Number of instances to plot")
    parser.add_argument('--t', type=float, default=1.2, help="Time to predict in the future")
    parser.add_argument('--dt', type=float, default=1.0/180.0, help="Delta Time")
    args = parser.parse_args()
    main(args)
